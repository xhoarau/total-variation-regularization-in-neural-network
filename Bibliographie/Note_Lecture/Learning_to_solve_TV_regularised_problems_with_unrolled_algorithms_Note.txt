objectifs:
	learn algorithm to solve TV optimisation problème
	test two architecture

Architecture 1:
	with a change of variable they are able to write a Lasso problème equivalent of the 1D TV problème
	thats give a closed-form proximal operator
	they have also conjecture a covergence rate better than before

Architecure 2:
	Gregor and Le Cun (2010) have show that the resolution of the equivalent Lasso problème can be donne by a recurent NN
	they have generalized this to penalised least squared problème
	they give the architecture and the initialisation method (link to the problème definition)
	this method is only aproximate as it needs an infinite number of iteration

backpropagation:
	as the architecture are NN an unrolling method is obious
	it is also possible to Derivate the Prox-TV

conclusion:
	it needs less iteration to find a good solution, the aproximation is faster but less precise.

