Abstract:

problem :
	1) argmin[x] f(x) | x>=0
	2) argmin[x] f(x) | b_1 >= x >= b_2

algorithm:
	x_k+1 = [x_k - a_k D_k grad(f(x_k))]⁺
	with [.]⁺ projection in positive values
	a_k = stepsize
	D_k = calculate on second derivative of f

Introduction:

for the 1) probleme a solution has 2 constraint:
	df(x)/x_i >=0 pour tout i
	df(x)/x_i = 0 pour tout x_i > 0

	in this situation the algorithm is:
		x_k+1 = [x_k - a_k grad(f(x_k))]⁺ 
	
	it is difficult to choose a_k
		a good and efficient way is define a_k = beta^m_k s
		with m_k the first nonnegative integer satisfying
		f(x_k) - f(x_k(beta^m s)) >= sigma grad(f(x_k)) (x_k - x_k(beta^m s))
		
		x_k(a) = x_k+1 with a as stepsize
		s>0, sigma in (0,1/2), beta in (0,1)

	finally when it converge we have 
		B(x*)=B(x_k) pour tout k > k*
			k* finite
		B(x) = {i|x_i = 0}

all those results hold for
	x_k+1 = [x_k - a_k D_k grad(f(x_k))]⁺
	with D_k diagonal positive definit

to achive a superlinear convergence we need a non diaganal D_k define as a aproximation of the inverse Hessian

	Plan
		2) covergeance rate and convergeance properties
		3) introduction of constraint
			b_1 <= Ax <= b_2
			argmin[y] h(y):=f(A⁻¹y) b_1 <= y <= b_2
		4) results on large scale optimal controle problème

2)
D_k has constraint because it might lead tu unsolvable problems

I⁺(x) := {i| x_i = 0, df(x)/dx_i > 0}

D is diagonal with respect to a sub set of indices I if
	d_ij = 0 pour i in I and j != i

	x(a) = [x-a D grad(f(x)]
	x(a) is a critical point of problème 1 (proof in paper)

so each D_k has to be diagonal with respect to I⁺(x_k)








