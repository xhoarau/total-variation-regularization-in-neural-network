import numpy as np
import torch
import time
import sys
import os
from tqdm import tqdm
#import GPUtil

import modules.insertion_donne as insert
import modules.save_result as sr
import modules.train_fonctions as tf
import modules.storage_class as sc
import modules.shift_tensor as shift

def main(args, coef_front, coef_weight, choix_aléatoire, taille_graph, reseau_complet):
	##recherche du réseau lambda existant si demandé
  if args.pretrain_Lambda: 
    path = "Models/"
    if os.path.exists(path):
      resultat_Lambda = sr.load(path + "resultat_Lambda_" + args.name_save_lambda + ".pt")
    else:
      path = os.path.join(os.getcwd(),os.pardir,"Models/")
      if os.path.exists(path):
        resultat = sr.load(path + "resultat_Lambda_" + args.name_save_lambda + ".pt")
      else:
        path = os.getcwd()
        resultat = sr.load(path + "resultat_Lambda_" + args.name_save_lambda + ".pt")
    args.model = resultat_Lambda.trained_Lambda
    args.conv_width_Lambda = [args.model.conv_0[0].weight.shape[0],args.model.FE1.conv_0[0].weight.shape[0],args.model.conv_1[0].weight.shape[0]]
  
	##recherche du réseau déjà apris si demandé
  if args.retrain :
    path = "Models/"
    if os.path.exists(path):
      resultat = sr.load(path + "resultat_" + args.model_name + ".pt")
    else:
      path = os.path.join(os.getcwd(),os.pardir,"Models/")
      if os.path.exists(path):
        resultat = sr.load(path + "resultat_" + args.model_name + ".pt")
      else:
        path = os.getcwd()
        resultat = sr.load(path + "resultat_" + args.model_name + ".pt")
    state_dict = resultat.trained_model.cut_pursuit.state_dict()
    state_dict['Lambda'] = torch.tensor(args.Lambda_init)
    resultat.trained_model.cut_pursuit.load_state_dict(state_dict)
    args.model = resultat.trained_model

  # génération des ensembles de donnée
  test_set, train_set, validation_set,n_test,n_train,n_valid= insert.creation_set(args, rand_choice = choix_aléatoire)
  ## calcul des poids des classes dans la loss, et du coeficient au frontière des classes
  train_loader = torch.utils.data.DataLoader(train_set, batch_size=10, shuffle=False, drop_last=False)
  train_loader = tqdm(train_loader)
  compt_pixel_class = torch.zeros([args.n_class])
  if args.do_augment_front:# si augmentation Loss au Frontière de classes connectivité 2 seulement (8 voisin)
    valid_loader = torch.utils.data.DataLoader(validation_set, batch_size=1, shuffle=False, drop_last=False)
    test_loader = torch.utils.data.DataLoader(test_set, batch_size=1, shuffle=False, drop_last=False)
    compt_pixel_front = torch.zeros(1)
    train_px_front = torch.zeros([n_train,taille_graph[0],taille_graph[1]]).bool()
    validation_px_front = torch.zeros([n_valid,taille_graph[0],taille_graph[1]]).bool()
    test_px_front = torch.zeros([n_test,taille_graph[0],taille_graph[1]]).bool()
    if args.cuda:
      compt_pixel_front = compt_pixel_front.cuda()
  for index, (tiles, gt, connect, gt_complet) in enumerate(train_loader):
    unique,count = torch.unique(gt, sorted=True, return_counts=True)
    if torch.any(unique == 99):
        unique = unique[:-1]
        count = count[:-1]
    compt_pixel_class[unique] = compt_pixel_class[unique] + count
    if args.do_augment_front:
      px_front = torch.zeros(gt.shape).bool()
      if args.cuda:
        gt = gt.cuda()
        px_front = px_front.cuda()
      for hor in range(-1,2):
        for ver in range(-1+abs(hor),1-abs(hor)+1):
          if ver !=0 or hor !=0:
            gt_temp = gt+1
            if hor>0:
              gt_temp = shift.droite(hor,gt_temp)
            else:
              gt_temp = shift.gauche(-hor,gt_temp)
            if ver>0:
              gt_temp = shift.haut(ver,gt_temp)
            else:
              gt_temp = shift.bas(-ver,gt_temp)
            px_front = px_front + (gt == (1-(gt_temp-1)))
      compt_pixel_front = compt_pixel_front + torch.count_nonzero(px_front)
      train_px_front[index] = px_front
  if args.do_augment_front:
    for index, (tiles, gt, connect, gt_complet) in enumerate(valid_loader):
      px_front = torch.zeros(gt.shape).bool()
      if args.cuda:
        gt = gt.cuda()
        px_front = px_front.cuda()
      for hor in range(-1,1+1):
        for ver in range(-1+abs(hor),1-abs(hor)+1):
          if ver !=0 or hor !=0:
            gt_temp = gt+1
            if hor>0:
              gt_temp = shift.droite(hor,gt_temp)
            else:
              gt_temp = shift.gauche(-hor,gt_temp)
            if ver>0:
              gt_temp = shift.haut(ver,gt_temp)
            else:
              gt_temp = shift.bas(-ver,gt_temp)
            px_front = px_front + (gt == (1-(gt_temp-1)))
      validation_px_front[index] = px_front

    for index, (tiles, gt, connect, gt_complet) in enumerate(test_loader):
      px_front = torch.zeros(gt.shape).bool()
      if args.cuda:
        gt = gt.cuda()
        px_front = px_front.cuda()
      for hor in range(-1,1+1):
        for ver in range(-1+abs(hor),1-abs(hor)+1):
          if ver !=0 or hor !=0:
            gt_temp = gt+1
            if hor>0:
              gt_temp = shift.droite(hor,gt_temp)
            else:
              gt_temp = shift.gauche(-hor,gt_temp)
            if ver>0:
              gt_temp = shift.haut(ver,gt_temp)
            else:
              gt_temp = shift.bas(-ver,gt_temp)
            px_front = px_front + (gt == (1-(gt_temp-1)))
      test_px_front[index] = px_front
    if args.cuda:
      train_px_front = train_px_front.cuda()#1 si pixel sur fronière 0 sinon
      validation_px_front = validation_px_front.cuda()
      test_px_front = test_px_front.cuda()
  else:# pas de prise en compte des frontières de classes
    train_px_front = None
    validation_px_front = None
    test_px_front = None

  total_px = torch.sum(compt_pixel_class)
  print("total pixels etiquetés entrainement : ", total_px)
  print("pixels par classes : ", compt_pixel_class)
  # poids = 1/nb_classes + ratio (nb pixels classe; nb pixel etiqueté)
  # le coeficient vient accentuer les écarts si >1 ou les annulé si = 0, le poids ne pouvant être null limite basse a 1/nb_classes^2
  args.equilibrage = torch.maximum(torch.zeros(args.n_class),(compt_pixel_class-(args.equilibrage_classes*torch.min(compt_pixel_class)))/compt_pixel_class)
  compt_pixel_class = (torch.ones(args.equilibrage.shape)-args.equilibrage)*compt_pixel_class
  weight_class = torch.ones(args.n_class) - (compt_pixel_class/total_px)
  #torch.maximum(torch.tensor((1/args.n_class)**2)*0,1/args.n_class + ((total_px - compt_pixel_class)/(total_px*(args.n_class-1))-1/args.n_class)*coef_weight)
  if args.do_augment_front:
    facteur_front = ((total_px-compt_pixel_front)/compt_pixel_front)*coef_front
    args.facteur_front = facteur_front
  if args.cuda:
    args.weight_class = weight_class.cuda()
  else:
    args.weight_class = weight_class
  print("poids des classes ajustés : ",args.weight_class)
  print("equilibrage : ",args.equilibrage)
  print("pixels visible : ",compt_pixel_class)
  mes_temps_entrainement = [] # Liste pour mesure de temps d'entrainement

  ### apprentissage et sauvegarde
  print("depart apprentissage")

  if args.edge_detection: #apprentissage réseau dédier au Lambda
    print("edge_detection selectioné")
    mes_temps_entrainement.append(time.time())
    [trained_Lambda,evol_train_Lambda,evol_test_Lambda,evol_valid_Lambda,epoch_save_Lambda] = tf.train_full_Lambda(args, train_set, validation_set, test_set)
    mes_temps_entrainement.append(time.time())
		##sauvegarde resultat de l'apprentissage, evolution de la loss et de IoU, durée
    resultat_Lambda = sc.result_Lambda(trained_Lambda,evol_train_Lambda,evol_test_Lambda,evol_valid_Lambda,epoch_save_Lambda,mes_temps_entrainement[1]-mes_temps_entrainement[0])
    args.model = trained_Lambda # model accessible pour la suite
  else :
    resultat_Lambda = None
  if reseau_complet: #apprentissage du réseau complet
    print("reseau_complet")
    mes_temps_entrainement.append(time.time())
    [trained_model,evol_train_TV,evol_test_TV,evol_valid_TV,evol_composante,epoch_valid,epoch_regularisation] = tf.train_full_TV(args, train_set, validation_set, test_set, train_px_front, validation_px_front, test_px_front)
    mes_temps_entrainement.append(time.time())
		##sauvegarde resultat de l'apprentissage, evolution de la loss et de IoU, durée
    resultat = sc.result(trained_model,evol_train_TV,evol_valid_TV,evol_test_TV,evol_composante,mes_temps_entrainement[1]-mes_temps_entrainement[0],epoch_valid,epoch_regularisation)
  else :
    resultat = None

  return resultat_Lambda,resultat

##########################################################################################################################################
if __name__ == "__main__":
  ## définition des paramètre pour le réseau
  name = "test_FCN_2"
  print("nom du nouveau model : ",name)

  ##verifie si un réseau du même nom exist pour eviter les réecritures
  path = "Models/"
  if os.path.exists(path):
    filename = path + "resultat_" + name + ".pt"
    assert not os.path.isfile(filename)
  else:
    path = os.path.join(os.getcwd(),os.pardir,"Models/")
    if os.path.exists(path):
      filename = path + "resultat_" + name + ".pt"
      assert not os.path.isfile(filename)
    else:
      path = os.getcwd()
      filename = path + "resultat_" + name + ".pt"
      assert not os.path.isfile(filename)

  ### paramètre apprentissage
  n_epoch = 100 # nombre maxixmum d'epoch pour l'apprentissage
  batch_size = 5
  n_class = 21
  class_names = ["background","aeroplane","bicycle","bird","boat","bottle","bus","car","cat","chair","cow","diningtable","dog","horse","motorbike","person","pottedplant","sheep","sofa","train","tvmonitor"]
        # ["Autre","person","rider","car","truck","bus","train","motorcycle","bicycle","pole","traffic sign","traffic light"]
        # ["urban","non_uran"]
        # ["background","bird","cat","dog","horse","sheep","cow","elephant","bear","zebra","giraffe"]
        # ["building_limit","water","field","road","vegetation","building"]#["background","bird","cat","dog","horse","sheep","cow","elephant","bear","zebra","giraffe"]
        # ["background","Square"]
        # ["background","aeroplane","bicycle","bird","boat","bottle","bus","car","cat","chair","cow","diningtable","dog","horse","motorbike","person","pottedplant","sheep","sofa","train","tvmonitor"]
  n_channels = 3 # nombre de valeur par pixels pour l'image d'entrée (rgb = 3, grayscale = 1)
  coef_weight = 1 # coefficient reglant l'equilibrage des classes [0,1] ( Si 0 => 1/n_classe; Si 1 => equilibrage en fonction du nombre de pixels)
  cuda = 1 # utilisation de la carte graphique
  lr = 0.0001 # learning rate
  n_epoch_validation = 10 # nombre d'époch avant arret de l'apprentissage si pas d'amelioration ( a partir de la fin de l'apprentissage des lambdas : blocage_epoch + 2*red_grad_lambda)
  equilibrage_classes = 1000 # facteur maximal entre la classes la plus représenter et la moins représenter

  use_drop = False # compatible segnet, FCN
  coef_drop = 0.2 # coeficient de toutes les couches de drop out ## verifier la disposition dans module network_definition

  # scheduler s'applique sur tout les paramètres a partir du début de leur apprentisage (prise en compte de blocage_epoch et de red_grad_lambda)
  scheduler_step = 10 # nb epoch entre chaque changement de lr
  scheduler_gamma = 0.2 # facteur appliqué au lr
  refined_scheduler = True # ne pas prendre en compte le nombre d'époch mais la convergeance de la loss

  retrain = False # nouvel entrainement d'un model existant
  model_name = "Lambda_unique_not_reg_batchnorm_update" # nom du model à réentrainer
  blocage_reseau = False # bloque l'apprentissage des paramètres du réseau préentrainé (apprentisage de la régularisation seul)

    ### caractéristique données
  set_name = 'square_5' #'big_segmentation_animaux_coco' # choix des données (nom du fichier)
  #(pas compatible avec tout les datasets)
  choix_aléatoire = False # choix des images entrainement et validation  (ensemble de tests et d'entrainement déjà selectionée)
  prop_degrad = 0 # proportion de dégradation de l'etiquetage ([0,1])
  prop_img_train_set = 1 # nombre d'image dans l'ensemble d'entrainement (max * [0,1])

  land_scape = False
  urban = False # choix entre des donnée urban/not urban or road/not road (mettre a faux si square = True)
  
  square = False # choix set carré

  fiftyone = False # prend un dataset fiftyone (coco)
  resize = False #mettre faux si fiftyone dataset déjà resize 

  VOC = True # prend le dataset PascalVOC 2012 (trainval (2300,300,300))
  taille_ensemble = [1400,1200,300] # [train,val,test] total = 2900 max


  ### caractéristique regularisation
  do_reg = False # applique la régularisation a la fin du réseau
  TV = False # ajoute un terme de variation total dans la loss
  coef_TV = 2
  do_augment_front = False # augmenté la loss au frontière de class (mettre Faux si coef_front = 0 economie de memoire)
  coef_front = 1 # coefficient reglant l'importance des pixels au frontière [0,1]
  
  blocage_epoch = 5 # nombre d'époch sans régularisation (10 trop grand le réseau est trop précis) (compatible TV loss, CP, TVL)
  # (-1 => aprend le modèle sans régularisation puis l'ajoute en utilisant blocage_reseau pour bloqué ou non le premier model)

   ### option de reseau
    ##SegNet
  SegNet = False
  conv_width = [64,128,128,64,32] # nombre de filtre à chaque couche, nombre impaire de couche:
  # la dernière est indépandante; toutes les autres sont constuite par paire pour les connections intermedière
  # toutes les paire de convolution avec les mêmes paramètres:
  dilation = 1 # seulement la deuxième convolution de chaque paire
  kernel_size = 3 # toute les convolutions

    ##UNet
  unetflex = False
  nb_level = 4
  nbfeaturesbase = 20
  ratiofeatures = 2

    ##FCNs
  FCN = True 

    #CUt pursuit (compatible segnet)
  Cut_Pursuit = False
  #choix de la methode
  Lambda_unique = False # gradient calculé analytiquement
  gradient_analytique = False # lambda multiple
  graphe_tronque = False # lambda multiple et gradiant analytique sur une portion du graphe
  diff_fini = False # Lambda multiple et gradiant calculer par difference fini

  nb_thread = 1 # si >0 maximise les thread de Cut pursuit
  do_use_connect_set = 0
  # 0 : ne pas utilisé les donné sur les arrête
  # 1 : donner les Lambdas "Parfait" (incompatible avec données satellitaires)
  # 2 : ajouté les données dans la loss (utilisé avec coef_reg)
  # 3 : donner les Lambdas "Parfait" + générer des résidu (incompatible avec données satellitaires)

  # génération du graphe lier au donné:
  taille_graph = [256,256] #[384,384] #taille spacial des données
  connectivity = 1 # si graph dist = False : 1 = 4 plus proche, 2 = 8 plus proche
  graph_dist = False # graph a grand distance 
  List_Distance = [1,4,8] # dist des distance considérer, puissance de 2
  max_dir = 0 # densité des direction considérer

  reg_sousgrad = False #choix controle Ridge ou Subgrad (si coef_reg = 0 mettre False pour economie de memoire) (pris en compte si do_use_connect_set != 2)
  coef_reg = 0 # poid du controle des lambda dans la loss
  blocage_reg = 0 # nombre d'epoch sans contrôle des lambdas (reg_sousgrad bloquer) (aprés blocage epoch)
  # petit nombre de composante => 10% des arrète coupé sois un coût sur 115000 arrète
  # 94% des pixels deja bien prédis => gain possible estimé 1%
  # coef reg < 1% / 115000 = 9e-8 sois environ 1e-7
  nb_feature = 1/2 # proportion de feature donnée au lambda ([0,1])
  red_grad_lambda = 0 #nombre d'epoch de reduction des gradiants de lambda (aprés blocage epoch)
  taille_sous_graph_pixel = 1 #taille du sous graph pour le calcul du gradiant (si calcul sur graph tronqué)
  taille_sous_graph_arret = 0 #taille du sous graph pour le calcul du gradiant des lambdas (si calcul sur graph tronqué)

  logit = False # ajoute les logits pour la generation des Lambdas
  couche_sup_lambda = False # ajout d'une couche suplémentaire sur la génération des lambdas
  Lambda_separe = False # la génération des lambdas a son propre réseau
  conv_width_Lambda = [2,4,2] # utile si Lambda_separe = True
  Lambda_init = 1 # valeur initial de lambda si Lambda unique

  pretrain_Lambda = False # récupère un réseau prés appris 
  name_save_lambda = "edge_detection_leger" #nom du réseau prés appris
  edge_detection = False # apprend un réseau spécifique pour les Lamdbas
  n_dilation = 4 # nombre de convolution dans le bloque enrichissement (voir module network_definition TIN1) 

      #TV layer (compatible segnet)
  TV_layer = False # utiliser la methode layer TV (papier tv layer for computer vision)
  TVL_Lambda_unique = False # un seul lambda dans le dernier layer (appliquable que si 2 classes)
  Multi_TV_layer = False # ajouter un layer TV aprés chaque convolution

  reseau_complet = SegNet + FCN + unetflex
  # verification selection model correcte
  assert (SegNet + FCN + unetflex) == 1 # une seul option selectionée à la fois
  assert (not do_reg and not Cut_Pursuit and not TV_layer) or (do_reg and Cut_Pursuit and (Lambda_unique + gradient_analytique + graphe_tronque + diff_fini == 1) and os.getenv('CONDA_DEFAULT_ENV')!='tv_opt') or (do_reg and TV_layer and os.getenv('CONDA_DEFAULT_ENV')=='tv_opt')


  ### création des données
  args = sc.argument(n_epoch, batch_size, n_class, class_names, n_channels, cuda, lr, n_epoch_validation, equilibrage_classes, \
                      use_drop, coef_drop, scheduler_step, scheduler_gamma, refined_scheduler, retrain, model_name,  blocage_reseau, \
                      set_name, prop_degrad, prop_img_train_set, \
                      land_scape, urban, square, fiftyone, resize, VOC, taille_ensemble,\
                      SegNet, conv_width, dilation, kernel_size, \
                      unetflex, nb_level, nbfeaturesbase, ratiofeatures, \
                      FCN, \
                      do_reg, blocage_epoch, TV, coef_TV, do_augment_front, \
                      Cut_Pursuit, Lambda_unique, gradient_analytique, graphe_tronque, diff_fini, \
                      nb_thread, do_use_connect_set, taille_graph, connectivity, graph_dist, List_Distance, max_dir, \
                      reg_sousgrad, coef_reg, blocage_reg, nb_feature, red_grad_lambda, taille_sous_graph_pixel, taille_sous_graph_arret, \
                      logit, couche_sup_lambda, Lambda_separe, conv_width_Lambda, Lambda_init, \
                      pretrain_Lambda, name_save_lambda, edge_detection, n_dilation, \
                      TV_layer, TVL_Lambda_unique, Multi_TV_layer, \
                      )
  # print("args : ",GPUtil.getGPUs()[0].memoryUsed)
	
  resultat_Lambda,resultat = main(args, coef_front, coef_weight, choix_aléatoire, taille_graph, reseau_complet)

	## sauvegarde des résultats
  path = "Models/"
  if os.path.exists(path):
    if reseau_complet:
      sr.save(resultat, path + "resultat_" + name + ".pt")
    if args.edge_detection:
      sr.save(resultat_Lambda, path + "resultat_Lambda_" + name + ".pt")
    sr.save(args, path + "args_" + name + ".pt")
  else:
    path = os.path.join(os.getcwd(),os.pardir,"Models/")
    if os.path.exists(path):
      if reseau_complet:
        sr.save(resultat, path + "resultat_" + name + ".pt")
      if args.edge_detection:
        sr.save(resultat_Lambda, path + "resultat_Lambda_" + name + ".pt")
      sr.save(args, path + "args_" + name + ".pt")
    else:
      path = os.getcwd()
      if reseau_complet:
        sr.save(resultat, path + "resultat_" + name + ".pt")
      if args.edge_detection:
        sr.save(resultat_Lambda, path + "resultat_Lambda_" + name + ".pt")
      sr.save(args, path + "args_" + name + ".pt")
	
	## recuperation, sauvegarde et affichage des temps de calcul
  mes_temps_cut_pursuit,mes_temps_backward,mes_temps_reg_value,mes_temps_gene_Lambda,mes_temps_gene_logit,mes_temps_reg = tf.net_def.get_temps()
  
  
  mes_temps_cut_pursuit = torch.tensor(mes_temps_cut_pursuit).float()
  mes_temps_backward = torch.tensor(mes_temps_backward).float()
  mes_temps_reg_value = torch.tensor(mes_temps_reg_value).float()
  mes_temps_gene_Lambda = torch.tensor(mes_temps_gene_Lambda).float()
  mes_temps_gene_logit = torch.tensor(mes_temps_gene_logit).float()
  mes_temps_reg = torch.tensor(mes_temps_reg).float()
  
  print("Mesures Temps : \n")
  print("mean cut pursuit : ",torch.mean(mes_temps_cut_pursuit))
  print("std cut pursuit : ",torch.std(mes_temps_cut_pursuit))
  print("\n")
  print("mean backward : ",torch.mean(mes_temps_backward))
  print("std backward : ",torch.std(mes_temps_backward))
  print("\n")
  print("mean reg_value : ",torch.mean(mes_temps_reg_value))
  print("std reg_value : ",torch.std(mes_temps_reg_value))
  print("\n")
  print("mean gene_Lambda : ",torch.mean(mes_temps_gene_Lambda))
  print("std gene_Lambda : ",torch.std(mes_temps_gene_Lambda))
  print("\n")
  print("mean gene_logit : ",torch.mean(mes_temps_gene_logit))
  print("std gene_logit : ",torch.std(mes_temps_gene_logit))
  print("\n")
  print("mean reg : ",torch.mean(mes_temps_reg))
  print("std reg : ",torch.std(mes_temps_reg))
  print("\n")
  
  mes_temps_pred,mes_temps_backward,mes_temps_optimizer = tf.get_temps()
  
  
  mes_temps_pred = torch.tensor(mes_temps_pred).float()
  mes_temps_backward = torch.tensor(mes_temps_backward).float()
  mes_temps_optimizer = torch.tensor(mes_temps_optimizer).float()
  mes_temps_gene_Lambda = torch.tensor(mes_temps_gene_Lambda).float()
  
  print("Mesures Temps : \n")
  print("mean pred : ",torch.mean(mes_temps_pred))
  print("std pred : ",torch.std(mes_temps_pred))
  print("\n")
  print("mean backward total: ",torch.mean(mes_temps_backward))
  print("std backward total: ",torch.std(mes_temps_backward))
  print("\n")
  print("mean optimizer : ",torch.mean(mes_temps_optimizer))
  print("std optimizer : ",torch.std(mes_temps_optimizer))
  print("\n")
  
  print("\n Fin")
