"""
SCRIPT NON EXECUTABLE 

import de tout les codes important pour les tests
morceau de code a utiliser pour les tests

"""

import math
#format libraries
import h5py
#visualization libraries
import matplotlib.pyplot as plt
import matplotlib.image


#deep learning tools
import torch
import torch.nn as nn
import torchvision.transforms as transforms

from PIL import Image
from PIL import ImageOps
from PIL import ImageDraw

import modules.plot_exemple_result as pl_re

import sys,os
import numpy as np

sys.path.append(os.path.expanduser('~'),"grid-graph/python/bin")
from grid_graph import grid_to_graph
from grid_graph import edge_list_to_forward_star

sys.path.append(os.path.join(os.path.expanduser('~'),"parallel-cut-pursuit/python/wrappers")
from cp_prox_tv import cp_prox_tv

# name_old_set = "square_5_bord"

# filename = name_old_set + ".hdf5"
# if os.path.isfile(filename):
#   data_file = h5py.File(filename,'r')
# else:
#   if hasattr(args,'set_name'):
#     filename = os.path.join(os.getcwd(),os.pardir,args.set_name + ".hdf5")
#   else:
#     filename = os.path.join(os.getcwd(),os.pardir,"land_cover.hdf5")

#   if os.path.isfile(filename):
#     data_file = h5py.File(filename,'r')
#   else:
#     assert False and "pas de fichier de donnée"


# train_obs = torch.tensor(data_file['train_observation'][:])
# train_gt = torch.tensor(data_file['train_gt'][:])
# test_obs = torch.tensor(data_file['test_observation'][:])
# test_gt = torch.tensor(data_file['test_gt'][:])

# taille = train_gt.shape[1]
# n_train = train_gt.shape[0]
# n_test = test_gt.shape[0]

# filter_h = torch.tensor([[12,11,10,9,8,7,0,-7,-8,-9,-10,-11,-12],[11,10,9,8,7,6,0,-6,-7,-8,-9,-10,-11],[10,9,8,7,6,5,0,-5,-6,-7,-8,-9,-10],[9,8,7,6,5,4,0,-4,-5,-6,-7,-8,-9],[8,7,6,5,4,3,0,-3,-4,-5,-6,-7,-8],[7,6,5,4,3,2,0,-2,-3,-4,-5,-6,-7],[6,5,4,3,2,1,0,-1,-2,-3,-4,-5,-6],[7,6,5,4,3,2,0,-2,-3,-4,-5,-6,-7],[8,7,6,5,4,3,0,-3,-4,-5,-6,-7,-8],[9,8,7,6,5,4,0,-4,-5,-6,-7,-8,-9],[10,9,8,7,6,5,0,-5,-6,-7,-8,-9,-10],[11,10,9,8,7,6,0,-6,-7,-8,-9,-10,-11],[12,11,10,9,8,7,0,-7,-8,-9,-10,-11,-12]])
# # filter_h = torch.tensor([[2,1,0,-1,-2],[4,2,0,-2,-4],[6,3,0,-3,-6],[4,2,0,-2,-4],[2,1,0,-1,-2]])
# # filter_h = torch.tensor([[5,4,0,-4,-5],[8,10,0,-10,-8],[10,20,0,-20,-10],[8,10,0,-10,-8],[5,4,0,-4,-5]])

# filter_v = filter_h.transpose(0,1)
# conv = nn.Conv2d(1,2,13)
# conv.weight.data[0] = filter_h
# conv.weight.data[1] = filter_v
# conv.bias.data = torch.tensor([0.,0.])

# def print_img(img,num):
#   fig = plt.figure(figsize=(5, 5)) #adapted dimension
#   ax = fig.add_subplot(1, 1, 1, aspect='equal')
#   plt.imshow(torch.squeeze(img).detach().numpy(),cmap='gray')
#   plt.savefig("test_filtre " + str(num) + ".png")
#   plt.close() 

# def use_cp_prox_tv(args,num,img,Lambda,output,List_Comp,List_Gtv,nb_proc=1):#calcule cp_prox_tv sur l'image et l'enregistre a l'indice donné
#   if args.reg_sousgrad:
#     # mes_temps_cut_pursuit.append(time.time())
#     Comp,rX,Gtv = cp_prox_tv(img, args.first_edges, args.adj_vertices, edge_weights =  (Lambda).cpu().detach().numpy(),verbose=0,compute_Subgrads = True, max_num_threads=nb_proc, pfdr_dif_tol=0.00001) #.cpu.detach.numpy pour récupérer une valeur utilisable
#     # mes_temps_cut_pursuit[len(mes_temps_cut_pursuit)-1] = time.time() - mes_temps_cut_pursuit[len(mes_temps_cut_pursuit)-1]
#     List_Gtv[num]=torch.from_numpy(Gtv).cuda()
#   else:
#     # mes_temps_cut_pursuit.append(time.time())
#     input_bug[num] = [img, args.first_edges, args.adj_vertices, (Lambda).cpu().detach().numpy(), 0, 1, 0.00001]
#     Comp,rX = cp_prox_tv(img, args.first_edges, args.adj_vertices, edge_weights =  (Lambda).cpu().detach().numpy(),verbose=0, max_num_threads=nb_proc, pfdr_dif_tol=0.00001) #.cpu.detach.numpy pour récupérer une valeur utilisable
#     # mes_temps_cut_pursuit[len(mes_temps_cut_pursuit)-1] = time.time() - mes_temps_cut_pursuit[len(mes_temps_cut_pursuit)-1]
#   output[num,0,:,:]=torch.from_numpy(rX[Comp].reshape([256,256]))#reviens a la forme d'origine et reviens en torch (transfert vers cuda)
#   List_Comp[num]=Comp.astype("int64")# conserve Comp pour backward



img = Image.open('singe_couleur.png')
convert_tensor = transforms.ToTensor()
img1 = convert_tensor(img)[0]
img2 = convert_tensor(img)[1]
img3 = convert_tensor(img)[2]

sharp = 1
moyenne = 1
lissage_fin = 0
boucle = 20

fig = plt.figure(figsize=(boucle*5,15)) #adapted dimension

# edges, connectivities = grid_to_graph(np.array(img.shape,dtype='int32'), connectivity = 1, compute_connectivities = True, graph_as_forward_star = False, row_major_index = True)# calcul du graph des adjacance
# first_edges,adj_vertices, reindex = edge_list_to_forward_star(np.prod(img.shape),edges)# changement de representation
# first_edges = first_edges.astype('uint32')
# adj_vertices = adj_vertices.astype('uint32')
# edges[reindex,:] = np.copy(edges)
# edges = torch.from_numpy(edges.astype("int64"))
# connectivities[reindex] = np.copy(connectivities)
# connectivities = torch.sqrt(torch.from_numpy(connectivities))

conv = nn.Conv2d(1,1,3,padding = 1)

conv.weight.data = torch.tensor([[[[1,1,1],[1,1,1],[1,1,1]]]])/9.0
conv.bias.data = torch.tensor([0.])

for i in range(boucle):
  ax = fig.add_subplot(3, boucle, i+1, aspect='equal')
  print("shooth: ",i)
  if moyenne:
    img_2 = conv(img.reshape([1,1,256,256])).reshape([256,256])
  else:
    Comp,rX = cp_prox_tv(img.numpy().transpose(), first_edges, adj_vertices, edge_weights = 0.05,verbose=0, max_num_threads=1)
    img_2 = rX[Comp].reshape([256,256])
  if sharp:
    print("sharp: ",i)
    img = img * 2 - img_2
  else:
    img = torch.tensor(img_2)
  plt.imshow(torch.squeeze(img).detach().numpy(),cmap='gray')

for i in range(boucle):
  ax = fig.add_subplot(3, boucle, i+1+boucle, aspect='equal')
  print("shooth: ",i)
  if moyenne:
    img_2 = conv(img2.reshape([1,1,256,256])).reshape([256,256])
  else:
    Comp,rX = cp_prox_tv(img2.numpy().transpose(), first_edges, adj_vertices, edge_weights = 0.05,verbose=0, max_num_threads=1)
    img_2 = rX[Comp].reshape([256,256])
  if sharp:
    print("sharp: ",i)
    img2 = img2 * 2 - img_2
  else:
    img2 = torch.tensor(img_2)
  plt.imshow(torch.squeeze(img2).detach().numpy(),cmap='gray')

for i in range(boucle):
  ax = fig.add_subplot(3, boucle, i+1+2*boucle, aspect='equal')
  print("shooth: ",i)
  if moyenne:
    img_2 = conv(img3.reshape([1,1,256,256])).reshape([256,256])
  else:
    Comp,rX = cp_prox_tv(img3.numpy().transpose(), first_edges, adj_vertices, edge_weights = 0.05,verbose=0, max_num_threads=1)
    img_2 = rX[Comp].reshape([256,256])
  if sharp:
    print("sharp: ",i)
    img3 = img3 * 2 - img_2
  else:
    img3 = torch.tensor(img_2)
  plt.imshow(torch.squeeze(img3).detach().numpy(),cmap='gray')

if lissage_fin:
  print("smooth")
  if moyenne:
    img_2 = conv(img)
  else:
    Comp,rX = cp_prox_tv(img.numpy().transpose(), first_edges, adj_vertices, edge_weights = 1,verbose=0, max_num_threads=1)
    img_2 = rX[Comp].reshape([256,256])

plt.savefig("sharp_mc3.png")
plt.close() 

# Comp,rX = cp_prox_tv(img,)

img = Image.open('lena1.png')
convert_tensor = transforms.ToTensor()
img = convert_tensor(img)[0]

sharp = 1
moyenne = 1
lissage_fin = 0
boucle = 20

fig = plt.figure(figsize=(boucle*5,5)) #adapted dimension

# edges, connectivities = grid_to_graph(np.array(img.shape,dtype='int32'), connectivity = 1, compute_connectivities = True, graph_as_forward_star = False, row_major_index = True)# calcul du graph des adjacance
# first_edges,adj_vertices, reindex = edge_list_to_forward_star(np.prod(img.shape),edges)# changement de representation
# first_edges = first_edges.astype('uint32')
# adj_vertices = adj_vertices.astype('uint32')
# edges[reindex,:] = np.copy(edges)
# edges = torch.from_numpy(edges.astype("int64"))
# connectivities[reindex] = np.copy(connectivities)
# connectivities = torch.sqrt(torch.from_numpy(connectivities))

conv = nn.Conv2d(1,1,3,padding = 1)

conv.weight.data = torch.tensor([[[[1,1,1],[1,1,1],[1,1,1]]]])/9.0
conv.bias.data = torch.tensor([0.])

for i in range(boucle):
  ax = fig.add_subplot(1, boucle, i+1, aspect='equal')
  print("shooth: ",i)
  # img_2 = conv(img.reshape([1,1,256,256])).reshape([256,256])
  # img = img * 2 - img_2
  Comp,rX = cp_prox_tv(img.detach().numpy().transpose(), first_edges, adj_vertices, edge_weights = 0.5,verbose=0, max_num_threads=1)
  img_2 = rX[Comp].reshape([256,256])
  img = img * 2 - img_2
  plt.imshow(torch.squeeze(img).detach().numpy(),cmap='gray')

plt.savefig("cp005_sharp_test.png")
plt.close() 

def moving_average(x, w):
  return np.convolve(x, np.ones(w), 'valid') / w

begin=0
end=1
npoint = 1000
boucle = 10
window = 0.05
interval = (end-begin)/npoint
t = np.linspace(begin, end, npoint, endpoint=True)
y = signal.sawtooth(2 * np.pi * 2 * t)
fig = plt.figure(figsize=((boucle+1)*5,5))
ax = fig.add_subplot(1, boucle+1, 1)
plt.plot(t, y)

for i in range(boucle):
  y1 = moving_average(np.pad(y,int(window/interval-1),'wrap'),int(2*window/interval-1))
  y = 2*y-y1
  # y = y/np.max(np.abs(y))
  ax = fig.add_subplot(1, boucle+1, i+2)
  plt.plot(t, y)

plt.savefig("test.png")
plt.close()
