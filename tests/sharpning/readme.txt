following the reading of "Total Variation Optimization Layers for Computer Vision" paper

questions : 
  what is sharpning and how it works?
  is it useful?

definition in the paper:
  after a smothing operation (minimisation of TV) 
    y = RegTV(x)
  y = 2x - y

adding to the original the difference with the mean

tests:
  1) cut_pursuit with unique lambda = 0.5
  2) cut_pursuit with unique lambda = 0.05
  3) gaussian mean filter of size 5
  4) mean filter of size 3

  each image is the result after an iteration
  good edge detection tool, a bit overpowered
  cut-pursuit creat new edges when lambda too big and takes a lot of time
    but keep more information inside the image

if pixel > local mean : go up
if pixel < local mean : go down
else : don't move

create a spatial oscilation near the edges

git commit -m "ajout du dossier de tests, test du sharpning"
