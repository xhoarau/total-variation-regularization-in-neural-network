import fiftyone as fo
import fiftyone.zoo as foz
import numpy as np

classes = {'bird':1,'cat':2,'dog':3,'horse':4,'sheep':5,'cow':6,'elephant':7,'bear':8,'zebra':9,'giraffe':10}

dataset = foz.load_zoo_dataset(
  "coco-2017",
  split = "validation",
  dataset_name = "segmentation_coco_animaux_validation",
  label_types = "segmentations",
  classes = ["bird","cat","dog","horse","sheep","cow","elephant","bear","zebra","giraffe"],
  only_matching = True,
  max_samples = 40,
)

pb = fo.ProgressBar()

for sample in pb(dataset):
  mask = np.zeros([sample.metadata.height,sample.metadata.width])
  for d in sample.ground_truth.detections:
    mask[round(d.bounding_box[1]*mask.shape[0]):round((d.bounding_box[1]+d.bounding_box[3])*mask.shape[0]),round(d.bounding_box[0]*mask.shape[1]):round((d.bounding_box[0]+d.bounding_box[2])*mask.shape[1])] = d.mask*classes[d.label]
  sample['segmentations'] = fo.Segmentation(mask=mask)
  sample.save()

dataset.default_mask_targets = {1:'bird',2:'cat',3:"dog",4:"horse",5:"sheep",6:"cow",7:"elephant",8:"bear",9:"zebra",10:"giraffe"}
dataset.persistent = True




