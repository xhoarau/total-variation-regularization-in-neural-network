import torch
import fiftyone as fo
import fiftyone.zoo as foz
import fiftyone.utils.labels as foul
from PIL import Image
from torchvision.transforms import functional as func

dataset = foz.load_zoo_dataset(
    "cityscapes",
    dataset_name="cityscape_construction",
    source_dir = "/home/xhoarau/these/cityscapes/",
)

pb = fo.ProgressBar()

nb_train = 200
nb_test = 100
nb_valid = 100
tr = 0
te = 0
v= 0
list_delete = []

for sample in pb(dataset):
     if sample.tags[0] == "train":
             if tr < nb_train:
                    tr = tr+1
             else:
                     if te < nb_test:
                             te = te+1
                             sample.tags[0] = "test"
                             sample.save()
                     else:
                             list_delete.append(sample.id)
     else:
             if sample.tags[0] == "validation":
                     if v < nb_valid:
                             v = v+1
                     else:
                             list_delete.append(sample.id)
             else:
                     list_delete.append(sample.id)

dataset.delete_samples(list_delete)

import fiftyone.utils.labels as foul
foul.objects_to_segmentations(
	dataset,
	"gt_fine",
	"segmentations",
	mask_targets={1: "person", 2: "rider",3: "car",4: "truck",5: "bus",6: "train",7: "motorcycle",8: "bicycle",9: "pole",10: "traffic sign",11: "traffic light"},
)



dataset.persistent = True
dataset.delete_sample_field("gt_fine")
dataset.name = "little_cityscape"
dataset.save()

