from skimage.metrics import structural_similarity as ssim
import matplotlib.pyplot as plt
import numpy as np
import fiftyone as fo
from PIL import Image
from torchvision import transforms as T
import torch
def mse(imageA, imageB):
	# the 'Mean Squared Error' between the two images is the
	# sum of the squared difference between the two images;
	# NOTE: the two images must have the same dimension
	err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
	err /= float(imageA.shape[0] * imageA.shape[1])

	# return the MSE, the lower the error, the more "similar"
	# the two images are
	return err

dataset = fo.load_dataset("big_segmentation_animaux_coco")
pb = fo.ProgressBar()

result_mse = []
result_ssim = []

for sample in pb(dataset):
    #img = Image.open(sample.filepath)
    gt = torch.from_numpy(sample.segmentations.mask)
    #trans_img = T.Compose([
    #    T.Resize([512,512]),
    #    T.Resize([img.size[1],img.size[0]]),
    #    ])
    trans_gt = T.Compose([
        T.ToPILImage(),
        T.Resize([512,512],interpolation=T.InterpolationMode.NEAREST),
        T.Resize(gt.shape,interpolation=T.InterpolationMode.NEAREST),
        T.ToTensor(),
        ])

    #result_mse.append(mse(np.array(img),np.array(trans_img(img))))
    #result_ssim.append(ssim(np.array(img),np.array(trans_img(img)),channel_axis=2))
    gt_r = (trans_gt(gt.float()/10)*10).squeeze().round()
    result_mse.append(mse(np.array(gt),np.array(gt_r)))
    result_ssim.append(ssim(np.array(gt),np.array(gt_r)))

