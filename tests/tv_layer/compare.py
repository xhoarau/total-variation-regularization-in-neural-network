import torch
import numpy as np
from matplotlib import pyplot as plt
import sys,os
import time

assert os.getenv('CONDA_DEFAULT_ENV')=='tv_opt'

sys.path.append(os.path.expanduser('~')+"/these/tv_layers_for_cv/grid-graph/python/bin")
from grid_graph import grid_to_graph
from grid_graph import edge_list_to_forward_star

sys.path.append(os.path.expanduser('~')+"/these/tv_layers_for_cv/parallel-cut-pursuit/python/wrappers")
from cp_prox_tv import cp_prox_tv
from cp_pfdr_d1_ql1b import cp_pfdr_d1_ql1b

from tv_opt_layers.layers.general_tv_2d_layer import GeneralTV2DLayer

from PIL import Image
from PIL import ImageOps
from PIL import ImageDraw

from prox_tv import tv1_2d

img = Image.open('lena.png')

img_np = np.array(img,np.float32)/255
img_ten = torch.from_numpy(np.array(img,np.float32)/255)

edges, connectivities = grid_to_graph(np.array(img_np.shape,dtype='int32'), connectivity = 1, compute_connectivities = True, graph_as_forward_star = False, row_major_index = True)
first_edges,adj_vertices, reindex = edge_list_to_forward_star(np.prod(img_np.shape),edges)
first_edges = first_edges.astype('uint32')
adj_vertices = adj_vertices.astype('uint32')
edges[reindex,:] = np.copy(edges)
edges = torch.from_numpy(edges.astype("int64"))
connectivities[reindex] = np.copy(connectivities)
connectivities = torch.sqrt(torch.from_numpy(connectivities))

Lambda = [0.01,0.05,0.1,0.5,1,2,5,10,50]
n_test = 25

vcp=[]
vtvl=[]
vptv=[]
tcp=[]
ttvl=[]
tptv=[]
etvl=[]
ertvl=[]
eptv=[]
erptv=[]

for i in range(len(Lambda)):
    L = Lambda[i]
    vcp.append([])
    vtvl.append([])
    vptv.append([])
    tcp.append([])
    ttvl.append([])
    tptv.append([])
    etvl.append([])
    ertvl.append([])
    eptv.append([])
    erptv.append([])

    print("test Lambda = ",L)
    layer = GeneralTV2DLayer(lmbd_init=L,num_iter=10).cuda()
    for _ in range(n_test):
        tmp_cp = time.time()
        Comp,rX = cp_pfdr_d1_ql1b(img_np.transpose(), 1, first_edges, adj_vertices, edge_weights=L, verbose=0, max_num_threads=0, pfdr_dif_tol=0.00001)
        #Comp,rX = cp_prox_tv(img_np.transpose(), first_edges, adj_vertices, edge_weights = L,verbose=0, max_num_threads=1,pfdr_dif_tol=0.00001)
        img_out_cp = rX[Comp].reshape([256,256])
        tmp_cp = time.time()-tmp_cp
      
        tmp_tvl = time.time()
        img_out_TVl = layer(img_ten.cuda().reshape([1,1,256,256])).reshape([256,256])
        tmp_tvl = time.time()-tmp_tvl
      
        tmp_ptv = time.time()
        img_out_ptv = tv1_2d(img_np,L)
        tmp_ptv = time.time()-tmp_ptv

        if len(Lambda) == 1:
            plt.imshow(img_out_cp,cmap='gray')
            plt.savefig("lena_cp.png")
            plt.close()

            plt.imshow(img_out_TVl.detach().cpu().numpy(),cmap='gray')
            plt.savefig("lena_TVl.png")
            plt.close()

            plt.imshow(img_out_ptv,cmap='gray')
            plt.savefig("lena_ptv.png")
            plt.close()
        
        img_out_ptv = torch.from_numpy(img_out_ptv)
        img_out_TVl = img_out_TVl.detach().cpu()
        img_out_cp = torch.from_numpy(img_out_cp)
        val_tvl = 1/2*torch.pow(torch.norm(img_out_TVl-img_np,2),2)+torch.sum(torch.diff(img_out_TVl.reshape(65536)[edges])*L)
        val_cp = 1/2*torch.pow(torch.norm(img_out_cp-img_np,2),2)+torch.sum(torch.diff(img_out_cp.reshape(65536)[edges])*L)
        val_ptv = 1/2*torch.pow(torch.norm(img_out_ptv-img_np,2),2)+torch.sum(torch.diff(img_out_ptv.reshape(65536)[edges])*L)

        ecart_tvl = val_tvl-val_cp
        ecart_relatif_tvl = (ecart_tvl/val_cp)*100

        ecart_ptv = val_ptv-val_cp
        ecart_relatif_ptv = (ecart_ptv/val_cp)*100

        if n_test == 1:
            print("Lambda=",L)
            print("cp objectif value ",val_cp)
            print("tvl objectif value ",val_tvl)
            print("ptv objectif value ",val_ptv)
            print("ecart tvl ",ecart_tvl)
            print("ecart relatif tvl ",ecart_relatif_tvl)
            print("ecart ptv ",ecart_ptv)
            print("ecart relatif ptv ",ecart_relatif_ptv)
            print("temps cp ",tmp_cp)
            print("temps tvl ",tmp_tvl)
            print("temps ptv ",tmp_ptv)
        vcp[i].append(val_cp.item())
        print(vcp)
        vtvl[i].append(val_tvl.item())
        vptv[i].append(val_ptv.item())
        tcp[i].append(tmp_cp)
        ttvl[i].append(tmp_tvl)
        tptv[i].append(tmp_ptv)
        etvl[i].append(ecart_tvl.item())
        ertvl[i].append(ecart_relatif_tvl.item())
        eptv[i].append(ecart_ptv.item())
        erptv[i].append(ecart_relatif_ptv.item())

for i in range(len(Lambda)):
    print("Lambda=",Lambda[i])
    print("cp objectif value ",np.mean(vcp[i]))
    print("cp std objectif value ",np.std(vcp[i]))
    print("tvl objectif value ",np.mean(vtvl[i]))
    print("tvl std objectif value ",np.std(vtvl[i]))
    print("ptv objectif value ",np.mean(vptv[i]))
    print("ptv std objectif value ",np.std(vptv[i]))
    print("ecart tvl ",np.mean(etvl[i]))
    print("ecart std tvl ",np.std(etvl[i]))
    print("ecart relatif tvl ",np.mean(ertvl[i]))
    print("ecart std relatif tvl ",np.std(ertvl[i]))
    print("ecart ptv ",np.mean(eptv[i]))
    print("ecart std ptv ",np.std(eptv[i]))
    print("ecart relatif ptv ",np.mean(erptv[i]))
    print("ecart std relatif ptv ",np.std(erptv[i]))
    print("temps cp ",np.mean(tcp[i]))
    print("temps std cp ",np.std(tcp[i]))
    print("temps tvl ",np.mean(ttvl[i]))
    print("temps std tvl ",np.std(ttvl[i]))
    print("temps ptv ",np.mean(tptv[i]))
    print("temps std ptv ",np.std(tptv[i]))



