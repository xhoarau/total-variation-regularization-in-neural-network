import numpy as np 
import torch
from matplotlib import pyplot as plt
import sys,os

from tv_opt_layers.layers.general_tv_2d_layer import GeneralTV2DLayer

from  PIL import Image
from  PIL import ImageOps
from  PIL import ImageDraw

layer = GeneralTV2DLayer(lmbd_init=1,num_iter=10).cuda()

img = Image.open("lena.png")

img_out = layer(torch.from_numpy(np.array(img,np.float32)).permute(2,0,1).unsqueeze(0).cuda())

plt.imshow((img_out/256).cpu().detach().squeeze().permute(1,2,0).numpy())

plt.savefig("butterfly2.png")
















