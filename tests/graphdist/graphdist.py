import numpy as np

def graph_dist_unique(dist,connect,grid):
  """
  calcul l'ensemble des aretes (paire de points) dans une grille
  dist = nombre>=0 (distance entre les points : norm_inf = 2^dist)
  connect = nombre in [0,dist+1] 
    densité des connextions:
      -1 : seulement selon les directions principales
      n : tout les point dont les coordonées sont composées de 0 et de multiple de 2^dist/2^(n)
  grid = numpy array (dimention de la grille)
  edge = numpy array (output, ensemble des paires relier)
  """
  dist_val = np.power(2,dist)
  dim = grid.shape[0]
  if dist>=0 and connect<=dist and connect>=-1:
    if connect == -1:
      ensemble = np.diag(np.full(dim,dist_val))
    else:
      list_element = np.arange(-dist_val,dist_val+0.1,dist_val/np.power(2,connect),dtype=int)
      ensemble = np.stack([np.tile(list_element,list_element.shape[0]),np.repeat(list_element,list_element.shape[0])],axis=1)
      for d in range(2,dim):
        ensemble = np.concatenate((np.expand_dims(np.tile(list_element,np.power(list_element.shape[0],2)),axis=1),ensemble.repeat(list_element.shape[0],axis=0)),axis=1) 
      ensemble = ensemble[np.logical_and(ensemble[np.arange(ensemble.shape[0]),np.argmin(ensemble==0,axis=1)]>=0,np.max(np.abs(ensemble),axis=1)==dist_val)]
    n_point_dim = np.array([1])
    grid_co = np.arange(grid[0])
    n_point=grid[0]
    if dim > 1:
      n_point_dim = np.append(n_point_dim,n_point)
      grid_co = np.transpose(np.stack((np.tile(np.arange(grid[1]),grid[0]),grid_co.repeat(grid[1]))))
      n_point = n_point*grid[1]
    for d in range(2,dim):
      n_point_dim = np.append(n_point_dim,n_point)
      grid_co = np.concatenate((np.expand_dims(np.tile(np.arange(grid[d]),n_point),axis=1),grid_co.repeat(grid[d],axis=0)),axis=1)
      n_point = n_point*grid[d]
    end_co = np.repeat(grid_co,ensemble.shape[0],axis=0)+np.tile(ensemble,(np.prod(grid),1))
    edge = np.stack((np.repeat(grid_co,ensemble.shape[0],axis=0),end_co),axis=1)
    connectivitie = np.tile(np.linalg.norm(ensemble,2,axis=1),np.prod(grid))[np.logical_and(np.prod(edge<grid,axis=(1,2),dtype=bool),np.min(edge,axis=(1,2))>=0)]
    edge = edge[np.logical_and(np.prod(edge<grid,axis=(1,2),dtype=bool),np.min(edge,axis=(1,2))>=0)]
    edge = np.sum(np.multiply(edge,n_point_dim),axis=2)
    return edge,connectivitie

def graph_dist(dist,connect,grid):
  """
  calcul l'ensemble des aretes (paire de points) dans une grille
  dist = numpy array (list des distances à créer, cf. graph_dist_unique)
  connect = nombre (densité maximal des connextions, cf. graph_dist_unique)
            numpy array (de même dimention que dist, densité de connextion pour chaque distance)
  grid = numpy array (dimention de la grille)
  edge = numpy array (output)
  """
  if np.isscalar(connect):
    connect = np.array([connect])
  assert connect.shape[0]==1 or connect.shape[0]==dist.shape[0]
  connect = np.minimum(connect,dist)
  edges,connectivities = graph_dist_unique(dist[0],connect[0],grid)
  for d in range(1,dist.shape[0]):
    n_edge,n_connectivitie = graph_dist_unique(dist[d],connect[d],grid)
    edges = np.concatenate([edges,n_edge])
    connectivities = np.concatenate([connectivities,n_connectivitie])
  return edges,connectivities
