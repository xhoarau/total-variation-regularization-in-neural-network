import os
import yaml
import time
import shutil
import torch
import random
import argparse
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

from torch.utils import data
from tqdm import tqdm

from ptsemseg.models import get_model
from ptsemseg.loss import get_loss_function
from ptsemseg.loader import get_loader
from ptsemseg.utils import get_logger
from ptsemseg.metrics import runningScore, averageMeter
from ptsemseg.augmentations import get_composed_augmentations
from ptsemseg.schedulers import get_scheduler
from ptsemseg.optimizers import get_optimizer
from ptsemseg.loader.collate import collate_crop_small

from ptsemseg.utils import convert_state_dict

from tensorboardX import SummaryWriter

import GPUtil

cfg = yaml.safe_load(open("configs/fcn8s_pascal_batch.yml"))

# Setup seeds
torch.manual_seed(cfg.get("seed", 1337))
torch.cuda.manual_seed(cfg.get("seed", 1337))
np.random.seed(cfg.get("seed", 1337))
random.seed(cfg.get("seed", 1337))

# Setup device
device = torch.device("cuda:"+str(cfg["training"]["device"]-1) if torch.cuda.is_available() else "cpu")
torch.cuda.set_device(device)
#print(torch.cuda.mem_get_info())

# Setup Augmentations
augmentations = cfg["training"].get("augmentations", None)
data_aug = get_composed_augmentations(augmentations)

# Setup Dataloader
data_loader = get_loader(cfg["data"]["dataset"])
data_path = cfg["data"]["path"]


if "scribble_path" in cfg["data"]:
    scribble_path = cfg["data"]["scribble_path"]
    length_scribble = cfg["data"]["length_scribble"]
else:
    scribble_path = None
    length_scribble = None

t_loader = data_loader(
    data_path,
    sbd_path = cfg["data"]["sbd_path"],
    scribble_path = scribble_path,
    is_transform=True,
    split=cfg["data"]["train_split"],
    img_size=(cfg["data"]["img_rows"], cfg["data"]["img_cols"]),
    augmentations=data_aug,
    length_scribble = length_scribble
)

v_loader = data_loader(
data_path,
sbd_path = cfg["data"]["sbd_path"],
scribble_path = scribble_path,
    is_transform=True,
    split=cfg["data"]["val_split"],
    img_size=(cfg["data"]["img_rows"], cfg["data"]["img_cols"]),
    length_scribble = length_scribble
)

n_classes = t_loader.n_classes

if cfg["training"]["batch_size"] != 1:
    collate_fn = collate_crop_small
else:
    collate_fn = None

trainloader = data.DataLoader(
    t_loader,
    batch_size=cfg["training"]["batch_size"],
    num_workers=0,#cfg["training"]["n_workers"],
    collate_fn = collate_fn,
    shuffle=True,
)

valloader = data.DataLoader(
    v_loader, batch_size=1, num_workers=cfg["training"]["n_workers"]
)

# Setup Metrics
running_metrics_val = runningScore(n_classes)

# Setup Model
#print(torch.cuda.mem_get_info())
model = get_model(cfg["model"], n_classes).to(device)

device_ids = [cfg["training"]["device"]-1]
#device_ids = [*range(torch.cuda.device_count())]
#device_ids.pop(cfg["training"]["device"]-1)
#device_ids.insert(0,cfg["training"]["device"]-1)
print("device id : ",device_ids)
model = torch.nn.DataParallel(model, device_ids=device_ids)

# Setup optimizer, lr_scheduler and loss function
optimizer_cls = get_optimizer(cfg)
optimizer_params = {k: v for k, v in cfg["training"]["optimizer"].items() if k != "name"}

optimizer = optimizer_cls(model.parameters(), **optimizer_params)

scheduler = get_scheduler(optimizer, cfg["training"]["lr_schedule"])

loss_fn = get_loss_function(cfg)

start_iter = 0

val_loss_meter = averageMeter()
time_meter = averageMeter()

best_iou = -100.0
i = start_iter
flag = True
count_converge = 0

graph = True

for (images, labels) in trainloader:
    if graph:
        G = images[1]
        images = images[0]
    else:
        G = None
    print(images.shape)
    print(G)
