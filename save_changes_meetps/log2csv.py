import csv
import argparse
import yaml

parser = argparse.ArgumentParser(description='take the file log path and put PA and IoU in csv')
parser.add_argument("-pl", "--pathtolog", required = True)
parser.add_argument("-fn", "--filename", required = True)
parser.add_argument("-cf", "--config", required = True)
args = parser.parse_args()

cfg = yaml.safe_load(open(args.config))

f_log = open(args.pathtolog,"r")
f_csv = open(args.filename,"w",newline='')
writer = csv.writer(f_csv)
writer.writerow(["Num iteration","OA","mIoU"])
iteration = 0
for row in f_log:
    old_iteration = iteration
    
    idx = row.find("Overall")
    if idx!=-1:
        idx_val = row.find("0",idx)
        PA = row[idx_val:idx_val+10]
    
    idx = row.find("IoU")
    if idx!=-1:
        idx_val = row.find("0",idx)
        IoU = row[idx_val:idx_val+10]
        iteration = iteration + 1
    
    if iteration!=old_iteration:
        writer.writerow([iteration*cfg["training"]["val_interval"],PA,IoU])

f_log.close()
f_csv.close()
