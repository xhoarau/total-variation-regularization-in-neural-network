import os
from os import path
from os import listdir
from os import system as sys

from git import Repo

repo = Repo(".")

target = "save_changes_meetps"
if not path.exists(target):
    os.makedirs(target)

list_ext = [".py",".yml",".pkl",".log"]

for d in repo.index.diff(None):
    dirname = path.dirname(d.a_path)
    if len(dirname) != 0 and not path.exists(path.join(target,dirname)):
        os.makedirs(path.join(target,dirname))
    sys("cp " + d.a_path + " " + path.join(target,dirname))

for d in repo.untracked_files:
    if path.splitext(d)[-1] in list_ext and d.split("/")[0] != target:
        dirname = path.dirname(d)
        if len(dirname) != 0 and not path.exists(path.join(target,dirname)):
            os.makedirs(path.join(target,dirname))
        sys("cp " + d + " " + path.join(target,dirname))

