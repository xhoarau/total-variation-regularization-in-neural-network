r""""Contains definitions of the methods used by the _BaseDataLoaderIter workers to
collate samples fetched from dataset into Tensor(s).

These **needs** to be in global scope since Py2 doesn't support serializing
static methods.
"""

import torch
import re
import collections
from torch._six import string_classes
import math
import random

np_str_obj_array_pattern = re.compile(r'[SaUO]')

default_collate_err_msg_format = (
    "collate_crop_small: batch must contain tensors, numpy arrays, numbers, "
    "dicts or lists; found {}")

def collate_crop_small(batch, pos=None, size = None):
    r"""Puts each data field into a tensor with outer dimension batch size
        crop all sample to the size of the smallest on two last dimention"""
    elem = batch[0]
    elem_type = type(elem)
    if isinstance(elem, torch.Tensor):
        batch = list(batch)
        out = None
        if pos is None:
            pos,size = define_random_crop(batch)
        for idx in range(len(batch)):
            batch[idx] = batch[idx][...,pos[0]:pos[0]+size[0],pos[1]:pos[1]+size[1]].squeeze()
        batch = tuple(batch)
        if torch.utils.data.get_worker_info() is not None:
            # If we're in a background process, concatenate directly into a
            # shared memory tensor to avoid an extra copy
            numel = sum(x.numel() for x in batch)
            storage = elem.storage()._new_shared(numel)
            out = elem.new(storage)
        return torch.stack(batch, 0, out=out)
    elif elem_type.__module__ == 'numpy' and elem_type.__name__ != 'str_' \
            and elem_type.__name__ != 'string_':
        if elem_type.__name__ == 'ndarray' or elem_type.__name__ == 'memmap':
            # array of string classes and object
            if np_str_obj_array_pattern.search(elem.dtype.str) is not None:
                raise TypeError(default_collate_err_msg_format.format(elem.dtype))

            return collate_crop_small([torch.as_tensor(b) for b in batch])
        elif elem.shape == ():  # scalars
            return torch.as_tensor(batch)
    elif isinstance(elem, float):
        return torch.tensor(batch, dtype=torch.float64)
    elif isinstance(elem, int):
        return torch.tensor(batch)
    elif isinstance(elem, string_classes):
        return batch
    elif isinstance(elem, collections.abc.Mapping):
        return {key: collate_crop_small([d[key] for d in batch]) for key in elem}
    elif isinstance(elem, tuple) and hasattr(elem, '_fields'):  # namedtuple
        return elem_type(*(collate_crop_small(samples) for samples in zip(*batch)))
    elif isinstance(elem, collections.abc.Sequence):
        if pos is None:
            pos, size = define_random_crop(batch)
        # check to make sure that the elements in batch have consistent size
        it = iter(batch)
        elem_size = len(next(it))
        if not all(len(elem) == elem_size for elem in it):
            raise RuntimeError('each element in list of batch should be of equal size')
        transposed = zip(*batch)
        return [collate_crop_small(samples,pos = pos, size = size) for samples in transposed]

    raise TypeError(default_collate_err_msg_format.format(elem_type))

def define_random_crop(batch):
    if isinstance(batch[0], collections.abc.Sequence):
        return define_random_pos(batch[0])
    else:
        min_height = math.inf
        min_width = math.inf
        pos = []
        for elem in batch:
            min_height = min(min_height,batch[idx].shape[-1])
            min_width = min(min_width,batch[idx].shape[-2])
        for elem in batch:
            h1 = random.randint(0,elem.shape[-1]-min_height)
            w1 = random.randint(0,elem.shape[-2]-min_width)
            pos.append((w1,h1))
        return pos,(min_width,min_height)
