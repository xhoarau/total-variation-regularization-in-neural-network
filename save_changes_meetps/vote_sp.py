import torch
from torch_scatter import scatter_max

def majority_SP(lbl,SP_map):
    unique,counts = torch.unique(torch.stack((SP_map.flatten(),lbl.flatten())),return_counts=True,dim=1)
    _,argmax = scatter_max(counts,unique[0])
    val = unique[1,argmax]
    return val[SP_map]
