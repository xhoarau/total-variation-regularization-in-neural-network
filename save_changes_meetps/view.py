import numpy as np


def view_error(pred, gt,ignored_index = 250):
    """ show the error between pred and gt with colorcode: 
    green when 'gt'='pred', red when 'gt'!='pred' and black
    when unannotated (gt = 99). Display in figure ax"""
    colors = np.zeros((pred.shape[0],pred.shape[1],3))
    colors[pred==gt] = np.array([0., 1., 0.]) #correct prediction
    colors[pred!=gt] = np.array([1., 0., 0.]) #error
    colors[gt==ignored_index] = np.array([0., 0., 0.])   #unannotated 
    return colors
