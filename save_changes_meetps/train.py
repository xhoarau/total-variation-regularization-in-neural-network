import os
import yaml
import time
import shutil
import torch
import random
import argparse
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

from torch.utils import data
from torchmetrics import JaccardIndex
from tqdm import tqdm

from ptsemseg.models import get_model
from ptsemseg.loss import get_loss_function
from ptsemseg.loader import get_loader
from ptsemseg.utils import get_logger
from ptsemseg.metrics import runningScore, averageMeter
from ptsemseg.augmentations import get_composed_augmentations
from ptsemseg.schedulers import get_scheduler
from ptsemseg.optimizers import get_optimizer
from ptsemseg.loader.collate import collate_crop_small

from ptsemseg.utils import convert_state_dict

from tensorboardX import SummaryWriter

import GPUtil

from vote_sp import majority_SP
from view import view_error

def val(cfg):
    # Setup seeds
    torch.manual_seed(cfg.get("seed", 1337))
    torch.cuda.manual_seed(cfg.get("seed", 1337))
    np.random.seed(cfg.get("seed", 1337))
    random.seed(cfg.get("seed", 1337))

    # Setup device
    device = torch.device("cuda:"+str(cfg["training"]["device"]-1) if torch.cuda.is_available() else "cpu")
    torch.cuda.set_device(device)

    # Setup Augmentations
    augmentations = cfg["training"].get("augmentations", None)
    data_aug = get_composed_augmentations(augmentations)

    # Setup Dataloader
    data_loader = get_loader(cfg["data"]["dataset"])
    data_path = cfg["data"]["path"]
    
    if "scribble_path" in cfg["data"]:
        scribble_path = cfg["data"]["scribble_path"]
        length_scribble = cfg["data"]["length_scribble"]
    else:
        scribble_path = None
        length_scribble = None
    
    if "scale_SuperPixels" in cfg["model"]:
        scale_SuperPixels = cfg["model"]["scale_SuperPixels"]
    else:
        scale_SuperPixels = None


    """t_loader = data_loader(
        data_path,
        sbd_path = cfg["data"]["sbd_path"],
        scribble_path = scribble_path,
        is_transform=True,
        split=cfg["data"]["train_split"],
        img_size=(cfg["data"]["img_rows"], cfg["data"]["img_cols"]),
        augmentations=data_aug,
        length_scribble = length_scribble,
        scale_SuperPixels = scale_SuperPixels,
    )"""

    v_loader = data_loader(
        data_path,
        sbd_path = cfg["data"]["sbd_path"],
        scribble_path = scribble_path,
        is_transform=True,
        split=cfg["data"]["val_split"],
        img_size=(cfg["data"]["img_rows"], cfg["data"]["img_cols"]),
        length_scribble = length_scribble,
        scale_SuperPixels = scale_SuperPixels,
    )

    n_classes = v_loader.n_classes
    if cfg["training"]["batch_size"] != 1:
        collate_fn = collate_crop_small
    else:
        collate_fn = None

    """trainloader = data.DataLoader(
        t_loader,
        batch_size=cfg["training"]["batch_size"],
        num_workers=cfg["training"]["n_workers"],
        collate_fn = collate_fn,
        shuffle=True,
    )"""

    valloader = data.DataLoader(
        v_loader, batch_size=1, num_workers=cfg["training"]["n_workers"]
    )

    # Setup Metrics
    running_metrics_val = runningScore(n_classes)
    running_metrics_val_SP = runningScore(n_classes)
    compute_IoU = JaccardIndex(task="multiclass",num_classes=n_classes)
    average_gain = averageMeter()
    count_gain = 0

    # Setup Model
    model_file_name = os.path.split(args.model_path)[1]
    model_name = model_file_name[: model_file_name.find("p")-1]

    model_dict = {"arch": model_name}
    model = get_model(model_dict, n_classes)
    state = convert_state_dict(torch.load(args.model_path)["model_state"])
    model.load_state_dict(state)
    model.eval()
    model.to(device)
    
    device_ids = [*range(torch.cuda.device_count())]
    device_ids.pop(cfg["training"]["device"]-1)
    device_ids.insert(0,cfg["training"]["device"]-1)
    model = torch.nn.DataParallel(model, device_ids=device_ids)
    
    loss_fn = get_loss_function(cfg)

    start_iter = 0

    train_loss_meter = averageMeter()
    val_loss_meter = averageMeter()
    train_time_meter = averageMeter()
    val_time_meter = averageMeter()

    flag = True
    
    model.eval()

    list_classes = torch.zeros(n_classes)
    min_gain = 100
    """
    # Setup Model
    model2 = get_model(cfg["model"], n_classes).to(device)
    
    device_ids = [*range(torch.cuda.device_count())]
    device_ids.pop(cfg["training"]["device"]-1)
    device_ids.insert(0,cfg["training"]["device"]-1)
    print(device_ids)
    model2 = torch.nn.DataParallel(model2, device_ids=device_ids)

    model2.eval()

    # Setup optimizer, lr_scheduler and loss function
    optimizer_cls = get_optimizer(cfg)
    optimizer_params = {k: v for k, v in cfg["training"]["optimizer"].items() if k != "name"}

    optimizer = optimizer_cls(model.parameters(), **optimizer_params)
    logger.info("Using optimizer {}".format(optimizer))

    scheduler = get_scheduler(optimizer, cfg["training"]["lr_schedule"])
    
    n_shown = 13
    n_element = 3
    
    fig = plt.figure(figsize=(n_element * 5, n_shown * 5), dpi = 500)
    subplot_index = 1
    for i in range(n_shown):
        image,label = v_loader.__getitem__(i)
        image = image.to(device)
        #label = label.to(device)
        if "nb_it_start" in cfg["model"]:
            pred,pred_not_reg,idx_reg = model(image.unsqueeze(0),cfg["model"]["nb_it_start"]+1)
        else:
            pred = model(image.unsqueeze(0))
            pred_not_reg = None
            unique,counts = torch.unique(torch.argmax(pred,dim=1),return_counts=True)
            if 0 in unique:
                unique = unique[1:]
                counts = counts[1:]
            if counts.shape[0]>3:
                idx_reg = torch.topk(counts,3)[1]
            else:
                idx_reg = unique

        print(pred.shape)
        print(idx_reg)
        pred = pred.detach().cpu().squeeze(0)
        pred_rgb = t_loader.decode_segmap(torch.argmax(pred,dim=0).long().numpy())
        label_rgb = t_loader.decode_segmap(label.long().numpy())

        ax = fig.add_subplot(n_shown,n_element,subplot_index, aspect='equal')
        plt.imshow(((image-torch.min(image))/(torch.max(image)-torch.min(image))).detach().cpu().squeeze().permute(1,2,0).numpy())
        plt.axis('off')
        subplot_index = subplot_index + 1
        ax = fig.add_subplot(n_shown,n_element,subplot_index, aspect='equal')
        plt.imshow(label_rgb)
        plt.axis('off')
        subplot_index = subplot_index + 1
        ax = fig.add_subplot(n_shown,n_element,subplot_index, aspect='equal')
        plt.imshow(pred_rgb)
        plt.axis('off')
        subplot_index = subplot_index + 1
        
        if pred_not_reg is not None:
            pred_not_reg = pred_not_reg.detach().cpu().squeeze(0)
            pred_not_reg_rgb = t_loader.decode_segmap(torch.argmax(pred_not_reg,dim=0).long().numpy())

            ax = fig.add_subplot(n_shown,n_element,subplot_index, aspect='equal')
            plt.imshow(pred_not_reg_rgb)
            plt.axis('off')
        
        subplot_index = subplot_index + 1
        
        for j in range(3):
            if j < idx_reg.numel():
                ax = fig.add_subplot(n_shown,n_element,subplot_index, aspect='equal')
                plt.imshow(pred[idx_reg[j]],cmap='gray')
                plt.title("max : "+str(torch.round(torch.max(pred[idx_reg[j]])*1000.).item()/1000.)+", min : "+str(torch.round(torch.min(pred[idx_reg[j]])*1000.).item()/1000.))
                plt.axis('off')
                subplot_index = subplot_index + 1
                if pred_not_reg is not None:
                    ax = fig.add_subplot(n_shown,n_element,subplot_index, aspect='equal')
                    plt.imshow(pred_not_reg[idx_reg[j]],cmap='gray')
                    plt.title("max : "+str(torch.round(torch.max(pred_not_reg[idx_reg[j]])*1000.).item()/1000.)+", min : "+str(torch.round(torch.min(pred_not_reg[idx_reg[j]])*1000.).item()/1000.))
                    plt.axis('off')
                    subplot_index = subplot_index + 1
                    ax = fig.add_subplot(n_shown,n_element,subplot_index, aspect='equal')
                    diff = torch.abs(pred[idx_reg[j]]-pred_not_reg[idx_reg[j]])
                    plt.imshow(diff,cmap='gray')
                    plt.title("max : "+str(torch.round(torch.max(diff)*1000.).item()/1000.)+", min : "+str(torch.round(torch.min(diff)*1000.).item()/1000.))
                    plt.axis('off')
                    subplot_index = subplot_index + 1
                else:
                    subplot_index = subplot_index + 2
            else:
                subplot_index = subplot_index + 3
    
    plt.savefig(model_name+".png")
    
    #print("mem start : ",GPUtil.getGPUs()[0].memoryUsed)
    n_reg = 0
    n_class = 0
    i = 0
    
    for (images, labels) in tqdm(trainloader):
        #print("mem boucle principale : ",torch.cuda.max_memory_allocated())
        i=i+1
        start_ts = time.time()
        images = images.to(device)
        labels = labels.to(device)
        #print("images shape : ",images.shape)
        #print("mem load : ",torch.cuda.max_memory_allocated())

        #assert not torch.any(torch.isnan(images))
        if "nb_it_start" in cfg["model"]:
            outputs = model(images,cfg["model"]["nb_it_start"]+1)
        else:
            outputs = model(images)
        #unique,count = torch.unique(torch.topk(outputs,3,dim=1)[0],return_counts=True)
        #n_reg = n_reg + unique[count>1000].numel()
        #n_class = n_class + unique.numel()
        loss = loss_fn(input=outputs, target=labels)
        train_loss_meter.update(loss.item())

        train_time_meter.update(time.time() - start_ts)

    print("train average loss : ",train_loss_meter.avg)
    print("train average time : ",train_time_meter.avg)
    #print("mean n reg : ",n_reg/i)
    #print("mean n class : ",n_class/i)
    """
    i = 0
    for (images, labels) in tqdm(valloader):
        i = i+1
        if "scale_SuperPixels" in cfg["model"] and cfg["model"]["scale_SuperPixels"]!=0:
            images,SP_maps = images
            SP_maps = SP_maps.to(device)
        start_ts = time.time()
        images = images.to(device)
        labels = labels.to(device)
        #print("images shape : ",images.shape)
        #print("mem load : ",torch.cuda.max_memory_allocated())

        #assert not torch.any(torch.isnan(images))
        if "nb_it_start" in cfg["model"]:
            outputs = model(images,cfg["model"]["nb_it_start"]+1)
        else:
            outputs = model(images)

        loss = loss_fn(input=outputs, target=labels)
        val_loss_meter.update(loss.item())

        val_time_meter.update(time.time() - start_ts)
        pred = outputs.data.max(1)[1].cpu().numpy()
        gt = labels.data.cpu().numpy()
        running_metrics_val.update(gt, pred)
        IoU = compute_IoU(labels.data.cpu(),outputs.data.max(1)[1].cpu())
        if "scale_SuperPixels" in cfg["model"] and cfg["model"]["scale_SuperPixels"]!=0:
            SP_pred = majority_SP(outputs.data.max(1)[1],SP_maps).cpu()
            running_metrics_val_SP.update(gt, SP_pred.numpy())
            IoU_SP = compute_IoU(labels.data.cpu(),SP_pred.data)
            count_gain = count_gain + (IoU < IoU_SP) * 1
            average_gain.update((IoU_SP - IoU).numpy())

        if torch.any(list_classes[torch.unique(labels)]==0):
            list_classes[torch.unique(labels)] = 1
            n_plot = 4
            fig = plt.figure(figsize=(n_plot * 5, 2 * 5), dpi = 500)

            subplot_index = 1
            ax = fig.add_subplot(2, n_plot, subplot_index, aspect='equal')
            ax.set(title="RGB")
            plt.imshow(((images-torch.min(images))/(torch.max(images)-torch.min(images))).squeeze().permute(1,2,0).cpu())
            plt.axis('off')
            subplot_index = subplot_index + 1
            
            ax = fig.add_subplot(2, n_plot, subplot_index, aspect='equal')
            ax.set(title="label")
            plt.imshow(v_loader.decode_segmap(labels.squeeze().cpu().numpy()))
            plt.axis('off')
            subplot_index = subplot_index + 1

            ax = fig.add_subplot(2, n_plot, subplot_index, aspect='equal')
            ax.set(title="pred")
            plt.imshow(v_loader.decode_segmap(pred.squeeze()))
            plt.axis('off')
            subplot_index = subplot_index + 1
            
            ax = fig.add_subplot(2, n_plot, subplot_index, aspect='equal')
            ax.set(title="error")
            plt.imshow(view_error(pred.squeeze(),labels.squeeze().cpu().numpy()))
            plt.axis('off')
            subplot_index = subplot_index + 1
            
            if "scale_SuperPixels" in cfg["model"] and cfg["model"]["scale_SuperPixels"]!=0:
                SP_maps = SP_maps.squeeze()
                SP_bord = torch.logical_not(torch.logical_or(torch.abs(SP_maps - torch.roll(SP_maps,shifts=(0,1),dims=(0,1))),torch.abs(SP_maps - torch.roll(SP_maps,shifts=(1,0),dims=(0,1))))).squeeze()*1
            
                ax = fig.add_subplot(2, n_plot, subplot_index, aspect='equal')
                ax.set(title="SP_RGB")
                plt.imshow((((images-torch.min(images))/(torch.max(images)-torch.min(images)))*SP_bord).squeeze().permute(1,2,0).cpu())
                plt.axis('off')
                subplot_index = subplot_index + 1
                
                ax = fig.add_subplot(2, n_plot,subplot_index, aspect='equal')
                ax.set(title="SP_label")
                plt.imshow((v_loader.decode_segmap(labels.squeeze().cpu().numpy())))
                plt.axis('off')
                subplot_index = subplot_index + 1

                ax = fig.add_subplot(2, n_plot, subplot_index, aspect='equal')
                ax.set(title="SP_pred")
                plt.imshow(v_loader.decode_segmap(SP_pred.squeeze().numpy()))
                plt.axis('off')
                subplot_index = subplot_index + 1
                
                ax = fig.add_subplot(2, n_plot,subplot_index, aspect='equal')
                ax.set(title="SP_error")
                plt.imshow(view_error(SP_pred.squeeze().numpy(),labels.squeeze().cpu().numpy()))
                plt.axis('off')
                subplot_index = subplot_index + 1

            plt.savefig("view_" + str(i) + ".png")
            plt.close()
        if "scale_SuperPixels" in cfg["model"] and cfg["model"]["scale_SuperPixels"]!=0: 
            measur_min = (IoU_SP - IoU)
        else:
            measur_min = IoU
        if min_gain>measur_min:
            min_gain = measur_min
            img_visual = images.clone()
            lbl_visual = labels.clone()
            pred_visual = np.copy(pred)
            if "scale_SuperPixels" in cfg["model"] and cfg["model"]["scale_SuperPixels"]!=0:
                SP_map_visual = SP_maps.clone()
                SP_pred_visual = SP_pred.clone()
                SP_maps = SP_maps.squeeze()
                SP_bord = torch.logical_not(torch.logical_or(torch.abs(SP_maps - torch.roll(SP_maps,shifts=(0,1),dims=(0,1))),torch.abs(SP_maps - torch.roll(SP_maps,shifts=(1,0),dims=(0,1))))).squeeze()*1
                SP_bord_visual = SP_bord.clone()
    
    
    print("worse measure : ",min_gain.item())
    logger.info("worse measure: {}".format(min_gain.item()))
    n_plot = 4
    if "scale_SuperPixels" in cfg["model"] and cfg["model"]["scale_SuperPixels"]!=0:
        n_variation = 2
    else:
        n_variation = 1
    fig = plt.figure(figsize=(n_plot * 5, n_variation  * 5), dpi = 500)

    subplot_index = 1
    ax = fig.add_subplot(n_variation, n_plot, subplot_index, aspect='equal')
    ax.set(title="RGB")
    plt.imshow(((img_visual-torch.min(img_visual))/(torch.max(img_visual)-torch.min(img_visual))).squeeze().permute(1,2,0).cpu())
    plt.axis('off')
    subplot_index = subplot_index + 1
    
    ax = fig.add_subplot(n_variation, n_plot, subplot_index, aspect='equal')
    ax.set(title="label")
    plt.imshow(v_loader.decode_segmap(lbl_visual.squeeze().cpu().numpy()))
    plt.axis('off')
    subplot_index = subplot_index + 1

    ax = fig.add_subplot(n_variation, n_plot, subplot_index, aspect='equal')
    ax.set(title="pred")
    plt.imshow(v_loader.decode_segmap(pred_visual.squeeze()))
    plt.axis('off')
    subplot_index = subplot_index + 1
    
    ax = fig.add_subplot(n_variation, n_plot, subplot_index, aspect='equal')
    ax.set(title="error")
    plt.imshow(view_error(pred_visual.squeeze(),lbl_visual.squeeze().cpu().numpy()))
    plt.axis('off')
    subplot_index = subplot_index + 1

    if "scale_SuperPixels" in cfg["model"] and cfg["model"]["scale_SuperPixels"]!=0:
        ax = fig.add_subplot(n_variation, n_plot, subplot_index, aspect='equal')
        ax.set(title="SP_RGB")
        plt.imshow((((img_visual-torch.min(img_visual))/(torch.max(img_visual)-torch.min(img_visual)))*SP_bord_visual).squeeze().permute(1,2,0).cpu())
        plt.axis('off')
        subplot_index = subplot_index + 1
        
        ax = fig.add_subplot(n_variation, n_plot,subplot_index, aspect='equal')
        ax.set(title="SP_label")
        plt.imshow((v_loader.decode_segmap(lbl_visual.squeeze().cpu().numpy())))
        plt.axis('off')
        subplot_index = subplot_index + 1

        ax = fig.add_subplot(n_variation, n_plot, subplot_index, aspect='equal')
        ax.set(title="SP_pred")
        plt.imshow(v_loader.decode_segmap(SP_pred_visual.squeeze().numpy()))
        plt.axis('off')
        subplot_index = subplot_index + 1
        
        ax = fig.add_subplot(n_variation, n_plot,subplot_index, aspect='equal')
        ax.set(title="SP_error")
        plt.imshow(view_error(SP_pred_visual.squeeze().numpy(),lbl_visual.squeeze().cpu().numpy()))
        plt.axis('off')
        subplot_index = subplot_index + 1

    plt.savefig("view_worse_" + str(min_gain.item()) + ".png")
    plt.close()


    print("val average loss : ",val_loss_meter.avg)
    print("val average time : ",val_time_meter.avg)
    score, class_iou = running_metrics_val.get_scores()
    for k, v in score.items():
        print(k, v)
        logger.info("{}: {}".format(k, v))
        writer.add_scalar("val_metrics/{}".format(k), v, i + 1)
    
    for k, v in class_iou.items():
        logger.info("{}: {}".format(k, v))
        writer.add_scalar("val_metrics/cls_{}".format(k), v, i + 1)

    if "scale_SuperPixels" in cfg["model"] and cfg["model"]["scale_SuperPixels"]!=0:
        score, class_iou = running_metrics_val_SP.get_scores()
        for k, v in score.items():
            print(k, v)
            logger.info("{}: {}".format(k, v))
            writer.add_scalar("val_metrics/{}".format(k), v, i + 1)
        print("nb gain : {}; ratio gain : {}".format(count_gain,count_gain/average_gain.count))
        print("mean gain : {}; std gain : {}".format(average_gain.avg,average_gain.std))
        logger.info("nb gain : {}; ratio gain : {}".format(count_gain,count_gain/average_gain.count))
        logger.info("mean gain : {}; std gain : {}".format(average_gain.avg,average_gain.std))

        for k, v in class_iou.items():
            logger.info("{}: {}".format(k, v))
            writer.add_scalar("val_metrics/cls_{}".format(k), v, i + 1)

    val_loss_meter.reset()
    running_metrics_val.reset()
    
    return model#,model2,optimizer,scheduler
    

def train(cfg, writer, logger):

    # Setup seeds
    torch.manual_seed(cfg.get("seed", 1337))
    torch.cuda.manual_seed(cfg.get("seed", 1337))
    np.random.seed(cfg.get("seed", 1337))
    random.seed(cfg.get("seed", 1337))

    # Setup device
    device = torch.device("cuda:"+str(cfg["training"]["device"]-1) if torch.cuda.is_available() else "cpu")
    torch.cuda.set_device(device)

    # Setup Augmentations
    augmentations = cfg["training"].get("augmentations", None)
    data_aug = get_composed_augmentations(augmentations)

    # Setup Dataloader
    data_loader = get_loader(cfg["data"]["dataset"])
    data_path = cfg["data"]["path"]
   
    if "scribble_path" in cfg["data"]:
        scribble_path = cfg["data"]["scribble_path"]
        length_scribble = cfg["data"]["length_scribble"]
    else:
        scribble_path = None
        length_scribble = None

    if "scale_SuperPixels" in cfg["model"]:
        scale_SuperPixels = cfg["model"]["scale_SuperPixels"]
    else:
        scale_SuperPixels = None

    t_loader = data_loader(
        data_path,
        sbd_path = cfg["data"]["sbd_path"],
        scribble_path = scribble_path,
        is_transform=True,
        split=cfg["data"]["train_split"],
        img_size=(cfg["data"]["img_rows"], cfg["data"]["img_cols"]),
        augmentations=data_aug,
        length_scribble = length_scribble,
        scale_SuperPixels = scale_SuperPixels,
    )

    v_loader = data_loader(
        data_path,
        sbd_path = cfg["data"]["sbd_path"],
        scribble_path = scribble_path,
        is_transform=True,
        split=cfg["data"]["val_split"],
        img_size=(cfg["data"]["img_rows"], cfg["data"]["img_cols"]),
        length_scribble = length_scribble,
        scale_SuperPixels = scale_SuperPixels,
    )

    n_classes = t_loader.n_classes
    
    if cfg["training"]["batch_size"] != 1:
        collate_fn = collate_crop_small
    else:
        collate_fn = None

    trainloader = data.DataLoader(
        t_loader,
        batch_size=cfg["training"]["batch_size"],
        num_workers=0,#cfg["training"]["n_workers"],
        collate_fn = collate_fn,
        shuffle=True,
    )

    valloader = data.DataLoader(
        v_loader, batch_size=1, num_workers=cfg["training"]["n_workers"]
    )

    # Setup Metrics
    running_metrics_val = runningScore(n_classes)

    # Setup Model
    model = get_model(cfg["model"], n_classes).to(device)
    device_ids = [cfg["training"]["device"]-1]
    model = torch.nn.DataParallel(model, device_ids=device_ids)
    
    # Setup optimizer, lr_scheduler and loss function
    optimizer_cls = get_optimizer(cfg)
    optimizer_params = {k: v for k, v in cfg["training"]["optimizer"].items() if k != "name"}

    optimizer = optimizer_cls(model.parameters(), **optimizer_params)
    logger.info("Using optimizer {}".format(optimizer))

    scheduler = get_scheduler(optimizer, cfg["training"]["lr_schedule"])

    loss_fn = get_loss_function(cfg)
    logger.info("Using loss {}".format(loss_fn))

    start_iter = 0
    if cfg["training"]["resume"] is not None:
        if os.path.isfile(cfg["training"]["resume"]):
            print("resume file found")
            logger.info(
                "Loading model and optimizer from checkpoint '{}'".format(cfg["training"]["resume"])
            )
            checkpoint = torch.load(cfg["training"]["resume"])
            model.load_state_dict(checkpoint["model_state"])
            optimizer.load_state_dict(checkpoint["optimizer_state"])
            scheduler.load_state_dict(checkpoint["scheduler_state"])
            start_iter = checkpoint["epoch"]
            logger.info(
                "Loaded checkpoint '{}' (iter {})".format(
                    cfg["training"]["resume"], checkpoint["epoch"]
                )
            )
        else:
            logger.info("No checkpoint found at '{}'".format(cfg["training"]["resume"]))

    val_loss_meter = averageMeter()
    time_meter = averageMeter()

    best_iou = -100.0
    i = start_iter
    flag = True
    count_converge = 0

    while i <= cfg["training"]["train_iters"] and flag:
        for (images, labels) in trainloader:
            # image init
            i += 1
            start_ts = time.time()
            model.train()
            images = images.to(device)
            labels = labels.to(device)
            
            # prediction
            optimizer.zero_grad()
            if "nb_it_start" in cfg["model"]:
                outputs = model(images,i)
            else:
                outputs = model(images)
            
            # compute loss
            loss = loss_fn(input=outputs, target=labels)

            # optimise model
            loss.backward()
            optimizer.step()
            scheduler.step()

            time_meter.update(time.time() - start_ts)

            if (i + 1) % cfg["training"]["print_interval"] == 0:
                fmt_str = "Iter [{:d}/{:d}]  Loss: {:.4f}  Time/Image: {:.4f}"
                print_str = fmt_str.format(
                    i + 1,
                    cfg["training"]["train_iters"],
                    loss.item(),
                    time_meter.avg / cfg["training"]["batch_size"],
                )

                print(print_str)
                logger.info(print_str)
                writer.add_scalar("loss/train_loss", loss.item(), i + 1)
                time_meter.reset()

            if (i + 1) % cfg["training"]["val_interval"] == 0 or (i + 1) == cfg["training"][
                "train_iters"
            ]:
                model.eval()
                with torch.no_grad():
                    for i_val, (images_val, labels_val) in tqdm(enumerate(valloader)):
                        images_val = images_val.to(device)
                        labels_val = labels_val.to(device)
                        if "nb_it_start" in cfg["model"]:
                            outputs = model(images_val,i)
                        else:
                            outputs = model(images_val)
                        val_loss = loss_fn(input=outputs, target=labels_val)

                        pred = outputs.data.max(1)[1].cpu().numpy()
                        gt = labels_val.data.cpu().numpy()

                        running_metrics_val.update(gt, pred)
                        val_loss_meter.update(val_loss.item())

                writer.add_scalar("loss/val_loss", val_loss_meter.avg, i + 1)
                logger.info("Iter %d Loss: %.4f" % (i + 1, val_loss_meter.avg))

                score, class_iou = running_metrics_val.get_scores()
                for k, v in score.items():
                    print(k, v)
                    logger.info("{}: {}".format(k, v))
                    writer.add_scalar("val_metrics/{}".format(k), v, i + 1)

                for k, v in class_iou.items():
                    logger.info("{}: {}".format(k, v))
                    writer.add_scalar("val_metrics/cls_{}".format(k), v, i + 1)

                val_loss_meter.reset()
                running_metrics_val.reset()

                if score["Mean IoU : \t"] >= best_iou:
                    count_converge = 0
                    best_iou = score["Mean IoU : \t"]
                    state = {
                        "epoch": i + 1,
                        "model_state": model.state_dict(),
                        "optimizer_state": optimizer.state_dict(),
                        "scheduler_state": scheduler.state_dict(),
                        "best_iou": best_iou,
                    }
                    save_path = os.path.join(
                        writer.file_writer.get_logdir(),
                        "{}_{}_best_model.pkl".format(cfg["model"]["arch"], cfg["data"]["dataset"]),
                    )
                    torch.save(state, save_path)
                else:
                    count_converge = count_converge + 1

            if (i + 1) == cfg["training"]["train_iters"] or count_converge >= cfg["training"]["limit_converge"]:
                flag = False
                break


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="config")
    parser.add_argument(
        "-cf",
        "--config",
        nargs="?",
        type=str,
        default="configs/fcn8s_pascal.yml",
        help="Configuration file to use",
    )

    parser.add_argument(
        "-m",
        "--mode",
        nargs="?",
        type=str,
        default="train",
        help="choose val or train mode, default: train",
    )

    parser.add_argument(
        "-mp",
        "--model_path",
        nargs="?",
        type=str,
        default=None,
        help="Path to the saved model",
    )

    args = parser.parse_args()

    with open(args.config) as fp:
        cfg = yaml.safe_load(fp)

    run_id = time.asctime().replace(" ","_").replace(":","-")
    logdir = os.path.join("runs", os.path.basename(args.config)[:-4], str(run_id))
    writer = SummaryWriter(log_dir=logdir)

    print("RUNDIR: {}".format(logdir))
    shutil.copy(args.config, logdir)

    logger = get_logger(logdir)
    logger.info("Let the games begin")
    
    if args.mode == "val":
        model = val(cfg) #,model2,optimizers,scheduler = val(cfg)
    else:
        train(cfg, writer, logger)
