from ptsemseg.loader import get_loader
from ptsemseg.augmentations import get_composed_augmentations
import yaml
from torch.utils import data
import torch

fp = open("configs/fcn8s_pascal.yml")
cfg = yaml.safe_load(fp)
data_loader = get_loader(cfg["data"]["dataset"])
data_path = cfg["data"]["path"] 
data_aug = get_composed_augmentations(cfg["training"].get("augmentation",None))

t_loader = data_loader(data_path, sbd_path = cfg["data"]["sbd_path"],is_transform = True,split=cfg["data"]["train_split"],img_size=(cfg["data"]["img_rows"],cfg["data"]["img_cols"]),augmentations=data_aug)

n_classes = t_loader.n_classes

train_loader = data.DataLoader(t_loader,batch_size=cfg["training"]["batch_size"],num_workers=cfg["training"]["n_workers"],shuffle=False)

#pixels_classes = torch.zeros([9000,21])
i = 0
mean_x=0
mean_y=0
max_x=0
min_x=1000
max_y=0
min_y=1000
n_class = []
#n_class_1000 = 0
for (images,labels) in train_loader:
    i=i+1
    mean_x = mean_x + images.shape[2]
    mean_y = mean_y + images.shape[3]
    max_x = max(images.shape[2],max_x)
    min_x = min(images.shape[2],min_x)
    max_y = max(images.shape[3],max_y)
    min_y = min(images.shape[3],min_y)
    #unique,count = torch.unique(labels,return_counts=True)
    #n_class.append(unique.numel())
    #n_class_1000 = n_class_1000 + unique[count>1000].numel()
    #pixels_classes[i,unique] = count.float()
    if i%100 == 0:
        print(i)
#n_class = torch.tensor(n_class).float()
#print("mean n class : ",n_class.mean())
#print("std n class : ",n_class.std())
#print("mean n class 1000 : ",n_class_1000/i)
print("mean x : ",mean_x/i)
print("mean y : ",mean_y/i)
print("max x : ",max_x)
print("min x : ",min_x)
print("max y : ",max_y)
print("min y : ",min_y)

