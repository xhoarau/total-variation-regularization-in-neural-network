import csv
import argparse

parser = argparse.ArgumentParser(description='take the file log path and put PA and IoU in csv')
parser.add_argument("-pl","--pathtolog",required = True)
parser.add_argument("-fn","--filename",required = True)
args = parser.parse_args()

f_log = open(args.pathtolog,"r")
f_csv = open(args.filename,"w",newline='')
writer = csv.writer(f_csv)
writer.writerow(["Num iteration","time/img"])
iteration = 0
for row in f_log:
    old_iteration = iteration
    
    idx = row.find("Time")
    if idx!=-1:
        idx_it = row.find("[")+1
        idx_end_it = row.find("/",idx_it)
        it = row[idx_it:idx_end_it]
        idx_val = row.find(":",idx)+2
        Time = row[idx_val:idx_val+6]
        iteration = iteration+1
    
    if iteration!=old_iteration:
        writer.writerow([it,Time])

f_log.close()
f_csv.close()
