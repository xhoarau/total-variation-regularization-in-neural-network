import numpy as np

def graph_dist_unique(dist,connect,grid):
    """
    calcul l'ensemble des aretes (paire de points) dans une grille
    dist = nombre>=0 (distance entre les points : norm_inf = 2^dist)
    connect = nombre in [0,dist+1] 
        densité des connextions:
            -1 : seulement selon les directions principales
            n : tout les point dont les coordonées sont composées de 0 et de multiple de 2^dist/2^(n)
    grid = numpy array (dimention de la grille)
    edge = numpy array (output, ensemble des paires relier)
    """
    dist_val = np.power(2,dist) # value of the distance (norm inf)
    dim = len(grid) # number of dimention of the grid
    assert dist>=0,"dist is strictly positive"
    assert connect<=dist and connect>=-1,"connect is between -1 and dist"
    if connect == -1:
        ensemble = np.diag(np.full(dim,dist_val)) # relative coordinate in all pricipal direction
    else:
        list_element = np.arange(-dist_val,dist_val+0.1,dist_val/np.power(2,connect),dtype=int) # values of the relative coordinate
        ensemble = np.array(np.meshgrid(*[list_element for i in range(dim)])).reshape([dim,np.power(len(list_element),2)]).transpose() # all relative coordinate
        ensemble = ensemble[ensemble[np.arange(ensemble.shape[0]),np.argmin(ensemble==0,axis=1)]>0] # keep only edges if the first non null valu is positive (non directional edges) 
    grid_co = np.array(np.meshgrid(*[np.arange(grid[i]) for i in range(dim)])).reshape([dim,grid.prod()]).transpose() # all coordinate in the grid
    end_co = np.repeat(grid_co,ensemble.shape[0],axis=0)+np.tile(ensemble,(grid.prod(),1)) # all edges ending
    edge = np.stack((np.repeat(grid_co,ensemble.shape[0],axis=0),end_co),axis=1) # all edges as point to point
    edge = edge[np.all(np.logical_and(edge<grid,edge>=0),axis=(1,2))] # remove edges that goes outside of the grid
    connectivitie = np.linalg.norm(edge[:,0,:]-edge[:,1,:],2,axis=1) # euclidean lenth of the edges
    edge = np.sum(np.multiply(edge,np.cumprod(np.insert(grid,0,1))[:-1]),axis=2) # code edges in numero of points
    return edge,connectivitie

def graph_dist(dist,connect,grid):
  """
  calcul l'ensemble des aretes (paire de points) dans une grille
  dist = numpy array (list des distances à créer, cf. graph_dist_unique)
  connect = nombre (densité maximal des connextions, cf. graph_dist_unique)
            numpy array (de même dimention que dist, densité de connextion pour chaque distance)
  grid = numpy array (dimention de la grille)
  edge = numpy array (output)
  """
  assert len(dist.shape) == 1,"dist is a 1D numpy array"
  assert len(connect.shape)==1 and (connect.shape[0]==1 or connect.shape[0]==dist.shape[0]),"connect is a 1D numpy array with same size as dist or of size 1"
  assert len(grid.shape) == 1,"grid is a 1D numpy array"
  connect = np.minimum(connect,dist)
  edges,connectivities = graph_dist_unique(dist[0],connect[0],grid)
  for d in range(1,dist.shape[0]):
    n_edge,n_connectivitie = graph_dist_unique(dist[d],connect[d],grid)
    edges = np.concatenate([edges,n_edge])
    connectivities = np.concatenate([connectivities,n_connectivitie])
  return edges.astype(np.uint32),connectivities
