import pickle
import modules.storage_class as sc
import numpy as np
import torch

def save(var,filename):
  f = open(filename,"wb")
  pickle.dump(var,f)
  f.close

def load(filename):
  f = open(filename,"rb")
  var = pickle.load(f)
  f.close()
  return var

