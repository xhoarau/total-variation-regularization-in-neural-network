# full assembly of the sub-parts to form the complete net

# sub-parts of the U-Net model

import torch
import torch.nn as nn
import torch.nn.functional as F
import sys

# dev = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

class double_conv(nn.Module):
    '''(conv => BN => ReLU) * 2'''
    def __init__(self, in_ch, out_ch):
        super(double_conv, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(in_ch, out_ch, 3, padding=1),
            nn.BatchNorm2d(out_ch),
            nn.ReLU(inplace=True),
            nn.Conv2d(out_ch, out_ch, 3, padding=1),
            nn.BatchNorm2d(out_ch),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        x = self.conv(x)
        return x


class inconv(nn.Module):
    def __init__(self, in_ch, out_ch):
        super(inconv, self).__init__()
        self.conv = double_conv(in_ch, out_ch)

    def forward(self, x):
        x = self.conv(x)
        return x


class down(nn.Module):
    def __init__(self, in_ch, out_ch):
        super(down, self).__init__()
        self.nbChannelsIn = in_ch
        self.nbChannelsOut = out_ch
        self.mpconv = nn.Sequential(
            nn.MaxPool2d(2),
            double_conv(in_ch, out_ch)
        )

    def forward(self, x):
        x = self.mpconv(x)
        return x


class up(nn.Module):
    def __init__(self, in_ch, out_ch, bilinear=True):
        super(up, self).__init__()
        self.nbChannelsIn = in_ch
        self.nbChannelsOut = out_ch
        
        if bilinear:
            self.up = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
        else:
            self.up = nn.ConvTranspose2d(in_ch//2, in_ch//2, 2, stride=2)

        self.conv = double_conv(in_ch, out_ch)

    def forward(self, x1, x2):
        x1 = self.up(x1)

        diffX = x1.size()[2] - x2.size()[2]
        diffY = x1.size()[3] - x2.size()[3]
        x2 = F.pad(x2, (diffX // 2, int(diffX / 2),
                        diffY // 2, int(diffY / 2)))
        x = torch.cat([x2, x1], dim=1)
        x = self.conv(x)
        return x


class outconv(nn.Module):
    def __init__(self, in_ch, out_ch):
        super(outconv, self).__init__()
        self.conv = nn.Conv2d(in_ch, out_ch, 1)

    def forward(self, x):
        x = self.conv(x)
        return x

class UNet(nn.Module):
    def __init__(self, nbclasses, nbfeaturesbase):
        super(UNet, self).__init__()
        self.inc = inconv(3, nbfeaturesbase)
        self.down1 = down(nbfeaturesbase, nbfeaturesbase*2)
        self.down2 = down(nbfeaturesbase*2, nbfeaturesbase*2)
        #self.down3 = down(256, 256)
        # self.down4 = down(512, 512)
        # self.up1 = up(1024, 256)
        #self.up2 = up(512, 128)
        self.up3 = up(nbfeaturesbase*4, nbfeaturesbase)
        self.up4 = up(nbfeaturesbase*2, nbfeaturesbase)
        self.outc = outconv(nbfeaturesbase, nbclasses)

    def forward(self, x):
        x1 = self.inc(x)
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        # x4 = self.down3(x3)
        # x5 = self.down4(x4)

        # print('x=', x.shape, 'x1=', x1.shape, 'x2=', x2.shape)
        # print('x3=', x3.shape, 'x4=', x4.shape, 'x5=', x5.shape)
        # exit(0)

        # x = self.up1(x5, x4)
        # x = self.up2(x4, x3) #x = self.up2(x, x3)
        x = self.up3(x3, x2) #x = self.up3(x,x2)
        x = self.up4(x, x1)
        x = self.outc(x)
        return x


class UNetFlex(nn.Module):
    def __init__(self, nbclasses, nbfeaturesbase, nblevels, ratiofeatures):
        super(UNetFlex, self).__init__()
        self.inc = inconv(3, nbfeaturesbase)
        self.outc = outconv(nbfeaturesbase, nbclasses)

        self.downLayers = []
        self.upLayers = []

        if nblevels>0:
            for l in range(nblevels-1):
                self.downLayers.append(down(int(nbfeaturesbase*(ratiofeatures**l)), int(nbfeaturesbase*(ratiofeatures**(l+1)))))
            self.downLayers.append(down(int(nbfeaturesbase*(ratiofeatures**(nblevels-1))), int(nbfeaturesbase*(ratiofeatures**(nblevels-1)))))

            # self.upLayers.append(up(nbfeaturesbase*(ratiofeatures**nblevels), nbfeaturesbase*(ratiofeatures**(nblevels-2))))
            for l in range(nblevels-1):
                in_ch = self.downLayers[nblevels-1-l].nbChannelsOut + self.downLayers[nblevels-2-l].nbChannelsOut 
                self.upLayers.append(up(in_ch, self.downLayers[nblevels-2-l].nbChannelsOut))
            self.upLayers.append(up(nbfeaturesbase + self.downLayers[0].nbChannelsOut , nbfeaturesbase))

            # print('Creation')
            for i in range(len(self.downLayers)):
                # print('down', i, ' : in=', self.downLayers[i].nbChannelsIn, ' out=', self.downLayers[i].nbChannelsOut)
                self.add_module("DownLayer" + '{:01}'.format(i), self.downLayers[i])
            for i in range(len(self.upLayers)):
                # print('up', i, ' : in=', self.upLayers[i].nbChannelsIn, ' out=', self.upLayers[i].nbChannelsOut)
                self.add_module("UpLayer" + '{:01}'.format(i), self.upLayers[i])

    def forward(self, args, x):
        nblevels = len(self.downLayers)
        if nblevels>0:
            xs = []
            xs.append(self.inc(x))
            for l in range(nblevels):
                # print('down', l, ' : in=', self.downLayers[l].nbChannelsIn, ' out=', self.downLayers[l].nbChannelsOut, ' shape=', xs[-1].shape)
                xs.append(self.downLayers[l](xs[-1]))

            x = self.upLayers[0](xs[nblevels], xs[nblevels-1])
            for l in range(1,nblevels):
                x = self.upLayers[l](x, xs[nblevels-1-l])
            x = self.outc(x)
        else:
            x = self.outc(self.inc(x))
        return x