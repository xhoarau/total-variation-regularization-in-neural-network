import sys,os
import numpy as np
import torch

import modules.save_result as sr
import modules.graphdist as gd
if os.getenv('CONDA_DEFAULT_ENV')=='tv_opt':
    sys.path.append(os.path.expanduser('~')+"/these/tv_layers_for_cv/grid-graph/python/bin")
    from grid_graph import grid_to_graph
    from grid_graph import edge_list_to_forward_star
else:
    sys.path.append(os.path.expanduser('~')+"/these/grid-graph/python/bin")
    from grid_graph import grid_to_graph
    from grid_graph import edge_list_to_forward_star

class argument: #contient tout les paramètre et argument d'un réseau
  def __init__(self,n_epoch, batch_size, n_class, class_names, n_channels, cuda, lr, n_epoch_validation, equilibrage_classes, \
                  use_drop, coef_drop, scheduler_step, scheduler_gamma, refined_scheduler, retrain, model_name, blocage_reseau, \
                  set_name, prop_degrad, prop_img_train_set, \
                  land_scape, urban, square, fiftyone, resize, VOC, taille_ensemble,\
                  SegNet, conv_width, dilation, kernel_size, \
                  unetflex, nb_level, nbfeaturesbase, ratiofeatures, \
                  FCN, \
                  do_reg, blocage_epoch, TV, coef_TV, do_augment_front, \
                  Cut_Pursuit, Lambda_unique, gradient_analytique, graphe_tronque, diff_fini, \
                  nb_thread, do_use_connect_set, taille_graph, connectivity, graph_dist, List_Distance, max_dir, \
                  reg_sousgrad, coef_reg, blocage_reg, nb_feature, red_grad_lambda, taille_sous_graph_pixel, taille_sous_graph_arret, \
                  logit, couche_sup_lambda, Lambda_separe, conv_width_Lambda, Lambda_init, \
                  pretrain_Lambda, name_save_lambda, edge_detection, n_dilation, \
                  TV_layer, TVL_Lambda_unique, Multi_TV_layer, \
                  ):

    # caractéristique réseau
    self.n_epoch = n_epoch
    self.batch_size = batch_size
    self.n_class = n_class
    self.class_names = class_names
    self.n_channels = n_channels
    self.cuda = cuda
    self.weight_class = [1/n_class]*n_class
    self.lr = lr
    self.n_epoch_validation = n_epoch_validation
    self.equilibrage = np.zeros(self.n_class,dtype = int).tolist()
    self.equilibrage_classes = equilibrage_classes

    self.use_drop = use_drop
    self.coef_drop = coef_drop

    self.scheduler_step = scheduler_step*(not refined_scheduler) + refined_scheduler
    self.scheduler_gamma = scheduler_gamma
    self.refined_scheduler = refined_scheduler

    self.retrain = retrain
    self.model_name = model_name
    self.model = None
    self.blocage_reseau = blocage_reseau

    self.set_name = set_name
    self.prop_degrad = prop_degrad
    self.prop_img_train_set = prop_img_train_set

    self.land_scape = land_scape
    self.urban = urban

    self.square = square

    self.fiftyone = fiftyone
    self.resize = resize

    self.VOC = VOC
    self.taille_ensemble = taille_ensemble

    self.SegNet = SegNet
    self.conv_width = conv_width
    self.dilation = dilation
    self.kernel_size = kernel_size

    self.unetflex = unetflex
    self.nb_level = nb_level
    self.nbfeaturesbase = nbfeaturesbase
    self.ratiofeatures = ratiofeatures

    self.FCN = FCN

    self.do_reg = do_reg
    self.blocage_epoch = blocage_epoch
    self.step_1_end = (blocage_epoch != -1) # apprentissage sans régularisation dans un premier temps
    self.TV = TV
    self.coef_TV = coef_TV
    self.do_augment_front = do_augment_front
    self.facteur_front = 1

    self.Cut_Pursuit = Cut_Pursuit
    self.compte_composante = []
    self.Lambda_unique = Lambda_unique
    self.gradient_analytique = gradient_analytique
    self.graphe_tronque = graphe_tronque
    self.diff_fini = diff_fini

    self.nb_thread = nb_thread
    if not Lambda_unique:
      self.do_use_connect_set = do_use_connect_set
    else:
      self.do_use_connect_set = 0
    self.taille_graph = taille_graph
    
    if graph_dist:
      edges, connectivities = gd.graph_dist(np.array(List_Distance),max_dir,np.array(taille_graph,dtype='int32'))
    else:
      edges, connectivities = grid_to_graph(np.array(taille_graph,dtype='int32'), connectivity = connectivity, compute_connectivities = True, graph_as_forward_star = False, row_major_index = True)# calcul du graph des adjacance
    first_edges,adj_vertices, reindex = edge_list_to_forward_star(np.prod(taille_graph),edges)# changement de representation
    first_edges = first_edges.astype('uint32')
    adj_vertices = adj_vertices.astype('uint32')
    edges[reindex,:] = np.copy(edges)
    edges = torch.from_numpy(edges.astype("int64")).cuda()
    connectivities[reindex] = np.copy(connectivities)
    connectivities = torch.sqrt(torch.from_numpy(connectivities).cuda())
    self.edges = edges
    self.adj_vertices = adj_vertices
    self.first_edges = first_edges
    self.connectivities = connectivities

    self.reg_sousgrad = reg_sousgrad
    self.coef_reg = coef_reg
    self.blocage_reg = blocage_reg
    if do_reg and not Lambda_separe and not Lambda_unique:
      self.nb_feature = nb_feature
    else:
      self.nb_feature = 1
    self.red_grad_lambda = red_grad_lambda
    self.taille_sous_graph_pixel = taille_sous_graph_pixel
    self.taille_sous_graph_arret = taille_sous_graph_arret

    if edge_detection or pretrain_Lambda:
      self.logit = False
      self.couche_sup_lambda = False
      self.Lambda_separe = False
    else:
      self.logit = logit
      self.couche_sup_lambda = couche_sup_lambda
      self.Lambda_separe = Lambda_separe
    if not pretrain_Lambda:
      self.conv_width_Lambda = conv_width_Lambda
    self.Lambda_init = Lambda_init

    self.pretrain_Lambda = pretrain_Lambda
    self.name_save_lambda = name_save_lambda
    self.edge_detection = edge_detection
    self.n_dilation = n_dilation

    self.TV_layer = TV_layer
    self.TVL_Lambda_unique = TVL_Lambda_unique
    self.Multi_TV_layer = Multi_TV_layer

    self.delta = 1e13
    

class result: #contien les resultats de l'apprentissage
  def __init__(self,trained_model,evol_train_TV,evol_valid_TV,evol_test_TV,evol_composante,duree_entrainement,epoch_save,epoch_regularisation):
    self.trained_model = trained_model
    self.evol_train_TV = evol_train_TV
    self.evol_valid_TV = evol_valid_TV
    self.evol_test_TV = evol_test_TV
    self.evol_composante = evol_composante
    self.duree_entrainement = duree_entrainement
    self.epoch_save = epoch_save
    self.epoch_regularisation = epoch_regularisation

class result_multi: #contien les resultats de l'apprentissage
  def __init__(self,List_trained_model,List_evol_train_TV,List_evol_valid_TV,List_evol_test_TV,List_evol_composante,List_temps_entrainement,List_epoch_valid,List_weight_class,List_facteur_front,List_epoch_regularisation):
    self.List_trained_model = List_trained_model
    self.List_evol_train_TV = List_evol_train_TV
    self.List_evol_valid_TV = List_evol_valid_TV
    self.List_evol_test_TV = List_evol_test_TV
    self.List_evol_composante = List_evol_composante
    self.List_temps_entrainement = List_temps_entrainement
    self.List_epoch_valid = List_epoch_valid
    self.List_weight_class = List_weight_class
    self.List_facteur_front = List_facteur_front
    self.List_epoch_regularisation = List_epoch_regularisation

class result_Lambda: #contien les resultats de l'apprentissage
  def __init__(self,trained_Lambda,evol_train_Lambda,evol_test_Lambda,evol_valid_Lambda,epoch_save_Lambda,duree_entrainement):
    self.trained_Lambda = trained_Lambda
    self.evol_train_Lambda = evol_train_Lambda
    self.evol_valid_Lambda = evol_valid_Lambda
    self.evol_test_Lambda = evol_test_Lambda
    self.duree_entrainement = duree_entrainement
