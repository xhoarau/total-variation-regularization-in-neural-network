import fiftyone as fo
import fiftyone.zoo as foz
import numpy as np
import torch
import torchvision.transforms as T
from torchvision.transforms import functional as F
import torchvision.datasets as D
import torchnet as tnt
import h5py
from functools import partial
import os

from PIL import Image

#############################################################
# data loader
#############################################################

def tile_loader(tile_index, set_obs, set_gt, set_connect, set_gt_complet, args):
  """
  load a tile and returns the observation and associated ground truth
  INPUT:
  tile_index = int, index of the tile
  train = bool, train = True if in the train set
  OUTPUT
  obs, [4x256 x 256] float Tensor containing the observation
  gt, [256 x 256] long Tensor, containing the pixels semantic labels
  """
  obs = set_obs[tile_index,:,:,:].transpose(2,0,1) #put channels first
  gt = set_gt[tile_index,:,:]
  if args.do_use_connect_set != 0 or args.edge_detection:
    assert set_connect is not None
    connect = set_connect[tile_index,:]
  if set_gt_complet is not None:
    gt_complet = set_gt_complet[tile_index,:,:]

  if args.n_class == 2:
    if not hasattr(args,"square") or not args.square:
      if args.urban:
        gt = 1*(gt==0)+1*(gt==3)+1*(gt==5)+99*(gt==99) # transformation en modèle binaire
      else:
        gt = 1*(gt==3)+99*(gt==99) # transformation en modèle binaire (route non route
  else:
    assert args.n_class == 6
  
  #create torch tensors
  obs = torch.from_numpy(obs)
  gt = torch.from_numpy(gt)
  if args.do_use_connect_set != 0 or args.edge_detection:
    connect = torch.from_numpy(connect)
  else:
    connect = torch.tensor(0)
  if set_gt_complet is not None:
    gt_complet = torch.from_numpy(gt_complet)
  else:
    gt_complet = torch.tensor(0)

  return obs, gt.long(), connect, gt_complet.long() #ground truth has to be long int type

def gene_connect_set(gt,edges):
  gt = torch.from_numpy(1*(gt==0)+1*(gt==3)+1*(gt==5)+99*(gt==99))
  edges = edges.cpu()
  connect_set = -1*(torch.multiply(gt[edges[:,0]] == 99,gt[edges[:,1]] == 99)) + 1 * torch.multiply(torch.multiply(gt[edges[:,0]] != 99, gt[edges[:,1]] != 99), gt[edges[:,0]] == gt[edges[:,1]])
  return np.array(connect_set)

#############################################################
#Data loader Fiftyone
#############################################################

def fifty_loader(sample_id,dataset,args):
  
    sample = dataset[sample_id]
    obs = Image.open(sample.filepath)
    if args.resize:
        img_resize = T.Compose([
            T.Resize(args.taille_graph),
            T.ToTensor(),
        ])
        ten_resize=T.Compose([
            T.ToPILImage(),
            T.Resize(args.taille_graph,interpolation=T.InterpolationMode.NEAREST),
            T.ToTensor(),
        ])

        obs = img_resize(obs)#torch.permute(torch.from_numpy(np.array(obs)/255.),(2,0,1))
        gt = (ten_resize(torch.from_numpy(sample.segmentations.mask).float()/(np.max(sample.segmentations.mask)))*(np.max(sample.segmentations.mask))).squeeze().round().long()
        assert not torch.any(torch.isnan(obs)) and not torch.any(torch.isnan(gt))
    else:
        obs = torch.from_numpy(np.array(obs)).permute(2,0,1)
        gt = torch.from_numpy(sample.segmentations.mask).long()

    connect = torch.tensor(0)
    if hasattr(sample,"save_mask") and sample.save_mask is not None:
        if args.resize:
            gt_complet = (ten_resize(torch.from_numpy(sample.save_mask).float()/(np.max(sample.save_mask)))*(np.max(sample.save_mask))).squeeze().round().long()
        else:
            gt_complet = torch.from_numpy(sample.save_mask).long()
    else:
        gt_complet = torch.tensor(0)
    
    return obs.float(), gt, connect, gt_complet.long() #ground truth has to be long int type
#############################################################
#Data loader Pascal VOC
#############################################################

def pad_if_smaller(img,size,fill = 0):
  ow, oh = img.size
  padh = size[0] - oh if oh < size[0] else 0
  padw = size[1] - ow if ow < size[1] else 0
  img = F.pad(img, (0, 0, padw, padh), fill=fill)
  return img

def VOC_loader(sample_id,dataset,args):
  obs = Image.open(dataset.images[sample_id])
  gt = Image.open(dataset.masks[sample_id])

  obs = pad_if_smaller(obs,args.taille_graph)
  gt = pad_if_smaller(gt,args.taille_graph)
  crop_params = T.RandomCrop.get_params(obs, args.taille_graph)
  obs = F.crop(obs, *crop_params)
  gt = F.crop(gt, *crop_params)

  obs = torch.from_numpy(np.array(obs)).permute(2,0,1).float()/255
  gt = torch.from_numpy(np.array(gt)).long()
  if torch.all(torch.unique(gt) == 255):
    gt = (gt == 255) * 0 +  (gt != 255) * gt
  else: 
    gt = (gt == 255) * 99 +  (gt != 255) * gt
  gt = (gt > args.n_class-1) * 0 + (gt <= args.n_class-1) * gt

  if sample_id < args.taille_ensemble[0]:
    mask = torch.rand(gt.shape)
    for c in range(args.n_class):
      gt = (gt == c) * (mask<args.equilibrage[c]) * 99 + (gt == c) * (mask>=args.equilibrage[c]) * c + (gt != c) * gt

  connect = torch.tensor(0)
  gt_complet = torch.tensor(0)

  return obs, gt, connect, gt_complet.long()


#############################################################
#Insertion des données
#############################################################
def creation_set(args, validation = True, rand_choice = True):
  """
  load the data and create the training, testing and validation set
  INPUT:
  args(class argument) : hold the parameters of the model
    proportion of degradation
    nb img for training
    number of class
    type of regroupement (urban/non urban or road/not road)
  rand_choice(booléen) : if True the training and validation set are randomly created
    if False : the training and the validation always take the same images
  OUTPUT:
  test_set, obs and gt *100
  train_set, obs and gt *nb img for training (max = 160)
  validation_set, obs ad gt *40
  """
  if hasattr(args,"VOC") and args.VOC:
    dataset = D.VOCSegmentation("../PascalVOC","2012","trainval")
    train_List=np.arange(args.taille_ensemble[0]).tolist()
    validation_List=(np.arange(args.taille_ensemble[1])+args.taille_ensemble[0]).tolist()
    test_List=(np.arange(args.taille_ensemble[2])+args.taille_ensemble[0]+args.taille_ensemble[1]).tolist()

    test_set  = tnt.dataset.ListDataset(test_List,partial(VOC_loader, dataset = dataset, args = args))
    validation_set = tnt.dataset.ListDataset(validation_List,partial(VOC_loader, dataset = dataset, args = args))
    train_set = tnt.dataset.ListDataset(train_List,partial(VOC_loader, dataset = dataset, args = args))

    n_test = len(test_List)
    n_train = len(train_List)
    n_validation = len(validation_List)

  else:

    if hasattr(args,'fiftyone') and args.fiftyone:
      
      assert args.set_name in fo.list_datasets()
      dataset = fo.load_dataset(args.set_name)
      pb = fo.ProgressBar()
      train_List=[]
      validation_List=[]
      test_List=[]
      for sample in pb(dataset):
        if sample.tags[0] == 'train':
          train_List.append(sample.id)
        else:
            if sample.tags[0] == 'validation':
                validation_List.append(sample.id)
            else:
                test_List.append(sample.id)

      train_List = train_List[:int(len(train_List)*args.prop_img_train_set)]
      
      test_set  = tnt.dataset.ListDataset(test_List,partial(fifty_loader, dataset = dataset, args = args))
      validation_set = tnt.dataset.ListDataset(validation_List,partial(fifty_loader, dataset = dataset, args = args))
      train_set = tnt.dataset.ListDataset(train_List,partial(fifty_loader, dataset = dataset, args = args))
      
      n_test = len(test_List)
      n_train = len(train_List)
      n_validation = len(validation_List)

    else:
      #ouverture du fichier
      if hasattr(args,'set_name'):
        filename = args.set_name + ".hdf5"
      else:
        filename = "land_cover.hdf5"
      print("name data file : ",filename)
      if os.path.isfile(filename):
        data_file = h5py.File(filename,'r')
      else:
        if hasattr(args,'set_name'):
          filename = os.path.join(os.getcwd(),os.pardir,args.set_name + ".hdf5")
        else:
          filename = os.path.join(os.getcwd(),os.pardir,"land_cover.hdf5")

        if os.path.isfile(filename):
          data_file = h5py.File(filename,'r')
        else:
          assert False and "pas de fichier de donnée"
    
      # récuperation des données
      train_obs = data_file['train_observation'][:]
      train_gt = data_file['train_gt'][:]
      test_obs = data_file['test_observation'][:]
      test_gt = data_file['test_gt'][:]

      n_train = train_obs.shape[0]
      n_test = test_obs.shape[0]

      if args.square :
          if all(args.connectivities == 1):
              try:
                  data_file['train_connect1_gt']
              except:
                  print("data train_connect1_gt not available")
                  train_connect_gt = None
              else:
                  train_connect_gt = data_file['train_connect1_gt'][:]
              try:
                  data_file['test_connect1_gt']
              except:
                  print("data test_connect1_gt not available")
                  test_connect_gt = None
              else:
                  test_connect_gt = data_file['test_connect1_gt'][:]
          else:
              try:
                  data_file['train_connect2_gt']
              except:
                  print("data train_connect2_gt not available")
                  train_connect_gt = None
              else:
                  train_connect_gt = data_file['train_connect2_gt'][:]
              try:
                  data_file['test_connect2_gt']
              except:
                  print("data test_connect2_gt not available")
                  test_connect_gt = None
              else:
                  test_connect_gt = data_file['test_connect2_gt'][:]
          try:
              data_file['train_gt_complet']
          except:
              print("set train_gt_complet not available (used with hand labled data)")
              train_gt_complet = None
          else:
              train_gt_complet = data_file['train_gt_complet'][:]
          try:
              data_file['test_gt_complet']
          except:
              print("set test_gt_complet not available (used with hand labled data)")
              test_gt_complet = None
          else:
              test_gt_complet = data_file['test_gt_complet'][:]
      else:
          if args.urban and args.do_use_connect_set == 2:
              train_connect_gt = np.zeros([n_train, args.edges.shape[0]])
              test_connect_gt = np.zeros([n_test, args.edges.shape[0]])
              for indice_img in range(n_train):
                  train_connect_gt[indice_img] = gene_connect_set(train_gt[indice_img].reshape(train_gt.shape[1]*train_gt.shape[2]),args.edges)
              for indice_img in range(n_test):
                  test_connect_gt[indice_img] = gene_connect_set(test_gt[indice_img].reshape(test_gt.shape[1]*test_gt.shape[2]),args.edges)
          else:
              train_connect_gt = None
              test_connect_gt = None
              train_gt_complet = None
              test_gt_complet = None

      # dégradation de l'etiquetage
      indice_degrade=np.random.choice(train_gt.size,int(train_gt.size*args.prop_degrad),replace=False)
      train_gt.reshape([train_gt.size])[indice_degrade] = 99
      if args.n_channels == 1:
          train_obs = train_obs.reshape([train_obs.shape[0],train_obs.shape[1],train_obs.shape[2],1])
          test_obs = test_obs.reshape([test_obs.shape[0],test_obs.shape[1],test_obs.shape[2],1])
      print("%d tiles for training, %d tiles for testing" % (n_train, n_test))
      print("Each tile is of size: %d x %d x %d" % (train_obs.shape[1:4]))

      print("load data ok")

      if validation : 
          if rand_choice:
              #create train and test dataset with ListDataset
              validation_List = np.random.choice(n_train,int(n_train/5), replace=False)# choisi aléatoirement 20% du set d'entrainement
              #réduction des données d'aprentissage
              List_temp = np.setdiff1d(list(range(n_train)),validation_List)# prend les 80% restant
              train_List = np.random.choice(List_temp,int((4*n_train/5)*args.prop_img_train_set), replace=False) # choisi aléatoirement une portion des 80% restant
          else:
              #create train and test dataset with ListDataset
              validation_List = np.arange(int(4*n_train/5),n_train,1)# choisi aléatoirement 20% du set d'entrainement
              #réduction des données d'aprentissage
              train_List = np.arange(0,int((4*n_train/5)*args.prop_img_train_set),1) # choisi aléatoirement une portion des 80% restant
          test_set  = tnt.dataset.ListDataset(list(range(n_test)),partial(tile_loader, set_obs = test_obs, set_gt = test_gt, set_connect = test_connect_gt, set_gt_complet = test_gt_complet, args=args))
          train_set = tnt.dataset.ListDataset(train_List,partial(tile_loader, set_obs = train_obs, set_gt = train_gt, set_connect = train_connect_gt, set_gt_complet = train_gt_complet, args=args))
          validation_set = tnt.dataset.ListDataset(validation_List,partial(tile_loader, set_obs = train_obs, set_gt = train_gt, set_connect = train_connect_gt, set_gt_complet = train_gt_complet, args=args))
      else:
          test_set  = tnt.dataset.ListDataset(list(range(n_test)),partial(tile_loader, set_obs = test_obs, set_gt = test_gt, set_connect = test_connect_gt, set_gt_complet = test_gt_complet, args=args))
          train_set = tnt.dataset.ListDataset(list(range(n_train)),partial(tile_loader, set_obs = train_obs, set_gt = train_gt, set_connect = train_connect_gt, set_gt_complet = train_gt_complet, args=args))
          validation_set = None
        # if train_connect_gt is not None:
        #   train_connect_set = tnt.dataset.ListDataset(train_List,partial(tile_loader, set_obs = train_obs, set_gt = train_connect_gt, args=args))
        #   validation_connect_set = tnt.dataset.ListDataset(validation_List,partial(tile_loader, set_obs = train_obs, set_gt = train_connect_gt, args=args))
        # else:
        #   train_connect_set = None
        #   validation_connect_set = None
        
        # if test_connect_gt is not None:
        #   test_connect_set = tnt.dataset.ListDataset(list(range(n_test)),partial(tile_loader, set_obs = test_obs, set_gt = test_connect_gt, args=args))
        # else:
        #   test_connect_set = None
      n_train = int(4*n_train/5)
      n_validation = int(n_train/5)
  print("init set ok")

  return test_set, train_set, validation_set,n_test,n_train,n_validation



