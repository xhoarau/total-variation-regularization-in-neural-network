import torch

def gauche(n,input1):
  output = torch.zeros(input1.shape,device = 'cuda')
  if n>0:
    output[:,:,:-n] = input1[:,:,n:]
  else:
    output = input1
  return output
def droite(n,input1):
  output = torch.zeros(input1.shape,device = 'cuda')
  if n>0:
    output[:,:,n:] = input1[:,:,:-n]
  else:
    output = input1
  return output

def haut(n,input1):
  output = torch.zeros(input1.shape,device = 'cuda')
  if n>0:
    output[:,:-n,:] = input1[:,n:,:]
  else:
    output = input1
  return output
def bas(n,input1):
  output = torch.zeros(input1.shape,device = 'cuda')
  if n>0:
    output[:,n:,:] = input1[:,:-n,:]
  else:
    output = input1
  return output