import numpy as np
import fiftyone as fo
import fiftyone.zoo as foz
import fiftyone.utils.labels as foul
from PIL import Image
import copy

dataset = foz.load_zoo_dataset(
    "cityscapes",
    dataset_name="cityscape_construction",
    source_dir = "/home/xhoarau/these/cityscapes/",
)

pb = fo.ProgressBar()

nb_train = 2000
nb_test = 500
nb_valid = 500
tr = 0
te = 0
v= 0
list_delete = []

for sample in pb(dataset):
     if sample.tags[0] == "train":
             if tr < nb_train:
                    tr = tr+1
             else:
                     if te < nb_test:
                             te = te+1
                             sample.tags[0] = "test"
                             sample.save()
                     else:
                             list_delete.append(sample.id)
     else:
             if sample.tags[0] == "validation":
                     if v < nb_valid:
                             v = v+1
                     else:
                             list_delete.append(sample.id)
             else:
                     list_delete.append(sample.id)


dataset.delete_samples(list_delete)

foul.objects_to_segmentations(
	dataset,
	"gt_fine",
	"segmentations",
	mask_targets={1: "person", 2: "rider",3: "car",4: "truck",5: "bus",6: "train",7: "motorcycle",8: "bicycle",9: "pole",10: "traffic sign",11: "traffic light"},
)

dataset.delete_sample_field("gt_fine")

ratio_max = 3

compt_pixel_class_train = np.zeros([12])
compt_pixel_class_test = np.zeros([12])
compt_pixel_class_valid = np.zeros([12])

pb = fo.ProgressBar()

for sample in pb(dataset):
    if sample.tags[0] == "train":
        unique,count = np.unique(sample.segmentations.mask, return_counts=True)
        if np.any(unique == 99):
            unique = unique[:-1]
            count = count[:-1]
        compt_pixel_class_train[unique] = compt_pixel_class_train[unique] + count
    else:
        if sample.tags[0] == "test":
            unique,count = np.unique(sample.segmentations.mask, return_counts=True)
            if np.any(unique == 99):
                unique = unique[:-1]
                count = count[:-1]
            compt_pixel_class_test[unique] = compt_pixel_class_test[unique] + count
        else:
            unique,count = np.unique(sample.segmentations.mask, return_counts=True)
            if np.any(unique == 99):
                unique = unique[:-1]
                count = count[:-1]
            compt_pixel_class_valid[unique] = compt_pixel_class_valid[unique] + count

print("train: ",compt_pixel_class_train)
print("test: ",compt_pixel_class_test)
print("valid: ",compt_pixel_class_valid)

ratio_classes = (compt_pixel_class_train*ratio_max>=max(compt_pixel_class_train))*(1-ratio_max*min(compt_pixel_class_train)/max(compt_pixel_class_train))+(compt_pixel_class_train*ratio_max<max(compt_pixel_class_train))*(compt_pixel_class_train-min(compt_pixel_class_train))/compt_pixel_class_train

pb = fo.ProgressBar()

for sample in pb(dataset):
    if sample.tags[0] == "train" or sample.tags[0] == "validation":
        rng = np.random.default_rng(seed = sum(ord(i) for i in sample.id))
        gt = sample.segmentations.mask 
        sample["save_mask"] = copy.deepcopy(gt)
        for i in range(len(ratio_classes)):
            indices_degrad = rng.choice(gt.size,int(gt.size*(ratio_classes[i])),replace=False)
            gt.reshape([gt.size])[indices_degrad] = (gt.reshape([gt.size])[indices_degrad]==i) * 99 + (gt.reshape([gt.size])[indices_degrad]!=i) * gt.reshape([gt.size])[indices_degrad]
        sample.segmentations.mask = gt
        sample.save()

name = "cityscape"

if name in fo.list_datasets():
    dataset_2 = fo.load_dataset(name)
    dataset_2.delete()
dataset.name = name
dataset.persistent = True
dataset.save()










