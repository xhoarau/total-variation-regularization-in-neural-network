import zipfile

chemin = "donnee_stage/not_reg/"
name = "not_reg_petit_multi_TV10_lent"
Boucle = True # si lancement en boucle utilisé
nb_boucle = 10

assert not zipfile.is_zipfile(chemin+name+".zip")
if Boucle:
  archive=zipfile.ZipFile(chemin+name+".zip", mode="w")
  archive.write(chemin+name+".txt")
  archive.write(chemin+name+"_IoU_Logit.pt")
  archive.write(chemin+name+"_IoU.pt")
  archive.write(chemin+name+"_Loss_edge.pt")
  archive.write(chemin+name+"_Loss_Logit.pt")
  archive.write(chemin+name+"_Loss.pt")
  archive.write(chemin+name+"_Loss_reg.pt")
  archive.write(chemin+name+"_n_comp.pt")
  for i in range(nb_boucle):
    archive.write(chemin+"args_" + name + "_" + str(i) + ".pt")
    archive.write(chemin+"resultat_" +name + "_" + str(i) + ".pt")

else:
  archive=zipfile.ZipFile(chemin+name+".zip", mode="w")
  archive.write(chemin+name+".txt")
  archive.write(chemin+"args_"+name+".pt")
  archive.write(chemin+"resultat_"+name+".pt")

