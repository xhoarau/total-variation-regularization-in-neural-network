import numpy as np
import torch
import time
import copy
import sys,os

import modules.insertion_donne as insert
import modules.save_result as sr
import modules.train_fonctions as tf
import modules.storage_class as sc

# définition des paramètre pour le réseau
name = "reseau_lambda_multi_s1e7"

path = "Models/"
if os.path.exists(path):
  filename = path + "resultat_" + name + ".pt"
  assert not os.path.isfile(filename)
else:
  path = os.path.join(os.getcwd(),os.pardir,"Models/")
  if os.path.exists(path):
    filename = path + "resultat_" + name + ".pt"
    assert not os.path.isfile(filename)
  else:
    path = os.getcwd()
    filename = path + "resultat_" + name + ".pt"
    assert not os.path.isfile(filename)

boucle = 10

# caractéristique réseau
n_epoch = 100 # nombre maxixmum d'epoch pour l'apprentissage
batch_size = 16
n_class = 2
class_names = ["Urban", "not Urban"]
n_channels = 4
conv_width = [32,32,16]
cuda = 1
lr = 5e-3
coef_drop = 0.5
n_epoch_validation = 10

# caractéristique regularisation
do_reg = True
taille_graph = [256,256] #taille spacial des données
blocage_epoch = 5 # nombre d'époch sans régularisation
blocage_reg = 0 # nombre d'epoch sans contrôle des lambdas (aprés blocage epoch)
coef_reg = 0 # poid du controle des lambda dans la loss
nb_feature = 1/2 # proportion de feature donnée au lambda ([0,1])
reg_sousgrad = False #choix controle Ridge ou Subgrad (si coef_reg = 0 mettre False pour economie de memoire)
red_grad_lambda = 10 #nombre d'epoch de reduction des gradiants de lambda (aprés blocage epoch)
logit = True

# caractéristique données
prop_degrad = 0 # proportion de dégradation de l'etiquetage ([0,1])
prop_img_train_set = 1 # nombre d'imga dans l'ensemble d'entrainement (160 * [0,1])
urban = True # choix entre des donnée urban/not urban or road/not road

args = sc.argument(n_epoch,batch_size,n_class,class_names,n_channels,conv_width,cuda,lr,coef_drop,n_epoch_validation,taille_graph, \
                  do_reg,blocage_epoch,blocage_reg,coef_reg,nb_feature,reg_sousgrad,red_grad_lambda,logit,prop_degrad,prop_img_train_set,urban)

mes_temps_entrainement = []
List_temps_entrainement = []
List_trained_model = []
List_evol_train_TV = []
List_evol_test_TV = []
List_evol_valid_TV = []
List_evol_composante = []
List_epoch_valid = []
List_weight_class = []

for index_tirage in range(boucle):

  test_set, train_set, validation_set = insert.creation_set(args)

  loader = torch.utils.data.DataLoader(train_set, batch_size=1, shuffle=False, drop_last=False)
  compt_pixel_class = torch.zeros([n_class])
  for index, (tiles, gt) in enumerate(loader):
    unique,count = torch.unique(gt, sorted=True, return_counts=True)
    if torch.any(unique == 99):
      unique = unique[:-1]
    compt_pixel_class = compt_pixel_class + count[unique]
  total_px = torch.sum(compt_pixel_class)
  weight_class = 1/n_class + ((total_px - compt_pixel_class)/(total_px*(n_class-1))-1/n_class)*coef_weight
  if cuda:
    args.weight_class = weight_class.cuda()
  else:
    args.weight_class = weight_class

  List_weight_class.append(args.weight_class)

  print("depart apprentissage : ",index_tirage)
  mes_temps_entrainement.append(time.time())
  [trained_model,evol_train_TV,evol_test_TV,evol_valid_TV,evol_composante,epoch_valid] = tf.train_full_TV(args, train_set, validation_set, test_set)
  mes_temps_entrainement.append(time.time())
  List_temps_entrainement.append(mes_temps_entrainement[1]-mes_temps_entrainement[0])
  mes_temps_entrainement = []
  List_trained_model.append(trained_model)
  List_evol_train_TV.append(evol_train_TV)
  List_evol_test_TV.append(evol_test_TV)
  List_evol_valid_TV.append(evol_valid_TV)
  List_evol_composante.append(evol_composante)
  List_epoch_valid.append(epoch_valid)

resultat = sc.result_multi(List_trained_model,List_evol_train_TV,List_evol_valid_TV,List_evol_test_TV,List_evol_composante,List_temps_entrainement,List_epoch_valid,List_weight_class)

path = "Models/"
if os.path.exists(path):
  sr.save(resultat, path + "resultat_" + name + ".pt")
  sr.save(args, path + "args_" + name + ".pt")
else:
  path = os.path.join(os.getcwd(),os.pardir,"Models/")
  if os.path.exists(path):
    sr.save(resultat, path + "resultat_" + name + ".pt")
    sr.save(args, path + "args_" + name + ".pt")
  else:
    path = os.getcwd()
    sr.save(resultat, path + "resultat_" + name + ".pt")
    sr.save(args, path + "args_" + name + ".pt")

print("Fin")