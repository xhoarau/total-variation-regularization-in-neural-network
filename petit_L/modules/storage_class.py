import sys,os
import numpy as np
import torch

sys.path.append(os.path.join(os.getcwd(),"grid-graph/python/bin"))
sys.path.append(os.path.join(os.getcwd(),os.pardir,"grid-graph/python/bin"))

from grid_graph import grid_to_graph
from grid_graph import edge_list_to_forward_star

class argument: #contient tout les paramètre et argument d'un réseau
  def __init__(self,n_epoch,batch_size,n_class,class_names,n_channels,conv_width,conv_width_Lambda,cuda,lr,coef_drop,n_epoch_validation,set_name,dilation,kernel_size,taille_graph,connectivity,do_reg,blocage_epoch,blocage_reg,coef_reg,nb_feature,reg_sousgrad,red_grad_lambda,logit,couche_sup_lambda,Lambda_separe,prop_degrad,prop_img_train_set,urban,square):
    
    edges, connectivities = grid_to_graph(np.array(taille_graph,dtype='int32'), connectivity = connectivity, compute_connectivities = True, graph_as_forward_star = False, row_major_index = True)# calcul du graph des adjacance
    first_edges,adj_vertices, reindex = edge_list_to_forward_star(np.prod(taille_graph),edges)# changement de representation
    first_edges = first_edges.astype('uint32')
    adj_vertices = adj_vertices.astype('uint32')
    edges[reindex,:] = np.copy(edges)
    edges = torch.from_numpy(edges.astype("int64")).cuda()
    connectivities[reindex] = np.copy(connectivities)
    connectivities = torch.sqrt(torch.from_numpy(connectivities).cuda())
    
    # caractéristique réseau
    self.n_epoch = n_epoch
    self.batch_size = batch_size
    self.n_class = n_class
    self.class_names = class_names
    self.n_channels = n_channels
    self.conv_width = conv_width
    self.conv_width_Lambda = conv_width_Lambda
    self.weight_class = [1/n_class]*n_class
    self.cuda = cuda 
    self.lr = lr
    self.coef_drop = coef_drop
    self.n_epoch_validation = n_epoch_validation
    self.set_name = set_name
    self.dilation = dilation
    self.kernel_size = kernel_size
    
    # caractéristique regularisation
    self.edges = edges
    self.adj_vertices = adj_vertices
    self.first_edges = first_edges
    self.connectivities = connectivities
    self.do_reg = do_reg
    self.blocage_epoch = blocage_epoch
    self.blocage_reg = blocage_reg
    self.coef_reg = coef_reg
    self.nb_feature = nb_feature
    self.reg_sousgrad = reg_sousgrad
    self.red_grad_lambda = red_grad_lambda
    self.logit = logit
    self.couche_sup_lambda = couche_sup_lambda
    self.Lambda_separe = Lambda_separe
    self.step_1_end = (blocage_epoch != -1) # apprentissage sans régularisation

    # caractéristique données
    self.prop_degrad = prop_degrad
    self.prop_img_train_set = prop_img_train_set
    self.urban = urban
    self.square = square

    self.compte_composante = []

class result: #contien les resultats de l'apprentissage
  def __init__(self,trained_model,evol_train_TV,evol_valid_TV,evol_test_TV,evol_composante,duree_entrainement,epoch_save,epoch_regularisation):
    self.trained_model = trained_model
    self.evol_train_TV = evol_train_TV
    self.evol_valid_TV = evol_valid_TV
    self.evol_test_TV = evol_test_TV
    self.evol_composante = evol_composante
    self.duree_entrainement = duree_entrainement
    self.epoch_save = epoch_save
    self.epoch_regularisation = epoch_regularisation

class result_multi: #contien les resultats de l'apprentissage
  def __init__(self,List_trained_model,List_evol_train_TV,List_evol_valid_TV,List_evol_test_TV,List_evol_composante,List_temps_entrainement,List_epoch_valid,List_weight_class):
    self.List_trained_model = List_trained_model
    self.List_evol_train_TV = List_evol_train_TV
    self.List_evol_valid_TV = List_evol_valid_TV
    self.List_evol_test_TV = List_evol_test_TV
    self.List_evol_composante = List_evol_composante
    self.List_temps_entrainement = List_temps_entrainement
    self.List_epoch_valid = List_epoch_valid
    self.List_weight_class = List_weight_class