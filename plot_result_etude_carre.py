import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stat
import modules.save_result as sr
import torch

def plot_hist(x,b,c,ax,save):
  plt.hist(x,bins=b,facecolor=c)
  if save:
    plt.savefig("test.png")

fig = plt.figure(figsize = (5,5),dpi = 1000)
ax = fig.add_subplot(1,1,1)

def load_donne(name):
  print(name)
  IoU = sr.load("Models/" + name + "_IoU.pt")
  Loss = sr.load("Models/" + name + "_Loss.pt")
  Loss_reg = sr.load("Models/" + name + "_Loss_reg.pt")
  IoU_Logit = sr.load("Models/" + name + "_IoU_Logit.pt")
  Loss_Logit = sr.load("Models/" + name + "_Loss_Logit.pt")
  Loss_edge = sr.load("Models/" + name + "_Loss_edge.pt")
  n_comp = sr.load("Models/" + name + "_n_comp.pt")

  return IoU, Loss, Loss_reg, IoU_Logit, Loss_Logit, Loss_edge, n_comp

def resultat_complet(name):
  IoU, Loss, Loss_reg, IoU_Logit, Loss_Logit, Loss_edge, n_comp = load_donne(name)

  print("mean IoU Train : ", torch.mean(IoU[:,0]).item()*100, "%")
  print("mean IoU Valid : ", torch.mean(IoU[:,1]).item()*100, "%")
  print("mean IoU Test : ", torch.mean(IoU[:,2]).item()*100, "%")

  print("maximum IoU Test : ", torch.max(IoU[:,2]).item()*100, "%")
  print("minimum IoU Test : ", torch.min(IoU[:,2]).item()*100, "%")

  print("std IoU Train : ", torch.std(IoU[:,0]).item()*100)
  print("std IoU Valid : ", torch.std(IoU[:,1]).item()*100)
  print("std IoU Test : ", torch.std(IoU[:,2]).item()*100)

  print("mean Loss Train : ", torch.mean(Loss[:,0]).item())
  print("mean Loss Valid : ", torch.mean(Loss[:,1]).item())
  print("mean Loss Test : ", torch.mean(Loss[:,2]).item())

  print("std Loss Train : ", torch.std(Loss[:,0]).item())
  print("std Loss Valid : ", torch.std(Loss[:,1]).item())
  print("std Loss Test : ", torch.std(Loss[:,2]).item())

  print("mean Loss reg Train : ", torch.mean(Loss_reg[:,0]).item())
  print("mean Loss reg Valid : ", torch.mean(Loss_reg[:,1]).item())
  print("mean Loss Test : ", torch.mean(Loss_reg[:,2]).item())

  print("std Loss reg Train : ", torch.std(Loss_reg[:,0]).item())
  print("std Loss reg Valid : ", torch.std(Loss_reg[:,1]).item())
  print("std Loss reg Test : ", torch.std(Loss_reg[:,2]).item())

  print("mean IoU not_reg Train : ", torch.mean(IoU_Logit[:,0]).item()*100, "%")
  print("mean IoU not_reg Valid : ", torch.mean(IoU_Logit[:,1]).item()*100, "%")
  print("mean IoU not_reg Test : ", torch.mean(IoU_Logit[:,2]).item()*100, "%")

  print("std IoU not_reg Train : ", torch.std(IoU_Logit[:,0]).item()*100)
  print("std IoU not_reg Valid : ", torch.std(IoU_Logit[:,1]).item()*100)
  print("std IoU not_reg Test : ", torch.std(IoU_Logit[:,2]).item()*100)

  print("mean Loss not_reg Train : ", torch.mean(Loss_Logit[:,0]).item())
  print("mean Loss not_reg Valid : ", torch.mean(Loss_Logit[:,1]).item())
  print("mean Loss not_reg Test : ", torch.mean(Loss_Logit[:,2]).item())

  print("std Loss not_reg Train : ", torch.std(Loss_Logit[:,0]).item())
  print("std Loss not_reg Valid : ", torch.std(Loss_Logit[:,1]).item())
  print("std Loss not_reg Test : ", torch.std(Loss_Logit[:,2]).item())

  print("mean Loss_edge Train : ", torch.mean(Loss_edge[:,0]).item())
  print("mean Loss_edge Valid : ", torch.mean(Loss_edge[:,1]).item())
  print("mean Loss_edge Test : ", torch.mean(Loss_edge[:,2]).item())

  print("std Loss_edge Train : ", torch.std(Loss_edge[:,0]).item())
  print("std Loss_edge Valid : ", torch.std(Loss_edge[:,1]).item())
  print("std Loss_edge Test : ", torch.std(Loss_edge[:,2]).item())

  print("mean n_comp Train : ", torch.mean(n_comp[:,0]).item())
  print("mean n_comp Valid : ", torch.mean(n_comp[:,1]).item())
  print("mean n_comp Test : ", torch.mean(n_comp[:,2]).item())

  print("std n_comp Train : ", torch.std(n_comp[:,0]).item())
  print("std n_comp Valid : ", torch.std(n_comp[:,1]).item())
  print("std n_comp Test : ", torch.std(n_comp[:,2]).item())

  return IoU, Loss, Loss_reg, IoU_Logit, Loss_Logit, Loss_edge, n_comp

def compare_resultat(name, name_2):
  IoU, Loss, Loss_reg, IoU_Logit, Loss_Logit, Loss_edge, n_comp = load_donne(name)
  IoU_2, Loss_2, Loss_reg_2, IoU_Logit_2, Loss_Logit_2, Loss_edge_2, n_comp_2 = load_donne(name_2)

  fig = plt.figure(figsize = (5,5),dpi = 500)
  
  ax = fig.add_subplot(1,1,1)
  # bins = np.linspace(min(IoU[:,2]*100),max(IoU[:,2]*100),10)
  ax.hist(np.array(IoU[:,2]*100), bins = 500, color = 'b')
  # bins = np.linspace(min(IoU_2[:,2]*100),max(IoU_2[:,2]*100),10)
  ax.hist(np.array(IoU_2[:,2]*100), bins = 500, color = 'r')

  plt.savefig("test.png")

  print(name, "  /  ", name_2)
  print("mean IoU Train : ", torch.mean(IoU[:,0]).item()*100, "%  /  ",torch.mean(IoU_2[:,0]).item()*100, "%")
  print("mean IoU Valid : ", torch.mean(IoU[:,1]).item()*100, "%  /  ",torch.mean(IoU_2[:,1]).item()*100, "%")
  print("mean IoU Test : ", torch.mean(IoU[:,2]).item()*100, "%  /  ",torch.mean(IoU_2[:,2]).item()*100, "%")

  print("maximum IoU Test : ", torch.max(IoU[:,2]).item()*100, "%  /  ",torch.max(IoU_2[:,2]).item()*100, "%")
  print("minimum IoU Test : ", torch.min(IoU[:,2]).item()*100, "%  /  ",torch.min(IoU_2[:,2]).item()*100, "%")

  print("std IoU Train : ", torch.std(IoU[:,0]).item()*100,"  /  ", torch.std(IoU_2[:,0]).item()*100)
  print("std IoU Valid : ", torch.std(IoU[:,1]).item()*100,"  /  ", torch.std(IoU_2[:,1]).item()*100)
  print("std IoU Test : ", torch.std(IoU[:,2]).item()*100,"  /  ", torch.std(IoU_2[:,2]).item()*100)

  print("mean Loss Train : ", torch.mean(Loss[:,0]).item(), "  /  ", torch.mean(Loss_2[:,0]).item())
  print("mean Loss Valid : ", torch.mean(Loss[:,1]).item(), "  /  ", torch.mean(Loss_2[:,1]).item())
  print("mean Loss Test : ", torch.mean(Loss[:,2]).item(), "  /  ", torch.mean(Loss_2[:,2]).item())

  print("std Loss Train : ", torch.std(Loss[:,0]).item(), "  /  ", torch.std(Loss_2[:,0]).item())
  print("std Loss Valid : ", torch.std(Loss[:,1]).item(), "  /  ", torch.std(Loss_2[:,1]).item())
  print("std Loss Test : ", torch.std(Loss[:,2]).item(), "  /  ", torch.std(Loss_2[:,2]).item())

  print("mean Loss reg Train : ", torch.mean(Loss_reg[:,0]).item(), "  /  ", torch.mean(Loss_reg_2[:,0]).item())
  print("mean Loss reg Valid : ", torch.mean(Loss_reg[:,1]).item(), "  /  ", torch.mean(Loss_reg_2[:,1]).item())
  print("mean Loss Test : ", torch.mean(Loss_reg[:,2]).item(), "  /  ", torch.mean(Loss_reg_2[:,2]).item())

  print("std Loss reg Train : ", torch.std(Loss_reg[:,0]).item(), "  /  ", torch.std(Loss_reg_2[:,0]).item())
  print("std Loss reg Valid : ", torch.std(Loss_reg[:,1]).item(), "  /  ", torch.std(Loss_reg_2[:,1]).item())
  print("std Loss reg Test : ", torch.std(Loss_reg[:,2]).item(), "  /  ", torch.std(Loss_reg_2[:,2]).item())

  print("mean IoU not_reg Train : ", torch.mean(IoU_Logit[:,0]).item()*100, "%  /  ", torch.mean(IoU_Logit_2[:,0]).item()*100, "%")
  print("mean IoU not_reg Valid : ", torch.mean(IoU_Logit[:,1]).item()*100, "%  /  ", torch.mean(IoU_Logit_2[:,1]).item()*100, "%")
  print("mean IoU not_reg Test : ", torch.mean(IoU_Logit[:,2]).item()*100, "%  /  ", torch.mean(IoU_Logit_2[:,2]).item()*100, "%")

  print("std IoU not_reg Train : ", torch.std(IoU_Logit[:,0]).item()*100,"  /  ", torch.std(IoU_Logit_2[:,0]).item()*100)
  print("std IoU not_reg Valid : ", torch.std(IoU_Logit[:,1]).item()*100,"  /  ", torch.std(IoU_Logit_2[:,1]).item()*100)
  print("std IoU not_reg Test : ", torch.std(IoU_Logit[:,2]).item()*100,"  /  ", torch.std(IoU_Logit_2[:,2]).item()*100)

  print("mean Loss not_reg Train : ", torch.mean(Loss_Logit[:,0]).item(), "  /  ", torch.mean(Loss_Logit_2[:,0]).item())
  print("mean Loss not_reg Valid : ", torch.mean(Loss_Logit[:,1]).item(), "  /  ", torch.mean(Loss_Logit_2[:,1]).item())
  print("mean Loss not_reg Test : ", torch.mean(Loss_Logit[:,2]).item(), "  /  ", torch.mean(Loss_Logit_2[:,2]).item())

  print("std Loss not_reg Train : ", torch.std(Loss_Logit[:,0]).item(),"  /  ", torch.std(Loss_Logit_2[:,0]).item())
  print("std Loss not_reg Valid : ", torch.std(Loss_Logit[:,1]).item(),"  /  ", torch.std(Loss_Logit_2[:,1]).item())
  print("std Loss not_reg Test : ", torch.std(Loss_Logit[:,2]).item(),"  /  ", torch.std(Loss_Logit_2[:,2]).item())

  print("mean Loss_edge Train : ", torch.mean(Loss_edge[:,0]).item(), "  /  ", torch.mean(Loss_edge_2[:,0]).item())
  print("mean Loss_edge Valid : ", torch.mean(Loss_edge[:,1]).item(), "  /  ", torch.mean(Loss_edge_2[:,1]).item())
  print("mean Loss_edge Test : ", torch.mean(Loss_edge[:,2]).item(), "  /  ", torch.mean(Loss_edge_2[:,2]).item())

  print("std Loss_edge Train : ", torch.std(Loss_edge[:,0]).item(), "  /  ", torch.std(Loss_edge_2[:,0]).item())
  print("std Loss_edge Valid : ", torch.std(Loss_edge[:,1]).item(), "  /  ", torch.std(Loss_edge_2[:,1]).item())
  print("std Loss_edge Test : ", torch.std(Loss_edge[:,2]).item(), "  /  ", torch.std(Loss_edge_2[:,2]).item())

  print("mean n_comp Train : ", torch.mean(n_comp[:,0]).item(), "  /  ", torch.mean(n_comp_2[:,0]).item())
  print("mean n_comp Valid : ", torch.mean(n_comp[:,1]).item(), "  /  ", torch.mean(n_comp_2[:,1]).item())
  print("mean n_comp Test : ", torch.mean(n_comp[:,2]).item(), "  /  ", torch.mean(n_comp_2[:,2]).item())

  print("std n_comp Train : ", torch.std(n_comp[:,0]).item(), "  /  ", torch.std(n_comp_2[:,0]).item())
  print("std n_comp Valid : ", torch.std(n_comp[:,1]).item(), "  /  ", torch.std(n_comp_2[:,1]).item())
  print("std n_comp Test : ", torch.std(n_comp[:,2]).item(), "  /  ", torch.std(n_comp_2[:,2]).item())

  return [IoU, Loss, Loss_reg, IoU_Logit, Loss_Logit, Loss_edge, n_comp],[IoU_2, Loss_2, Loss_reg_2, IoU_Logit_2, Loss_Logit_2, Loss_edge_2, n_comp_2]

def etude(names):
  r_IoU = torch.zeros([3,2,len(names)])
  r_Loss = torch.zeros([3,2,len(names)])
  r_Loss_reg = torch.zeros([3,2,len(names)])
  r_IoU_Logit = torch.zeros([3,2,len(names)])
  r_Loss_Logit = torch.zeros([3,2,len(names)])
  r_Loss_edge = torch.zeros([3,2,len(names)])
  r_n_comp = torch.zeros([3,2,len(names)])
  name = ""
  
  for indice_model in range(len(names)):
    IoU, Loss, Loss_reg, IoU_Logit, Loss_Logit, Loss_edge, n_comp = load_donne(names[indice_model])
    r_IoU[:,0,indice_model] = torch.mean(IoU,dim = 0)*100
    r_IoU[:,1,indice_model] = torch.std(IoU,dim = 0)*100
    r_Loss[:,0,indice_model] = torch.mean(Loss,dim = 0)
    r_Loss[:,1,indice_model] = torch.std(Loss,dim = 0)
    r_Loss_reg[:,0,indice_model] = torch.mean(Loss_reg,dim = 0)
    r_Loss_reg[:,1,indice_model] = torch.std(Loss_reg,dim = 0)
    r_IoU_Logit[:,0,indice_model] = torch.mean(IoU_Logit,dim = 0)*100
    r_IoU_Logit[:,1,indice_model] = torch.std(IoU_Logit,dim = 0)*100
    r_Loss_Logit[:,0,indice_model] = torch.mean(Loss_Logit,dim = 0)
    r_Loss_Logit[:,1,indice_model] = torch.std(Loss_Logit,dim = 0)
    r_Loss_edge[:,0,indice_model] = torch.mean(Loss_edge,dim = 0)
    r_Loss_edge[:,1,indice_model] = torch.std(Loss_edge,dim = 0)
    r_n_comp[:,0,indice_model] = torch.mean(n_comp,dim = 0)
    r_n_comp[:,1,indice_model] = torch.std(n_comp,dim = 0)
    name = name + names[indice_model] + " / "
    print("IoU\n",IoU)
    print("IoU_Logit\n",IoU_Logit)
    print("n_comp\n\n",n_comp)
  
  print("mean\nstd\n et \ntrain\nvalidation\ntest\n")
  print(name)
  print("IoU\n",r_IoU)
  print("Loss\n",r_Loss)
  print("Loss_reg\n",r_Loss_reg)
  print("IoU_Logit\n",r_IoU_Logit)
  print("Loss_Logit\n",r_Loss_Logit)
  print("Loss_edge\n",r_Loss_edge)
  print("n_comp\n\n",r_n_comp)

  

  
