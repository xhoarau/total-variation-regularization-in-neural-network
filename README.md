#When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
#Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Total Variation Regularization in Neural Network

Be sure to install properly those other git repository before any run:
    https://github.com/raymondyeh07/tv_layers_for_cv/
    https://gitlab.com/1a7r0ch3/parallel-cut-pursuit
    https://gitlab.com/1a7r0ch3/grid-graph

for all expriments that doesnt involve tv_layer_for_cv you will need the environement in "requirement.txt" (some version or package may not be absolutly necessary, a clean up will be donne later)
To use the tv layer change to the "requiremements_tvl.txt" (some version or package may not be absolutly necessary, a clean up will be donne later)

Finally to test the multiclasses network create a clone of https://github.com/meetps/pytorch-semseg, and then change the files by the ones in "save_changes_meetps" folder, be carefull not to erase the other files there are juste not changed.


