Votre travail de recherche est-il effectué pour tout ou partie dans un établissement autre qu'un établissement public d'enseignement supérieur et/ou de recherche ?
- Non

Quel est le temps de présence dans votre unité de recherche de rattachement ? - 80%

Merci de préciser comment vous organisez et répartissez vos temps de présence : 
	- Je me déplace régulièrement à Blois afin de donner des cours et de travailler avec mes encadrants.

Calendrier du projet de recherche :
Préciser les échéances prévisionnelles des étapes principales du projet doctoral jusqu’à la soutenance

    Durée prévue (3 ans à temps complet, entre 3 et 6 ans à temps partiel)
    Calendrier des séjours dans les deux pays si cotutelle internationale (à reporter dans le champ "Organisation de la cotutelle" de l'onglet "Cotutelle" de votre profil)
    Répartition du temps entre laboratoire académique et centre de recherche non académique (cas Cifre ou thèse en partenariat avec entreprise)
    Etapes et résultats du projet dans le cas d’un contrat de recherche partenariale.

	- Il est prévu que la thèse dur trois ans, aucun partenariat ni cotutelle ne sont prévus dans le contrat.

Modalités d'encadrement, de suivi de la formation et d'avancement des recherches de la thèse :
Préciser :

    les modalités décidées par l’Ecole doctorale pour le comité individuel de formation
    les prérequis spécifiques pour la soutenance (publications, heures ou crédits doctoraux …) ou renvoyer à un règlement intérieur ED

	- Se référer à la charte du Doctorat CDCVL.

Conditions matérielles de réalisation du projet de recherche, le cas échéant, les conditions de sécurité spécifiques :
Préciser :

    Moyens et méthodes disponibles dans l’unité de recherche pour mener à bien le projet
    Préciser si des conditions spécifiques de sécurité sont requises pour ce projet doctoral, en plus de celles évoquées dans le règlement intérieur de l'unité de recherche

	- Les membres de l'équipe du service informatique nous mettent à disposition des ordinateurs (et les services d'installation), ainsi que les moyens de connexion au serveur de calcul nécessaire.

	- Il n'y a pas de mesure particulière de sécurité puisqu'il n'y a pas d’accès physique au matériel mis à part notre ordinateur et que les accès en ligne sont contrôlés.

Modalités d'intégration dans l'unité ou l'équipe de recherche :
Indiquer les méthodes d'intégration de l'unité de recherche, telles que des animations scientifiques ou d'intégration (offertes ou obligatoires), les éventuelles responsabilités collectives que le doctorant devra assumer au sein du laboratoire.
Un calendrier prévsionel du projet de recherche peut être précisé. 

	- J'ai eu l’occasion de présenter mes travaux lors des JIRC où une partie de l'équipe était présente. J'ai aussi pu aller à la journée labo où il y a eu des présentations, un repas et une activité rassemblant tout le laboratoire. J'aurais aussi l'occasion de participer à des séminaires d'équipe pour discuter de mes travaux et discuter des travaux réalisés par l'équipe.
	
Parcours prévisionnel individuel de formation :
A compléter : Liste des formations envisagées en lien avec votre projet professionnel : formations transversales, scientifiques et techniques...
Le collège doctoral regroupant les différentes écoles doctorales propose un ensemble de formations scientifiques disciplinaires, pluridisciplinaires et transversales telles que celles préparant à l’insertion professionnelle et aux métiers de l’enseignement. Elles sont présentées en début d’année universitaire et se déroulent en général au second semestre (https://collegedoctoral-cvl.fr).
D’autres formations plus spécifiques peuvent être suivies à l’extérieur et validées par l’école doctorale. 

	- Avec l'école doctorale, je participe à deux modules de formation sur l'enseignement, et je souhaite m'inscrire l'an prochain à une formation en langue, en particulier sur la rédaction en anglais.

	- Je travaille aussi avec mes encadrants sur la rédaction en langue au travers de résumé d'article important dans notre état de l'art.

Objectifs de valorisation des travaux de recherche de la thèse : diffusion, publication et confidentialité, droit à la propriété intellectuelle selon le champ du programme de doctorat.
Préciser les objectifs de valorisation : diffusion, communications, publication et confidentialité, brevets (avec si possible des objectifs chiffrés), droit à la propriété intellectuelle selon le champ du programme de doctorat. 
	
	- Il est prévu de présenter à différentes conférences ainsi que de rédiger des papiers sur les résultats. Il y aura aussi des opportunités de diffusion au cours de séminaire interne au laboratoire.

DEVELOPPEMENT DE COMPETENCES ET PERSPECTIVES PROFESSIONNELLES
Indiquer :

    les compétences disciplinaires, thématiques et transverses, les compétences transférables qui pourront être acquises ou ont été acquises au cours du doctorat et qui pourront être valorisées lors de l'insertion professionnelle ou de la poursuite de carrière
    les perspectives d'insertion professionnelle ou de poursuite de carrière au projet 
	
	- J'ai l'opportunité de donner des cours ainsi que de développer mes compétences en communication et en langue. Je développe aussi mes connaissances sur différents langages de programmation et en algorithmie. Finalement, je travaille aussi sur de nombreux sujets liés à la science (esprit critique, nouvelle technologie ...) sur mon temps libre en étudiant des contenus de vulgarisation. 
	
	
OUVERTURE INTERNATIONALE
Préciser les éléments déjà réalisés ou prévus (selon l'avancement du projet doctoral) qui apporteront une ouverture internationale, telle qu'une mobilité internationale envisagée pendant la thèse, en précisant l'objet :
terrain d'étude à l'étranger, utilisation d'une plateforme expérimentale, séjour dans une unité de recherche pour acquérir une compétence particulière utile au projet, conférences et colloques internationaux. 
	
	- J'ai pu participer à deux conférences internationales : "CurveAndSurfaces2022" a Arcachon et "ICCV2023" a Paris.
	
	
	
