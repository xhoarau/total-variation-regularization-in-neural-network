Recall the complete reference of the article

Capital letters to your name

1. Objectives

The way you state the objective gives the feeling that the authors had no other motivation than just for the sake of it. Why did they think that “incorporating a optimization problem into a deep neural network” would be a good idea?

2. First Question

It is weird to structure a document with such section titles. Call rather the sections after their content! 

"minimisation": 'minimization' (scientific English is mostly American English)

"The minimization": in an introduction it can be acceptable to use such a vague term. But beware that just minimizing the TV is meaningless (0 is always a solution); what better name should be used for the approaches based on optimization problems involving, among other terms, the TV?

"The minimization ... have" : 'has', singular

"interesting bias" : what do you mean?

"The TV" : you must define abbreviation before using them. Usually, the first occurrence is put in full letters, and the abbreviation is put within parenthesis right after it, and can be used in the rest of the document

"The TV ... cannot be achieved" : one does not "achieve" the TV. Reformulate.
By the way, in what large class of operators belongs the convolution? So what would be a good mathematical keyword to describe the total variation regularization, suggesting both richness and computing challenge?

"for a 1D case": dimension of what?

What is x?

Why do you express the sum of absolute differences as ║D_n x║_1? What is this n? And by the way, in the left hand term n is a free variable, whereas in the left hand term it is bound.

Why do you change notations X, Y and x, y? What is Y_row(m), Y_col(n)? What are M and N, what are the ranges of m and n?

"by put λ = 0": 'putting'

3. Second Question

again, weird title

3.1 The Solver

"of the problem defined before": which one is it? Equation numbering serves precisely this purpose.

Even though the article does not delve into details on what is the dual, you should add a reference and state for yourself why (4) is the dual of the problem and where does it come from.

H_S is the Hessian of what? What are the active variables?
Also, be sure to know what is the Armijo rule, a "dual vairable update direction", why (5) solves a quadratic approximation, and how to derive H from (5).

"it is possible with the Cholesky method to solve", this sentence is hard to parse, used commas or reformulate. Without delving into details, be sure to understand why H is tridiagonal, and have an idea of what is a Cholesky method.

voir H et sa definition

3.2 The integration in a deep-net

"They had to" : who are they?

"the gradient of this operation": which operation? So far it has not been written explicitly

"They have computed the Jacobian": how?

"positive semi-positive": which one is it?

"they can also apply a Cholesky method": to what?

"For the 2D case, their implementation is based on Proximal Dyksra": implementation of what? Which problem is it solving "for each row and each column alternatively"? How does this help computing the gradient?

4. Experiment and Conclusion

Should expand this section : improvement between what and what, to what cost (if there is a cost at all), over which data sets, computation time of what (it seems you are mixing everything here)

I do not understand the "personnal note".


ajouter les problemes de math dans des annexe


