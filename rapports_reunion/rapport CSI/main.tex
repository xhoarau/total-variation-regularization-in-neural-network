\documentclass{article}
\usepackage{graphicx} % Required for inserting images
\usepackage{biblatex}
\addbibresource{tex.bib}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{adjustbox}
\usepackage{array}
\usepackage{tikz}

\usetikzlibrary{shapes}

\def\nbR{\ensuremath{\mathrm{I\! R}}}
\DeclareMathOperator{\argmin}{arg\,min}
\DeclareMathOperator{\softmax}{soft\,max}
\DeclareMathOperator{\TV}{TV}
\DeclareMathOperator{\TVloss}{TV\,loss}
\DeclareMathOperator{\loss}{loss}

\newcommand{\ju}[1]{\textcolor{red}{\bf Julien: #1}}

\title{Results of first year : Graph-based spatial regularization within deep learning architectures}
\author{Xavier Hoarau}
\date{May 2023}

\begin{document}

\maketitle

\section{Introduction}
    Regularization is a common way to improve deep learning architectures and has two main effects : stabilizing the learning process and adding prior-knowledge. The spatial regularization is more of the second kind. The objective is to give information about the expected spatial structure of the result. There are multiple ways to make spatial regularization. In this study we have focused on the minimization of the Total Variation (defined in section \ref{DEF_TV}). It is used in many situations, as denoising or image reconstruction. But most of the time, it is used as a post-processing, in other words outside of the learning phase, and it is also often approximate to reduce needed computing power. \newline
    The principal objective of our work is to study the influence of the integration of a module performing a spatial regularization into a deep neural network. The main algorithm used for the study is Cut-Pursuit (description in section \ref{DEF_CP}) from \cite{LandrieuObozinski17} and \cite{RaguetLandrieu18}. It minimizes exactly the TV on any graph, and in several situations it is faster than other solutions.
    At the time of writing this report, we completed the integration of the algorithm inside a network architecture, we were able to learn some of the parameters of the minimization, and we studied very simple cases. The ongoing work deals with more complex situations and architectures.
    
\section{Definitions } \label{DEF}
\subsection{Deep Learning}
    There are multiple ways to define deep learning, depending on the point of view.
    From the data point of view : Deep Learning is about finding a good data representation with respect to an objective thanks to a composition of transformations. From a method point of view : “Deep Learning is building a system by assembling parameterized modules into a computation graph, and train it to perform a task by optimizing the parameters using a gradient-based method.” (From Yann Lecun)
    And from a general point of view : "Deep learning is a class of machine learning algorithms that uses multiple layers to progressively extract higher-level features from the raw input. For example, in image processing, lower layers may identify edges, while higher layers may identify the concepts relevant to a human such as digits or letters or faces." \cite{wikideeplearning}, complete definition in \cite{SIG-039}.

    A deep learning architecture is often an artificial neural network. An artificial neuron is defined as follows:
    \begin{equation}
        y = f(\boldsymbol{w}\cdot\boldsymbol{x} + b)
    \end{equation}
    With $\boldsymbol{x}$ and $y$ the input (vector) and output of the neuron respectively. The weight vector $\boldsymbol{w}$ and bias $b$ are the parameters to be learnt. Finally, $f$ is the activation function, a non-linear application, for example the sigmoid or ReLU ($\max(x,0)$).
    By organizing the neurons into layers, it is possible to construct a complex architecture. 
    Lastly, to train a network, measuring the quality of the prediction is necessary. It is the role of the loss function. In image processing, it can be inversely proportional to the number of well-labeled pixels. This function is crucial in the learning phase, it is in the objective of minimizing it that the parameters ($\boldsymbol{w}$ and $b$) will be updated (the learning phase can be seen as the resolution of an optimization problem).

\subsection{Semantic Segmentation }
    We chose this task because it is well-known. Many classical architectures target this task, and it has some interesting properties for our regularization method.
    The objective is to assign a class to each pixel according to which part of the image it is in. An example is shown in figure \ref{fig:sem_seg}.\newline
    \begin{figure}[t]
        \centering
        \includegraphics[scale=0.3]{Images/Segmentation_semantic.png}
        \caption{Example of semantic segmentation: the two first images are the input of the algorithm, a raw satellite image (RGB + infrared). The third one is the objective, each color representing a class (except for white that means unknown). Finally, the last ones are the prediction and the error made by an algorithm.}
        \label{fig:sem_seg}
    \end{figure}
    

\subsection{Metrics }
    In semantic segmentation, there are two main metrics to measure the quality of an architecture. The first one is the pixel accuracy, defined as follows:
    \begin{equation}
        \text{PA} = \frac{\sum_{c = 1}^{\text{nclass}}\text{NTP}_c}{\text{NP}}
    \end{equation}
    where NP is the number of labeled pixels and $\text{NTP}_c$ the number of True Positive for each class (well classified pixels).\newline
    The second one is the mean Intersection-over-Union, mIoU, defined as follows:
    \begin{equation}
        \text{mIoU} = \frac{1}{\text{nclass}} \sum_{c = 1}^{\text{nclass}} \frac{\text{NTP}_c}{\text{NTP}_c + \text{NFP}_c + \text{NFN}_c}
    \end{equation}
    Where $\text{NFP}_c$ and $\text{NFN}_c$ are the numbers of False Positive and False Negative, respectively. For example $\text{NFP}_1$ is the number of pixels that are in the class 1 in the prediction but not in the ground-truth.\newline
        
\subsection{Total Variation }\label{DEF_TV}
    The TV is a measure of the spatial variation of a signal, it can be defined on a weighted undirected graph $G:=(V,E)$, with $V$, a set of vertices, and $E$ a set of pairs of vertices. An edge $e=\{u,v\}\in E$ is weighted with $\lambda_{\{u,v\}}\in \nbR$. let $x_u$ be a vector define on the vertex $u$ of the graph $G$. In this context, the TV of $x$ over $G$ is defined as :
    \begin{equation}\label{eq_TV}
        \TV = \sum_{\{u,v\}\in E} \lambda_{\{u,v\}} \Phi(x_u,x_v)
    \end{equation}
    With $\Phi$, a function that measures a difference between two variables.\newline
    For example, when $x$ is a scalar we can choose:
    \begin{equation}
        \Phi(x,y) = |x-y|
    \end{equation}
    We will use this definition for our binary case. Indeed, we can show that the prediction with two values for each pixels is equivalent to the difference of this two values compare to zero. This is true because of the presence of the $\softmax$ operation after the regularisation:
    \begin{align}
        \softmax(\{x,y\}) &= \left\{\frac{e^x}{e^x+e^y},\frac{e^y}{e^x+e^y}\right\}\\
        \softmax(\{x,y\}) \cdot \frac{e^{-y}}{e^{-y}} &= \left\{\frac{e^{x-y}}{e^{x-y}+1},\frac{1}{e^{x-y}+1}\right\} = \softmax(\{x-y,0\})
    \end{align}
        
    There are other definitions of the total variation. For example, in \cite{Condat17} multiple versions of TV are presented, especially they are searching a definition of TV on an image (a 2D grid where the edges connect a pixel to its four closest neighbor) that is isotropic and constant to some transformation (like flipping, rotating or take the negative of the image). Our definition \eqref{eq_TV} on an image will not consider the TV to be equal for a vertical line or the same line turne at 45°, it is an-isotropic. This may cause instability during the learning phase. But the neural network can compensate those errors and the computation power needed for the isotropic version is much higher.

    We have choose to study this measure because in lots of situations we know that the TV is low in the data. We then want to use this prior-knowledge to help produce better predictions.

    Finally, we have choose the definition on a graph because it is a more general data representation and we hope that after our research this solution can be extend to other type of data like point-cloud or to other tasks. It also allow us to take some distance from the classical 2D grid definition and try some other connectivity.
    
\subsection{Uses of TV}\label{use_TV}
    Knowing that we want the TV to be low there are multiple ways to integrate it into a neural network. The simplest way is to put it inside the loss $\TVloss = \loss + \alpha \TV$. During the learning phase, the network will try to find a trade-off between minimizing the TV of the prediction and making an accurate one. In some cases achieve a good trade-off might be difficult for the network, and if the $\alpha$ is not well choose it will not work at all. \newline

    Another way to integrate the TV is to use it in another optimization problem . 
    An example of problem involving TV :
    \begin{equation}
        \argmin_{z} \left\{ f(z;y,\lambda) = \frac{1}{2} \|z-y\|^2 + \sum_{\{u,v\}\in E} \lambda_{\{u,v\}} \Phi(z_u,z_v) \right\}
    \end{equation}
    the first term ensure that the result $z$ stay close to the input $y$ and the second is the TV measure, the solver (example in section \ref{DEF_CP}) will have to find balance between those two term. We have some guaranty of optimality as the problem is strictly convex.
    The issues with this formulation are the computation power needed and to decide where the regularization module is integrated. Indeed, solving such a problem is not trivial and as seen before, we update the parameters by minimising the loss function. To do so, a gradient decent method is used but it require that all operation are differentiable. But even when the differentiation is possible, compute the gradient of a solver use a lot of power.
    The easiest location in the architecture would then be to do it after the learning, only on the prediction. This solution avoid the problem of differentiation, but the network will not take into account the effect of the module. A part of our work is to introduce this operation during the learning to make sure the network is "aware" of the effect of the new module and learn to work with it, this location is also useful because it allow to learn the parameters of the regularisation as other weights of the network.

    This definition of the problem is powerful for semantic segmentation for another reason: on an image the result of this problem is the construction of constant connected components (regions with the same value for all pixels), indeed to minimise the second term we need to have a maximum of pixels with the same value. 
    
    \begin{table}[!h]
        \begin{adjustbox}{center}
            \begin{tabular}{|c|c|c|c|}
                \hline
                Name of application & Location & main asset & main limit \\
                \hline\hline
                $\TVloss$ & loss & easy to use & conflicting information \\
                \hline
                Post processing & on the prediction & no need to be derivable & not included in the learning \\
                \hline
                intern problem & any where in the architecture & learn-able parameters & need to be derivable almost every where \\
                \hline
            \end{tabular}
        \end{adjustbox}
        \caption{comparison of spatial regularization techniques}
        \label{tab:my_label}
    \end{table}
    

\subsection{Cut-Pursuit }\label{DEF_CP}
    It can solve the problem define in \ref{use_TV} and some problems of the same class. This algorithm has multiple advantages:
    \begin{enumerate}
        \item solve exactly this problem
        \item give the result under useful formats for the gradient computation
        \item is fast when the number of component is small
        \item works on any graph 
    \end{enumerate}

    The second point is important for the learning phase, indeed to adjust the parameters of the network, the information of the loss has to propagate through all operations. the computation of the gradient of Cut-Pursuit is facilitated by extra information given by the algorithm, as for example the list of pixels in each component.
    The fourth point means that we can define the connectivity between pixels that suit our needs, it is a point we want to study after the work on classical situation.

    There are also some limitations:
    \begin{enumerate}
        \item computation are on CPU only, and the networks are stored in GPU memory, which leads to numerous data transfers.
        \item the algorithm is slow when there are many components, which always happens at the start of the learning because the predictions are random.
        \item when it is parallelized, used for multidimensional data or for other problems that are not convex the optimally is not guaranteed
    \end{enumerate}

    For our application we will integrate Cut-Pursuit as the last operation of the network, the objective is to add the spatial prior-knowledge to the prediction and use it several time will increase the computation power needed to much.

    On this specific task, there are already other algorithms, such as GPU-based projected-Newton (GPN) in \cite{YehEtAl22}. This algorithm solves the same problem but only on a 2D grid with the four closest neighbors. Another major difference with Cut-Pursuit is that for GPN all the $\lambda_{\{u,v\}}$ are at the same value. We will use it in a first time to check if our solution is equivalent in the same task and then to measure the effect of varying connectivity and values of the weights. It is also a comparison point on computation speed and memory use. But, as it works on GPU we have to be careful in those comparison.
    To be complete with this comparison, the authors of this paper use GPN several time in the architecture. They are able to do this because there solution is fast but to achieve a good comparison we will only put it at the end like Cut-Pursuit (we have made one experience with several GPN for completeness). 

    Lastly, there is the question of the values of the weights. When there is only one value, it can be learned as a parameter of the neural network. But in the case where there are multiple weights we have to add a branch to the network. That branch will take a part of the intermediary information generated by the network and generate all the weights, as illustrated in figure \ref{fig:weight_gen}.
    \begin{figure}[!h]
        \centering
        \begin{tikzpicture}[node distance={15mm}, squarednode/.style={shape=rectangle,draw=black!255,fill=white!0,very thick,minimum size = 7mm}]
            \node[shape=rectangle,draw=white!0,fill=white!0,very thick,minimum size = 7mm] (x0) {Network};
            \node[squarednode] (x) [right of = x0]{$x$};
            \node[squarednode] (y) [right of = x]{$y$};
            \node[shape=diamond,draw=black!255,fill=white!0,very thick,minimum size = 7mm] (L) [below of = y] {$\lambda$};
            \node[shape=circle,draw=black!255,fill=white!0,very thick,minimum size = 7mm] (reg) [right of = y] {$Reg$};
            \node[squarednode] (z) [right of = reg] {$z$};
            \node[squarednode] (p) [below right of = z] {$pred$};
            \node[shape=rectangle,draw=white!0,fill=white!0,very thick,minimum size = 7mm] (loss) [above right of = z] {$loss$};
            \draw[->] (x0) to (x);
            \draw[->] (x) to (y);
            \draw[->] (x) to [bend right = 45] (L);
            \draw[->,draw=green] (y) to (reg);
            \draw[->,draw=green] (L) to [bend right = 45] (reg);
            \draw[->,draw=blue] (reg) to (z);
            \draw[->,draw=magenta] (z) to [bend left = 45] (loss);
            \draw[->,draw=cyan] (z) to [bend right = 45] (p);
        \end{tikzpicture}
        \caption{architecture with weight generation}
        \begin{tabular}{c c c c}
            black : & Convolutions & & \\
            green : & passing values & blue : & softmax \\
            magenta : & cross entropie & cyan : & argmax \\
        \end{tabular}
        \label{fig:weight_gen}
    \end{figure}


    
\section{Baseline Network }
To achieve fair comparison we used several classic segmentation architectures from benchmark:
\subsection{Binary case }
    For this simple task the network Segnet from \cite{7803544} is enough. It is built on the same idea as VGG16\cite{brusilovsky:simonyan2014very} (a classification network) and is used as a base for other application, illustration on figure \ref{fig:architecture_VGG16} and \ref{fig:architecture_SegNet}. It is not the best network but the objective is to compare with and without our layer easily.
    It performs on several benchmarks : 57\% mIoU on cityscape and 60\% mIoU on CamVid.\newline
    
\subsection{Multi-class case }
    After several tests, we conclude that Segnet that is learning from scratch need too much data to be at the state of the art level so we search for others networks that are closer to our objective.\newline
    The first network using transfer learning for semantic segmentation is Fully Convolutional Networks for Semantic Segmentation (FCN)\cite{DBLP:journals/corr/LongSD14}. It uses the learnt weights of VGG16 as a base and adds a light decoder cf. figure \ref{fig:architecture_FCN}.
   
    It performs well on the PascalVOC dataset with a mIoU of 65\% but it is over sized (138M parameters).\newline
    There are multiple ways to reduce the size of a network, first by changing some hyper parameters of the FCN. The second solution we have tried is replacing the VGG16\cite{brusilovsky:simonyan2014very} base by the Efficient-Net (https://github.com/lukemelas/EfficientNet-PyTorch and \cite{DBLP:journals/corr/abs-2104-00298}) that count only 5M parameters, and with the light decoder we achieved 8M parameters, as illustrated on figure \ref{fig:architecture_eff}.
    
\section{Results}
\subsection{Binary Study }

    \begin{figure}[!h]
        \centering
        \begin{adjustbox}{center}
            \includegraphics[scale=1]{Images/example_data.png}
        \end{adjustbox}
        \caption{binary case data}
        \label{fig:binary_data}
    \end{figure}

    The task in this case is to find the squares inside the noised images cf. figure \ref{fig:binary_data}.

    With a very small neural network (200 parameters) the results are already good, the two algorithms (CP and TVL) helps in this case by improving the result by 5\% of mIoU, but a bigger network does the same improvements and with a bigger network the regularization isn't improving more. (Table \ref{tab:bin_result} summarizes some results)
    
    The task may be too simple where such regularization is irrelevant, but as in some situation it can help we hope that with a more complex task it will show a real effect.
    
\subsection{Multi-class Study }
    We have chosen to work with the simplest data-set possible, for semantic segmentation a regularly used is PascalVOC, it count 21 classes of common objects.

    We achieve to build an FCN with VGG16 with only 18M parameters by modifying only a few parameters, (on figure \ref{fig:architecture_FCN} we only change the value 4096 in the center). The time needed to build the new network with efficient-net seems to be too long for the gain we have chosen to abandon this solution for now.

\section{Conclusion }
    The proposed method does not give relevant results on the task we studied for now but it might be caused by the simplicity of the task, we have to study further the multi-class case.
    The results with very little network comfort us on the idea that in some situations, this regularization might help, and the numerous publication on spatial regularization seems to confirm this hypothesis.
    Finally the next step is to build the architecture with the different algorithm and test our hypothesis, the spatial regularization based on minimising the total variation can help the learning and improve the prediction of a deep learning architecture on a semantic segmentation task.

\printbibliography

\section{Figures : }

    \begin{figure}[!h]
        \centering
        \begin{adjustbox}{center}
            \includegraphics[scale=0.3]{Images/VGG16_architecture.png}
        \end{adjustbox}
        \caption{illustration of the architecture of VGG16\cite{brusilovsky:simonyan2014very}}
        \label{fig:architecture_VGG16}
    \end{figure}
    
    \begin{figure}[!h]
        \centering
        \begin{adjustbox}{center}
            \includegraphics[scale=0.5]{Images/segnet_architecture.png}
        \end{adjustbox}
        \caption{illustration of the architecture of SegNet\cite{DBLP:journals/corr/LongSD14}}
        \label{fig:architecture_SegNet}
    \end{figure}
    
    \begin{figure}[!h]
        \centering
        \begin{adjustbox}{center}
            \includegraphics[scale=0.5]{Images/Fcn_architecture.png}
        \end{adjustbox}
        \caption{illustration of the architecture of FCN\cite{7803544}}
        \label{fig:architecture_FCN}
    \end{figure}
    
    \begin{figure}[!h]
        \centering
        \begin{adjustbox}{center}
            \includegraphics[scale=0.5]{Images/effnetb0_architecture.png}
        \end{adjustbox}
        \caption{illustration of the architecture of Efficient-net\cite{DBLP:journals/corr/abs-2104-00298}}
        \label{fig:architecture_eff}
    \end{figure}
    
    \begin{table}[!h]
        \begin{adjustbox}{center}
            \begin{tabular}{|c|c|c|c|c|c|m{3cm}|} 
                 \hline
                 Name & IoU & $\sigma$ IoU & Weight & $\sigma$ Weight & Duration epoch (s) & Note\\ [0.5ex] 
                 \hline\hline
                 \textit{Not Regularized} & \textit{67.29\%} & \textit{2.4\%} & \textit{None} & \textit{None} & \textit{1.84} &  \\ 
                 \hline
                 \textit{TV in Loss} & \textit{67.76\%} & \textit{0.82\%} & \textit{None} & \textit{None} & \textit{1.53} & $\lambda = 5$\\
                 \hline
                 Cut-Pursuit & 70.90\% & 2.6\% & 1.68 & 0.5 & 119.26 &   \\
                 \hline
                 TV Layer & 70.24\% & 2.5\% & 1.21 & 0.23 & \textbf{73.78} &   \\
                 \hline
                 Cut-Pursuit & 71.13\% & \textbf{1.97\%} & None & None & 108.49 &  multiple weights \\
                 \hline
                 Cut-Pursuit & 72.75\% & 1.98\% & 1.74 & 0.3 & 157.54 &  8 neighbors \\
                 \hline
                 Cut-Pursuit & 72.23\% & 3.07\% & None & None & 158.84 &  8 neighbors and multiple weights \\
                 \hline
                 TV Layer & \textbf{74.59\%} & 2.02\% & None & None & 176.25 & layer at each level of the encoder\\ [1ex] 
                 \hline
                 \textit{Not Regularized} & \textit{75.05\%} & \textit{1.14\%} & \textit{None} & \textit{None} & \textit{1.58} & \textit{depth 2 (936 parameters)} \\ 
                 \hline
                 \textit{Not Regularized} & \textit{73.44\%} & \textit{1.76\%} & \textit{None} & \textit{None} & \textit{1.68} & \textit{depth 3 (3480 parameters)} \\ 
                 \hline
                 Cut-Pursuit & 75.13\% & 1.18\% & 1.64 & 0.3 & 140,81 & depth 2 \\
                 \hline
                 TV Layer & 73.91\% & 1.4\% & None & None & 252.31 & depth 2 and layer in the encoder \\
                 \hline
                 Cut-Pursuit & 74.59\% & 1.27\% & None & None & 145,32 & depth 2 and multiple weights \\
                 \hline
            \end{tabular}
        \end{adjustbox}
        \caption{Summary of binary results}
        \label{tab:bin_result}
    \end{table}
    
\end{document}