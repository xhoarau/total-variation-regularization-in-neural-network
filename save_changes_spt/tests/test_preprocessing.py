import pyrootutils

root = str(pyrootutils.setup_root(
    search_from=__file__,
    indicator=[".git", "README.md"],
    pythonpath=True,
    dotenv=True))

from typing import Optional

import hydra
from omegaconf import OmegaConf, DictConfig
from pytorch_lightning import LightningDataModule

from src.data import NAG
from src.visualization.visualization import *
import os.path as osp

from src.metrics import *

from tqdm import tqdm

OmegaConf.register_new_resolver("eval", eval)

@hydra.main(version_base="1.2", config_path=root + "/configs", config_name="train.yaml")
def test(cfg: DictConfig) -> Optional[float]:
    
    if cfg.get("device_id"):
        torch.cuda.set_device("cuda:"+str(cfg.device_id))    
    
    # comment this line to test on the complete dataset
    cfg.datamodule.test_subset = 5 # size of the subset to test on

    datamodule: LightningDataModule = hydra.utils.instantiate(cfg.datamodule) # create datamodule
    datamodule.setup(test=True) # create train dataset and launch preprocessing
    # set test to True to test only on train set
    
    list_train_processed_data = datamodule.train_dataset.processed_file_names[:cfg.datamodule.test_subset]

    nlevel = len(cfg.datamodule.pcp_regularization)
    
    # keep track of size and quality of over segmentation
    meters = []
    for l in range(nlevel):
        meters.append(averageMeter())
    cm = ConfusionMatrix(cfg.datamodule.num_classes)

    for path in tqdm(list_train_processed_data):
        nag = NAG.load(osp.join(datamodule.train_dataset.root,"processed",path))
        # comment to check only the numbers
        #show(nag,"visualise_preprocessed_"+osp.splitext(osp.split(path)[1])[0],keys=["image","label","SegError"],title="preprocessed_"+osp.splitext(osp.split(path)[1])[0],class_color=datamodule.train_dataset.class_colors)
        graph_visualization(osp.join(datamodule.train_dataset.root,"processed",path),"visualise_preprocessed_"+osp.splitext(osp.split(path)[1])[0],class_color=datamodule.train_dataset.class_colors)
        for l in range(nlevel):
            sizes = nag.get_sub_size(l+1).numpy()
            meters[l].update(sizes)
        cm.update(nag[1].y.argmax(dim=1)[nag.get_super_index(high=1,low=0)],nag[0].y)
    
    for l in range(nlevel):
        print("avg sp size level ",l+1," : ",meters[l].avg)
        print("std sp size level ",l+1," : ",meters[l].std)
    cm.print_metrics(class_names = datamodule.train_dataset.class_names)

if __name__ == "__main__":
    test()
