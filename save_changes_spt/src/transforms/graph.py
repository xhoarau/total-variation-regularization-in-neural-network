import torch
import numpy as np
import itertools
from scipy.spatial import Delaunay
from torch_scatter import scatter_mean, scatter_std, scatter_max
from torch_geometric.utils import add_self_loops
from src.transforms import Transform
import src
from src.data import NAG,Data
from src.utils import grid_to_graph, is_trimmed, \
    PIXEL_FEATURES, SEGMENT_BASE_FEATURES, \
    SUBEDGE_FEATURES, ON_THE_FLY_HORIZONTAL_FEATURES, \
    ON_THE_FLY_VERTICAL_FEATURES, sanitize_keys # removed cluster_radius_nn for images
from src.metrics import averageMeter
import time
import sys
import os.path as osp

dependencies_folder = osp.dirname(osp.dirname(osp.abspath(__file__)))
sys.path.append(dependencies_folder)
sys.path.append(osp.join(dependencies_folder, "dependencies/grid_graph/python/bin"))
sys.path.append(osp.join(dependencies_folder, "dependencies/multilabel-potrace/python/bin"))
from grid_graph import edge_list_to_forward_star
from multilabel_potrace_polygraph import multilabel_potrace_polygraph
from multiprocessing.shared_memory import ShareableList

__all__ = [
    'AdjacencyGraph', 'SegmentFeatures','ShapeInterfacePolygones',
    # 'OnTheFlyHorizontalEdgeFeatures', 
    'OnTheFlyVerticalEdgeFeatures', 
    'NAGAddSelfLoops', 'NodeSize']


class AdjacencyGraph(Transform):
    """Create the adjacency graph in `edge_index` and `edge_attr` based
    on image size stored in the data and the chosen connectivity

    NB: this graph is directed wrt Pytorch Geometric, but cut-pursuit
    happily takes this as an input.

    :param dist: list of int
        length of the edges calculated as norm_inf(2^dist)
    :param connectivity: int or list of int 
        If int : density max for all distances
        If list : density of each distances, len(dist) = len(connectivity)
    """

    def __init__(self, dist=[0], connectivity=0):
        super().__init__()
        assert isinstance(dist,list) and all(isinstance(x,int) for x in dist), \
            "dist has to be a list of int"
        self.dist = np.array(dist)
        assert isinstance(connectivity,int) or (isinstance(connectivity,list) and all(isinstance(x,int) for x in dist)), \
            "connectivity has to be an int or a list of int"
        if isinstance(connectivity,int):
            self.connectivity = np.array([connectivity])
        else:
            self.connectivity = np.array(connectivity)

    def _process(self, data):
        
        # Save edges and edge features in data
        edge,attr = grid_to_graph(self.dist,self.connectivity,np.array(data.image.shape[-2:]))
        data.edge_index = torch.from_numpy(edge).long().to(data.device)
        data.edge_attr = torch.from_numpy(attr).float().to(data.device)
        return data


class SegmentFeatures(Transform): 
    """Compute segment features for all the NAG levels except its first
    (ie the 0-level). These are handcrafted node features that will be
    saved in the node attributes. To make use of those at training time,
    remember to move them to the `x` attribute using `AddKeysTo` and
    `NAGAddKeysTo`.

    The supported feature keys are the following:
        - node_size : number of pixels in the superpixel
        - perimeter: number of edges of level 0 linking a node to its neighbors

        - mean_rgb
        - mean_hsv
        - mean_lab
        - mean_intensity
        
        - std_rgb
        - std_hsv
        - std_lab
        - std_intensity

        - log_size
        - log_perimeter

    :param keys: List(str), str, or None
        Features to be computed segment-wise and saved under `<key>`.
        If None, all supported features will be computed
    :param mean_keys: List(str), str, or None
        Features to be computed from the pixels and the segment-wise
        mean aggregation will be saved under `mean_<key>`. If None, all
        supported features will be computed
    :param std_keys: List(str), str, or None
        Features to be computed from the pixels and the segment-wise
        std aggregation will be saved under `std_<key>`. If None, all
        supported features will be computed
    :param strict: bool
        If True, will raise an exception if an attribute from key is
        not within the input pixel Data keys
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG
    _NO_REPR = ['strict']

    def __init__(
            self,
            keys=None,
            mean_keys=None,
            std_keys=None,
            edge_keys=None,
            strict=True,
            num_classes=None):
        super().__init__()
        self.keys = sanitize_keys(keys, default=SEGMENT_BASE_FEATURES)
        self.mean_keys = sanitize_keys(mean_keys, default=PIXEL_FEATURES)
        self.std_keys = sanitize_keys(std_keys, default=PIXEL_FEATURES)
        self.edge_keys = sanitize_keys(edge_keys, default=ON_THE_FLY_HORIZONTAL_FEATURES)
        self.strict = strict
        assert num_classes is not None
        self.num_classes=num_classes
        self.normalize = []

    def _process(self, nag):
        for i_level in range(1, nag.num_levels):
            while len(self.normalize)<i_level+1:
                self.normalize.append({})
            nag = _compute_cluster_features(
                i_level,
                nag,
                keys=self.keys,
                mean_keys=self.mean_keys,
                std_keys=self.std_keys,
                edge_keys=self.edge_keys,
                strict=self.strict,
                normalize = self.normalize[i_level])
            nag = _compute_edge_features(
                i_level,
                nag,
                edge_keys=self.edge_keys,
                normalize = self.normalize[i_level])
        if len(self.normalize)==i_level+1:
            self.normalize.append(torch.zeros(self.num_classes,device=nag.device))
        self.normalize[i_level+1] += nag[i_level].y.sum(dim=0)[:self.num_classes]
        return nag


def _compute_cluster_features(
        i_level,
        nag,
        keys=None,
        mean_keys=None,
        std_keys=None,
        edge_keys=None,
        strict=True,
        normalize=None): 
    assert isinstance(nag, NAG)
    assert i_level > 0, "Cannot compute cluster features on level-0"
    assert nag[0].num_nodes < np.iinfo(np.uint32).max, \
        "Too many nodes for `uint32` indices"

    keys = sanitize_keys(keys, default=SEGMENT_BASE_FEATURES)
    mean_keys = sanitize_keys(mean_keys, default=PIXEL_FEATURES)
    std_keys = sanitize_keys(std_keys, default=PIXEL_FEATURES)
    edge_keys = sanitize_keys(edge_keys, default=ON_THE_FLY_HORIZONTAL_FEATURES)

    # Recover the i_level Data object we will be working on
    data = nag[i_level]
    num_nodes = data.num_nodes
    device = nag.device

    # Compute how many level-0 pixels each level cluster contains
    sub_size = nag.get_sub_size(i_level, low=0)

    # Get the cluster index each pixel belongs to (to calculate mean and std over super_lusters, perimeter and normal)
    super_index = nag.get_super_index(i_level)

    if 'node_size' in keys or 'e_size' in edge_keys:
        data.node_size = sub_size
        if 'node_size' not in normalize:
            normalize['node_size'] = [averageMeter()]
        normalize['node_size'][0].update(data.node_size)

    if 'log_size' in keys or 'e_log_size' in edge_keys:
        data.log_size = (torch.log(sub_size + 1).view(-1, 1) - np.log(2)) / 10
        if 'log_size' not in normalize:
            normalize['log_size'] = [averageMeter()]
        normalize['log_size'][0].update(data.log_size)

    if 'perimeter' in keys or 'e_perimeter' in edge_keys or 'log_perimeter' in keys or 'e_log_perimeter' in edge_keys:
        assert hasattr(nag[0],"edge_index"), "need level 0 edges to compute the perimeters"
        edge_index = nag[0].edge_index
        perimeter = torch.bincount(super_index[edge_index[:,super_index[edge_index[0]]!=super_index[edge_index[1]]]].flatten()).to(device).view(-1,1)

        if 'perimeter' in keys or 'e_perimeter' in edge_keys:
            data.perimeter = perimeter
            if 'perimeter' not in normalize:
                normalize['perimeter'] = [averageMeter()]
            normalize['perimeter'][0].update(data.perimeter)

        if 'log_perimeter' in keys or 'e_log_perimeter' in edge_keys:
            data.log_perimeter = (torch.log(perimeter+1) - np.log(2))/10
            if 'log_perimeter' not in normalize:
                normalize['log_perimeter'] = [averageMeter()]
            normalize['log_perimeter'][0].update(data.node_size)

    # Add the mean of pixel attributes, identified by their key
    for key in mean_keys:
        f = getattr(nag[0], key, None)
        if f is None and strict:
            raise ValueError(f"No pixel key `{key}` to build 'mean_{key} key'")
        if f is None:
            continue
        data[f'mean_{key}'] = scatter_mean(nag[0][key], super_index, dim=0)
        if f'mean_{key}' not in normalize:
            normalize[f'mean_{key}'] = [averageMeter() for _ in range(data[f'mean_{key}'].shape[1])]
        for i in range(data[f'mean_{key}'].shape[1]):
            normalize[f'mean_{key}'][i].update(data[f'mean_{key}'][:,i])

    # Add the std of pixel attributes, identified by their key
    for key in std_keys:
        f = getattr(nag[0], key, None)
        if f is None and strict:
            raise ValueError(f"No pixel key `{key}` to build 'std_{key} key'")
        if f is None:
            continue
        data[f'std_{key}'] = scatter_std(nag[0][key], super_index, dim=0)
        if f'std_{key}' not in normalize:
            normalize[f'std_{key}'] = [averageMeter() for _ in range(data[f'std_{key}'].shape[1])]
        for i in range(data[f'std_{key}'].shape[1]):
            normalize[f'std_{key}'][i].update(data[f'std_{key}'][:,i])

    # Update the i_level Data in the NAG
    nag._list[i_level] = data

    return nag

def _compute_edge_features(
        i_level,
        nag,
        edge_keys=None,
        normalize=None): 
    """ compute edge features to be saved (use more memory but less processing time)
    for all i->j and j->i horizontal edges of the NAG level, 
    impossible for the first level (ie the 0-level)."""
    assert isinstance(nag, NAG)
    assert i_level > 0, "Cannot compute edge features on level-0"
    assert nag[0].num_nodes < np.iinfo(np.uint32).max, \
        "Too many nodes for `uint32` indices"

    edge_keys = sanitize_keys(edge_keys, default=ON_THE_FLY_HORIZONTAL_FEATURES)

    # Recover the i_level Data object we will be working on
    data = nag[i_level]
    num_nodes = data.num_nodes
    device = nag.device
    
    se = data.edge_index

    if 'e_size' in edge_keys:
        data.e_size = torch.cat((data.node_size[se].squeeze().transpose(1,0),data.node_size[se].squeeze().flip(0).transpose(1,0)), dim=0)

    if 'e_log_size' in edge_keys:
        data.e_log_size = torch.cat((data.log_size[se].squeeze().transpose(1,0),data.log_size[se].squeeze().flip(0).transpose(1,0)), dim=0)

    if 'e_perimeter' in edge_keys:
        data.e_perimeter = torch.cat((data.perimeter[se].squeeze().transpose(1,0),data.perimeter[se].squeeze().flip(0).transpose(1,0)), dim=0)
    
    if 'e_log_perimeter' in edge_keys:
        data.e_log_perimeter = torch.cat((data.log_perimeter[se].squeeze().transpose(1,0),data.log_perimeter[se].squeeze().flip(0).transpose(1,0)), dim=0)

    if 'interface_length' in edge_keys:
        edge_index = nag[0].edge_index
        super_index = nag.get_super_index(i_level)
        n_node = torch.max(super_index).item()+1
        count = torch.bincount(super_index[edge_index[0]]+super_index[edge_index[1]]*n_node)
        f = (count.reshape([n_node,n_node])+count.reshape([n_node,n_node]).transpose(1,0))[se[0],se[1]]
        if 'interface_length' not in normalize:
            normalize['interface_length'] = [averageMeter()]
        normalize['interface_length'][0].update(f)
        data.interface_length = torch.cat(((f).view(-1,1),(f).view(-1,1)),dim=0)
    
    if 'centroid_dir' in edge_keys or 'centroid_dist' in edge_keys:
        # Compute the distance and direction between the segments'
        # centroids
        se_centroid_dir = data.pos[se[1]] - data.pos[se[0]]
        se_centroid_dist = se_centroid_dir.norm(dim=1).view(-1, 1)
        se_centroid_dir /= se_centroid_dist.view(-1, 1)
        se_centroid_dist = se_centroid_dist.sqrt()

        # Sanity checks on normalized directions
        se_centroid_dir[se_centroid_dir.isnan()] = 0
        se_centroid_dir = se_centroid_dir.clip(-1, 1)

        if 'centroid_dir' in edge_keys:
            if 'centroid_dir' not in normalize:
                normalize['centroid_dir'] = [averageMeter() for _ in range(se_centroid_dir.shape[1])]
            for i in range(se_centroid_dir.shape[1]):
                normalize['centroid_dir'][i].update(se_centroid_dir[:,i])
            data.centroid_dir = torch.cat((se_centroid_dir, -se_centroid_dir), dim=0)

        if 'centroid_dist' in edge_keys:
            if 'centroid_dist' not in normalize:
                normalize['centroid_dist'] = [averageMeter()]
            normalize['centroid_dist'][0].update(se_centroid_dist)
            data.centroid_dist = torch.cat((se_centroid_dist, se_centroid_dist), dim=0)
    
    # Update the edge_index with j->i edges
    data.edge_index = torch.cat((se, se.flip(0)), dim=1)

    # remove all in edge_attr
    if data.edge_attr is not None:
        data.edge_attr = None

    # Update the i_level Data in the NAG
    nag._list[i_level] = data

    return nag

class ShapeInterfacePolygones(Transform): 
    """Compute the polygones and polylines for all super pixels at each level.

    :param straight_line_tol: float
        precision of the aproximation (cf. https://gitlab.com/1a7r0ch3/multilabel-potrace)
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG
    _NO_REPR = ['strict']

    def __init__(
            self,
            straight_line_tol=1.0):
        super().__init__()
        self.straight_line_tol = straight_line_tol

    def _process(self, nag):
        w,h = nag[0].image.shape[-2:]
        for i_level in range(1, nag.num_levels):
            data = nag[i_level]

            # edges liste to forward star (CSR)
            source_csr, target, reindex = edge_list_to_forward_star(
                data.num_nodes, data.edge_index.T.contiguous().cpu().numpy())
            source_csr = source_csr.astype(np.uint32)
            target = target.astype(np.uint16)
            
            # compute shapes and interfaces aproximations
            Shapes,Interfaces = multilabel_potrace_polygraph(np.copy(nag.get_super_index(i_level).reshape(w,h).numpy().astype(np.uint16)), source_csr, target, straight_line_tol=self.straight_line_tol)
            
            shape_vertex = torch.from_numpy(Shapes[0].astype(np.float32)).to(data.device).transpose(0,1)
            shape_edges = torch.from_numpy(Shapes[1].astype(np.int64)).to(data.device)
            shape_index = torch.from_numpy(Shapes[2].astype(np.int64)).to(data.device)

            interface_vertex = torch.from_numpy(Interfaces[0].astype(np.float32)).to(data.device).transpose(0,1)
            interface_edges = torch.from_numpy(Interfaces[1].astype(np.int64)).to(data.device)
            interface_index = torch.from_numpy(Interfaces[2].astype(np.int64)).to(data.device)

            # Normalize shapes and interfaces
            ## each polygones or polylines is centered on it's centroïd, scaled in the unit sphere and rotate such that the farthest points from the center is on the abscissa axis
            centers = scatter_mean(shape_vertex,shape_index,dim=0)
            shape_vertex = shape_vertex-centers[shape_index]
            
            centers = scatter_mean(interface_vertex,interface_index,dim=0)
            interface_vertex = interface_vertex-centers[interface_index]
    
            distances = torch.sqrt(torch.sum(torch.pow(shape_vertex,2),dim=1))
            max_dist,max_dist_index = scatter_max(distances,shape_index)
            distances = distances/max_dist[shape_index]
            angles = torch.atan2(shape_vertex[:,1],shape_vertex[:,0])
            angles = angles-angles[max_dist_index][shape_index]
            shape_vertex = torch.stack((distances*torch.cos(angles),distances*torch.sin(angles)),dim=1)

            distances = torch.sqrt(torch.sum(torch.pow(interface_vertex,2),dim=1))
            max_dist,max_dist_index = scatter_max(distances,interface_index)
            distances = distances/max_dist[interface_index]
            angles = torch.atan2(interface_vertex[:,1],interface_vertex[:,0])
            interface_null = max_dist_index==angles.shape[0]
            max_dist_index[interface_null]=0
            angles = angles-angles[max_dist_index[max_dist_index!=angles.shape[0]]][interface_index]
            interface_vertex = torch.stack((distances*torch.cos(angles),distances*torch.sin(angles)),dim=1)
            
            interface_vertex = torch.cat((interface_vertex,torch.zeros([2*interface_null.sum(),2])),dim=0)
            interface_edges = torch.cat((interface_edges,(torch.arange(2*interface_null.sum())+angles.shape[0]).reshape([2,interface_null.sum()])),dim=1)
            interface_index = torch.cat((interface_index,torch.nonzero(interface_null).repeat_interleave(2)))
 
            # store the new data
            data.shapes = Data(edge_index=shape_edges, pos=shape_vertex, batch_node=shape_index) # batch in attribute for batch creation
            data.interfaces = Data(edge_index=interface_edges, pos=interface_vertex, batch_node=interface_index)
            
        return nag

"""
class OnTheFlyHorizontalEdgeFeatures(Transform):
    ### add a " juste after ###
    ""Compute edge features "on-the-fly" for all i->j and j->i
    horizontal edges of the NAG levels except its first (ie the
    0-level).

    Expects only trimmed edges as input, along with some edge-specific
    attributes that cannot be recovered from the corresponding source
    and target node attributes (see `src.utils.to_trimmed`).

    Accepts input edge_attr to be float16, to alleviate memory use and
    accelerate data loading and transforms. Output edge_<key> will,
    however, be in float32.

    Optionally adds some edge features that can be recovered from the
    source and target node attributes.

    Builds the j->i edges and corresponding features based on their i->j
    counterparts in the trimmed graph.

    Equips the output NAG with all i->j and j->i nodes and corresponding
    features.

    Note: this transform is intended to be called after all sampling
    transforms, to mitigate compute and memory impact of horizontal
    edges.

    The supported feature keys are the following:
      - centroid_dir: unit-normalized direction between the i and
        j centroids
      - centroid_dist: distance between the i and j centroids

      - e_size: size of the source and the target
      - e_perimeter: perimeter of the source and the target

      - e_log_size: log size of the source and the target
      - e_log_perimeter: log perimeter of the source and the target

      - interface_length: number of edges of level 0 linking the source to the target of the edge 
    
    :param keys: List(str)
        Features to be computed. Attributes will be saved under `<key>`
    :param use_mean_normal: bool
        Whether the 'normal' or the 'mean_normal' segment attribute
        should be used for computing normal-related edge features
    ""
    ### add a " juste before ###

    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def __init__(
            self, keys=None):
        super().__init__()
        self.keys = sanitize_keys(keys, default=ON_THE_FLY_HORIZONTAL_FEATURES)
        self.global_file_name = None
    
    def _process(self, nag):
        for i_level in range(1, nag.num_levels):
            nag._list[i_level] = _on_the_fly_horizontal_edge_features(
                nag,
                i_level,
                keys=self.keys,
                norm_val=torch.load(self.global_file_name)[i_level])
        return nag


def _on_the_fly_horizontal_edge_features(
        nag,i_level, keys=None, norm_val = None):
    ### add a " juste after ###
    ""Compute all edges and edge features for a horizontal graph, given
    a trimmed graph and some precomputed edge attributes.
    ""
    ### add a " juste before ###

    for key in norm_val:
        if norm_val[key].device!=nag.device:
            norm_val[key] = norm_val[key].to(nag.device)

    keys = sanitize_keys(keys, default=ON_THE_FLY_HORIZONTAL_FEATURES)

    data = nag._list[i_level]

    # Recover the edges between the segments
    se = data.edge_index


    data.raise_if_edge_keys()

    assert is_trimmed(se), \
        "Expects the graph to be trimmed, consider using " \
        "`src.utils.to_trimmed()` before computing the features"
    
    if 'e_size' in keys:
        assert getattr(data, 'node_size', None) is not None, \
            "Expected input Data to have 'node_size' attribute"
    if 'e_log_size' in keys:
        assert getattr(data, 'log_size', None) is not None, \
            "Expected input Data to have a 'log_size' attribute"
    if 'e_perimeter' in keys:
        assert getattr(data, 'perimeter', None) is not None, \
            "Expected input Data to have a 'perimeter' attribute"
    if 'e_log_perimeter' in keys:
        assert getattr(data, 'log_perimeter', None) is not None, \
            "Expected input Data to have a 'log_perimeter' attribute"

    f_list = []
    if 'e_size' in keys:
        f_list.append(torch.cat(((data.node_size[se].squeeze().transpose(1,0)-norm_val['node_size'][0])/norm_val['node_size'][1],
            (data.node_size[se].squeeze().flip(0).transpose(1,0)-norm_val['node_size'][0])/norm_val['node_size'][1]), dim=0))

    if 'e_log_size' in keys:
        f_list.append(torch.cat(((data.log_size[se].squeeze().transpose(1,0)-norm_val['log_size'][0])/norm_val['log_size'][1],
            (data.log_size[se].squeeze().flip(0).transpose(1,0)-norm_val['log_size'][0])/norm_val['log_size'][1]), dim=0))

    if 'e_perimeter' in keys:
        f_list.append(torch.cat(((data.perimeter[se].squeeze().transpose(1,0)-norm_val['perimeter'][0])/norm_val['perimeter'][1],
            (data.perimeter[se].squeeze().flip(0).transpose(1,0)-norm_val['perimeter'][0])/norm_val['perimeter'][1]), dim=0))

    if 'e_log_perimeter' in keys:
        f_list.append(torch.cat(((data.log_perimeter[se].squeeze().transpose(1,0)-norm_val['log_perimeter'][0])/norm_val['log_perimeter'][1],
            (data.log_perimeter[se].squeeze().flip(0).transpose(1,0)-norm_val['log_perimeter'][0])/norm_val['log_perimeter'][1]), dim=0))

    if 'interface_length' in keys:
        edge_index = nag[0].edge_index
        super_index = nag.get_super_index(i_level)
        n_node = torch.max(super_index).item()+1
        count = torch.bincount(super_index[edge_index[0]]+super_index[edge_index[1]]*n_node)
        f = (count.reshape([n_node,n_node])+count.reshape([n_node,n_node]).transpose(1,0))[se[0],se[1]]
        if norm_val is not None:
            f = (f-norm_val['interface_length'][0])/norm_val['interface_length'][1]
        f_list.append(torch.cat(((f).view(-1,1),(f).view(-1,1)),dim=0))
    
    if 'centroid_dir' in keys or 'centroid_dist' in keys:
        # Compute the distance and direction between the segments'
        # centroids
        se_centroid_dir = data.pos[se[1]] - data.pos[se[0]]
        se_centroid_dist = se_centroid_dir.norm(dim=1).view(-1, 1)
        se_centroid_dir /= se_centroid_dist.view(-1, 1)
        se_centroid_dist = se_centroid_dist.sqrt()

        # Sanity checks on normalized directions
        se_centroid_dir[se_centroid_dir.isnan()] = 0
        se_centroid_dir = se_centroid_dir.clip(-1, 1)

        if 'centroid_dir' in keys:
            if norm_val is not None:
                se_centroid_dir = (se_centroid_dir-norm_val['centroid_dir'][0])/norm_val['centroid_dir'][1]
            f_list.append(torch.cat((se_centroid_dir, -se_centroid_dir), dim=0))

        if 'centroid_dist' in keys:
            if norm_val is not None:
                se_centroid_dist = (se_centroid_dist-norm_val['centroid_dist'][0])/norm_val['centroid_dist'][1]
            f_list.append(torch.cat((se_centroid_dist, se_centroid_dist), dim=0))

    # Update the edge_index with j->i edges
    data.edge_index = torch.cat((se, se.flip(0)), dim=1)

    # Update all edge features into edge_attr and remove all other
    # edge_<key> to save memory
    for k in ['edge_attr'] + data.edge_keys:
        data[k] = None
    if len(f_list) > 0:
        data.edge_attr = torch.cat(f_list, dim=1)

    return data
"""

class OnTheFlyVerticalEdgeFeatures(Transform):
    """Compute edge features "on-the-fly" for all vertical edges of the
    NAG levels.

    Optionally build some edge features that can be recovered from the
    source and target node attributes.

    Note: this transform is intended to be called after all sampling
    transforms, to mitigate compute and memory impact of vertical
    edges.

    The supported feature keys are the following:
      - centroid_dir: unit-normalized direction between the child
        centroid and the parent centroid
      - centroid_dist: distance between the child and parent centroids
      - normal_angle: cosine of the angle between the child and parent
        normals
      - log_length: parent/child log length ratio
      - log_surface: parent/child log surface ratio
      - log_volume: parent/child log volume ratio
      - log_size: parent/child log size ratio

    :param keys: List(str)
        Features to be computed. Attributes will be saved under `<key>`
    :param use_mean_normal: bool
        Whether the 'normal' or the 'mean_normal' segment attribute
        should be used for computing normal-related edge features
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def __init__(self, keys=None, use_mean_normal=False):
        super().__init__()
        self.keys = sanitize_keys(keys, default=ON_THE_FLY_VERTICAL_FEATURES)
        self.use_mean_normal = use_mean_normal

    def _process(self, nag):
        for i_level in range(1, nag.num_levels):
            data_child = nag[i_level - 1]
            data_parent = nag[i_level]

            # For level-0 pixels, we artificially set 'mean_normal' from
            # 'normal', if need be
            if self.use_mean_normal and i_level == 1:
                if getattr(data_child, 'mean_normal', None):
                    data_child.mean_normal = getattr(data_child, 'normal', None)

            nag._list[i_level - 1] = _on_the_fly_vertical_edge_features(
                data_child,
                data_parent,
                keys=self.keys,
                use_mean_normal=self.use_mean_normal)
        return nag


def _on_the_fly_vertical_edge_features(
        data_child, data_parent, keys=None, use_mean_normal=False):
    """Compute edge features for a vertical graph, given child and
    parent nodes.
    """
    keys = sanitize_keys(keys, default=ON_THE_FLY_VERTICAL_FEATURES)

    if len(keys) == 0:
        return data_child

    normal_key = 'mean_normal' if use_mean_normal else 'normal'

    # Recover the parent index of each child node
    idx = data_child.super_index
    assert idx is not None, \
        "Expected input child Data to have a 'super_index' attribute"

    for d in [data_child, data_parent]:
        if 'normal_angle' in keys:
            assert getattr(d, normal_key, None) is not None, \
                f"Expected input Data to have a '{normal_key}' attribute"
        if 'log_length' in keys:
            assert getattr(d, 'log_length', None) is not None, \
                "Expected input Data to have a 'log_length' attribute"
        if 'log_surface' in keys:
            assert getattr(d, 'log_surface', None) is not None, \
                "Expected input Data to have a 'log_surface' attribute"
        if 'log_volume' in keys:
            assert getattr(d, 'log_volume', None) is not None, \
                "Expected input Data to have a 'log_volume' attribute"
        if 'log_size' in keys:
            assert getattr(d, 'log_size', None) is not None, \
                "Expected input Data to have a 'log_size' attribute"

    f_list = []

    if 'centroid_dir' in keys or 'centroid_dist' in keys:
        # Compute the distance and direction between the child and
        # parent segments' centroids
        ve_centroid_dir = data_parent.pos[idx] - data_child.pos
        ve_centroid_dist = ve_centroid_dir.norm(dim=1)
        ve_centroid_dir /= ve_centroid_dist.view(-1, 1)
        ve_centroid_dist = ve_centroid_dist.sqrt()

        # Sanity checks on normalized directions
        ve_centroid_dir[ve_centroid_dir.isnan()] = 0
        ve_centroid_dir = ve_centroid_dir.clip(-1, 1)

        if 'centroid_dir' in keys:
            f_list.append(ve_centroid_dir)

        if 'centroid_dist' in keys:
            f_list.append(ve_centroid_dist.view(-1, 1))

    if 'normal_angle' in keys:
        child_normal = getattr(data_child, normal_key, None)
        parent_normal = getattr(data_parent, normal_key, None)
        f = (child_normal * parent_normal[idx]).sum(dim=1).abs()
        f_list.append(f.view(-1, 1))

    if 'log_length' in keys:
        f = data_parent.log_length[idx] - data_child.log_length
        f_list.append(f.view(-1, 1))

    if 'log_surface' in keys:
        f = data_parent.log_surface[idx] - data_child.log_surface
        f_list.append(f.view(-1, 1))

    if 'log_volume' in keys:
        f = data_parent.log_volume[idx] - data_child.log_volume
        f_list.append(f.view(-1, 1))

    if 'log_size' in keys:
        f = data_parent.log_size[idx] - data_child.log_size
        f_list.append(f.view(-1, 1))

    # Stack all the vertical edge features into the child 'v_edge_attr'
    data_child.v_edge_attr = None
    if len(f_list) > 0:
        data_child.v_edge_attr = torch.cat(f_list, dim=1)

    return data_child


class NAGAddSelfLoops(Transform):
    """Add self-loops to all NAG levels having a horizontal graph. If
    the edges have attributes, the self-loops will receive 0-features.
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def _process(self, nag):
        for i_level in range(1, nag.num_levels):

            # Skip if the level has no horizontal graph
            if not nag[i_level].has_edges:
                continue

            # Recover edges and attributes
            num_nodes = nag[i_level].num_nodes
            edge_index = nag[i_level].edge_index
            edge_attr = nag[i_level].edge_attr


            nag[i_level].raise_if_edge_keys()

            # Add self-loops
            edge_index, edge_attr = add_self_loops(
                edge_index,
                edge_attr=edge_attr,
                num_nodes=num_nodes,
                fill_value=0)

            # Update the edges and attributes
            nag[i_level].edge_index = edge_index
            nag[i_level].edge_attr = edge_attr

        return nag

class NodeSize(Transform):
    """Compute the number of `low`-level elements are contained in each
    segment, at each above-level. Results are save in the `node_size`
    attribute of the corresponding Data objects.

    Note: `low=-1` is accepted when level-0 has a `sub` attribute
    (ie level-0 points are themselves segments of `-1` level absent
    from the NAG object).

    :param low: int
        Level whose elements we want to count
    """

    _IN_TYPE = NAG
    _OUT_TYPE = NAG

    def __init__(self, low=0):
        super().__init__()
        assert isinstance(low, int) and low >= -1
        self.low = low

    def _process(self, nag):
        for i_level in range(self.low + 1, nag.num_levels):
            nag[i_level].node_size = nag.get_sub_size(i_level, low=self.low)
        return nag
