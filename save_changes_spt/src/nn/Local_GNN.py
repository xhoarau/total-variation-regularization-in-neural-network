"""
from git : https://github.com/lukasknobel/ShapeGNN
"""
import torch
from torch import nn as nn
from torch_geometric import nn as geom_nn
from torch_sparse import SparseTensor

from src.nn import MLP
from src.nn.fusion import CatFusion 

from src.data import NAG,Data

import threading as th
        

__all__ = ['GraphEncoder','Shape_Interface_Encoders']

### from BuildingBlocks #####################################################################################################################################

class ConvBlock(nn.Module):
    """
    Implement multiple graph convolution
    """
    def __init__(self, conv_layer_type: str, agg, act_fn, nodes, get_linear_block, k=20):
        """
        :param conv_layer_type: name of graph convolution as define in torch_geometric : 'edgeconv', 'dynamicedgeconv', 'ginconv', 'gcnconv', 'gatconv', 'sageconv', 'genconv', 'transformerconv', 'gatv2conv' 
        :param agg: aggregation in EdgeConv : 'add', 'mean', 'max'
        :param act_fn: activation function: torch.nn.Module
        :param nodes: input and output size of each mlp layer
        :param get_linear_block: function creating MLP
        :param k: number of neigbors for DynamicEdgeConv 
        """
        super().__init__()
        self.conv_layer_type = conv_layer_type
        self.mlp_block = None
        self.act_fn = act_fn
        self.k = k
        self.input_dim = nodes[0]
        self.output_dim = nodes[-1]
        if conv_layer_type == 'edgeconv':
            mod_nodes = list(nodes)
            mod_nodes[0] *= 2
            mlp = get_linear_block(mod_nodes)
            conv_layer = geom_nn.EdgeConv(mlp, aggr=agg)
        elif conv_layer_type == 'dynamicedgeconv':
            mod_nodes = list(nodes)
            mod_nodes[0] *= 2
            mlp = get_linear_block(mod_nodes)
            conv_layer = geom_nn.DynamicEdgeConv(nn=mlp, k=self.k, aggr=agg)
        elif conv_layer_type == 'ginconv':
            mlp = get_linear_block(nodes)
            conv_layer = geom_nn.GINConv(mlp, aggr=agg)
        else:
            if len(nodes) > 2:
                self.mlp_block = get_linear_block(nodes[:-1])
            if conv_layer_type == 'gcnconv':
                conv_layer = geom_nn.GCNConv(nodes[-2], nodes[-1])
            elif conv_layer_type == 'gatconv':
                conv_layer = geom_nn.GATConv(nodes[-2], nodes[-1])
            elif conv_layer_type == 'sageconv':
                conv_layer = geom_nn.SAGEConv(nodes[-2], nodes[-1])
            elif conv_layer_type == 'genconv':
                conv_layer = geom_nn.GENConv(nodes[-2], nodes[-1])
            elif conv_layer_type == 'transformerconv':
                conv_layer = geom_nn.TransformerConv(nodes[-2], nodes[-1])
            elif conv_layer_type == 'gatv2conv':
                conv_layer = geom_nn.GATv2Conv(nodes[-2], nodes[-1])
            else:
                raise ValueError(f'Unknown conv_layer_type {conv_layer_type}')
        self.conv_layer = conv_layer

    def forward(self, x, adj_t: SparseTensor, pos):
        output = x
        if self.mlp_block is not None:
            output = self.mlp_block(output)
        else:
            output = self.conv_layer(output, adj_t)

        output = self.act_fn(output)
        return output, adj_t, pos


class ResBlock(nn.Module):
    """
    Based on ConvBlock adding a residual connection
    """
    def __init__(self, conv_block: ConvBlock, residual_type='add',verbose = False):
        """
        :param conv_block: A ConvBlock
        :param residual_type: type of connection: 'add', 'mean', 'no'
        """
        super().__init__()
        self.block = conv_block
        self.res_projection = None
        self.residual_type = residual_type
        if self.residual_type == 'add':
            self.input_dim = self.block.input_dim
            self.output_dim = self.block.output_dim
            if self.block.input_dim != self.block.output_dim:
                if verbose:
                    print(f'Adding linear projection to residual connection to map {self.block.input_dim} to {self.block.output_dim} dimensions')
                self.res_projection = nn.Linear(self.block.input_dim, self.block.output_dim)
        elif self.residual_type == 'cat':
            self.input_dim = self.block.input_dim
            self.output_dim = self.block.input_dim+self.block.output_dim
        elif self.residual_type == 'no':
            self.input_dim = self.block.input_dim
            self.output_dim = self.block.output_dim
        else:
            raise ValueError(f'Unknown residual_type: "{residual_type}"')

    def forward(self, x, adj_t: SparseTensor, pos):
        output, adj_t, pos = self.block(x, adj_t, pos)

        if self.residual_type == 'add':
            if self.res_projection is not None:
                output = output + self.res_projection(x)
            else:
                output = output + x
        elif self.residual_type == 'cat':
            output = torch.concat((x, output), dim=-1)
        elif self.residual_type == 'no':
            pass

        return output, adj_t, pos

### from GNNModels ##########################################################################################################################################

class BaseGNN(nn.Module):
    """
    Superclass of all other GNN classes
    """
    def __init__(self, linear_dropout=0.0, act_fn='relu', agg='add',
                 pool='add', residual_type='add', pooling_dim=-1, nb_layer=0, verbose = False, **kwargs):
        """
        :param linear_dropout: probability of zeroing in dropout layers
        :param act_fn: name of activation function: 'relu','leakyrelu','gelu' 
        :param agg: aggregation EdgeConv: 'add', 'mean', 'max'
        :param pool: aggregation for pooling (graph to embeding): 'add', 'mean', 'max', 'att'
            'att' for attention mecanism cf. BuildingBlocks
        :param residual_type: residual connection type : 'add', 'cat', 'no'
        :param pooling_dim: number of feature for each node at polling input
        :param nb_layer: in case of 'att' pooling, use a network to compute values and keys from caracteristique generated by gnn 
        """
        super().__init__()
        if verbose:
            print(f'Ignoring the following keyword arguments when creating {self.__class__.__name__} model: {kwargs}')
        if linear_dropout > 0.0:
            self.linear_dropout = nn.Dropout(linear_dropout)
        else:
            self.linear_dropout = None
        self.pooling_dim = pooling_dim
        self.residual_type = residual_type
        self.agg = agg
        self.nb_layer = nb_layer
        self.verbose = verbose

        # identify activation function
        self.act_fn_name = act_fn.lower()
        if self.act_fn_name == 'relu':
            self.act_fn = nn.ReLU()
        elif self.act_fn_name == 'leakyrelu':
            self.act_fn = nn.LeakyReLU()
        elif self.act_fn_name == 'gelu':
            self.act_fn = nn.GELU()
        else:
            raise ValueError(f'Unknown activation function "{self.act_fn_name}"')

        # identify pooling method
        pool = pool.lower()
        self.pool_name = pool
        if self.pool_name == 'add':
            self.pool = geom_nn.global_add_pool
        elif self.pool_name == 'max':
            self.pool = geom_nn.global_max_pool
        elif pool == 'mean':
            self.pool = geom_nn.global_mean_pool
        elif self._set_att_pooling():
            pass
        else:
            raise ValueError(f'Unknown pooling "{self.pool_name}"')

    def _set_att_pooling(self):
        """
        Create an attention pooling layer and save it in an attribute
        """
        if self.pool_name == 'att':
            if self.pooling_dim == -1:
                raise ValueError(f'pre_pooling_dim has to be specified to use "{self.pool_name}"-pooling')
            if self.nb_layer>0:  
                self.pool = geom_nn.GlobalAttention(
                    gate_nn=MLP([self.pooling_dim for _ in range(self.nb_layer)]+[1],activation=self.act_fn, last_activation=False, norm=None,drop=self.linear_dropout),
                    nn=MLP([self.pooling_dim for _ in range(self.nb_layer)]+[self.pooling_dim*2],activation=self.act_fn, last_activation=False, norm=None,drop=self.linear_dropout)
                )
            else:
                self.pool = geom_nn.GlobalAttention(
                    gate_nn=nn.Linear(self.pooling_dim, 1)
                )
        else:
            return False
        return True

    def get_res_block(self, conv_layer_type, conv_lin_layers, residual_type=None):
        """
        Create a residual block
        """
        conv_block = self.get_conv_block(conv_layer_type, conv_lin_layers)
        if residual_type is None:
            residual_type = self.residual_type
        return ResBlock(conv_block, residual_type, self.verbose)

    def get_conv_block(self, conv_layer_type, conv_lin_layers):
        """
        Create a graph convolution block
        """
        return ConvBlock(conv_layer_type, self.agg, self.act_fn, conv_lin_layers,
                         self.get_linear_block)

    def get_linear_block(self, lin_layers, final_layer=False):
        """
        Create a linear block
        """
        return MLP(lin_layers,activation=self.act_fn, last_activation=not final_layer, norm=None, drop=self.linear_dropout) 
            # replace LinearBlock by MLP for uniformity in models, 
            # equivalent but batch_norm_momentum not implemented, not used in Local GNN

### from GraphEncoder #######################################################################################################################################

class GraphEncoder(BaseGNN):
    """
    The local GNN used to encode superpixel shapes
    """
    def __init__(self, num_features, latent_dim,  hidden_dim=0, pooling_dim=4, conv_layer_type='gcnconv', num_linear_layers_mult=1,
                 num_res_blocks=1, residual_type='no', pool='add', nb_layer=False, **kwargs):
        """
        :param num_features: number of features for each node
        :param latent_dim: size of embeding space
        :param hidden_dim: size of hidden layers
            if 0 replace by max(latent_dim,pooling_dim)
        :param pooling_dim: size of pooling latent space (querry and key dim = pooling_dim, value dim = pooling_dim*2 if nb_layer > 0)
        :param conv_layer_type: name of graph convolution as define in torch_geometric : 'edgeconv','dynamicedgeconv','ginconv','gcnconv','gatconv','sageconv','genconv','transformerconv','gatv2conv'
        :param num_linear_layers_mult: number of linear layer in each mlp
        :param num_res_blocks: number of res_block (cf. BuildingBlocks)
        :param residual_type: type of residual connection : 'add', 'cat', 'no'
        :param pool: aggregation for pooling (graph to embeding): 'add', 'mean', 'max', 'att'
            'att' for attention mecanism cf. BuildingBlocks
        :param nb_layer: in case of 'att' pooling, use a network to compute values and keys
        """
        self.latent_dim = latent_dim
        self.conv_layer_type = conv_layer_type
        if hidden_dim == 0:
            self.hidden_dim = max(self.latent_dim,pooling_dim)
        else:
            self.hidden_dim = hidden_dim

        super().__init__(residual_type=residual_type, pooling_dim=pooling_dim, pool=pool, nb_layer=nb_layer, **kwargs)

        res_block_list = []
        input_dim = num_features
        output_dim = hidden_dim
        for i in range(num_res_blocks):
            res_llayers = [input_dim] + [self.hidden_dim for _ in range(num_linear_layers_mult-1)] + [output_dim]
            res_block = self.get_res_block(conv_layer_type, res_llayers)
            res_block_list.append(res_block)
            input_dim = self.hidden_dim
            if i == num_res_blocks-2: # only for last res block
                output_dim = pooling_dim
        self.res_blocks = nn.ModuleList(res_block_list)
        
        if nb_layer>0 and pool == 'att':
            input_dim = pooling_dim*2
        else:
            input_dim = pooling_dim
        mlp_llayers = [input_dim] + [hidden_dim for _ in range(num_linear_layers_mult-1)] + [self.latent_dim]
        self.mlp = self.get_linear_block(mlp_llayers, final_layer=True)

    def forward(self, x, adj_t: SparseTensor, batch):
        pos = x

        for res_block in self.res_blocks:
            x, adj_t, pos = res_block(x=x, adj_t=adj_t, pos=pos)
        x = self.pool(x, batch)
        z = self.mlp(x)
        return z

class Shape_Interface_Encoders(nn.Module):
    """
    create all the encoders needed for SPT
    """
    def __init__(self, List_Param_Encoders, share_level, share_type, hyper_share_level, hyper_share_type, nb_levels, nano, use_shape_interface):
        """
        List_Param_Encoders : list of dictionary
            list of parameters of all networks
        # parameters sharing
		share_level: bool
			whether to share weights between levels
		share_type: bool
			whether to share weights between shape and interface encoders
		# hyperparameters sharing
		hyper_share_level: bool
			whether to use the same hyperparameters between levels
                relevant only if share_level = False
		hyper_share_type: bool
			whether to use the same hyperparameters between shape and interface encoders
                relevant only if share_level = False
		nb_levels: int
			number of down stage
        nano: bool
            is the network in nano mode
        use_shape_interface: bool
            whether to use shape encoding
            if False disable all in the class, and forward calls are locked
        """
        super().__init__()
        if use_shape_interface:
            assert max((nb_levels * (not share_level) * (not hyper_share_level)),1) * max((2 * (not share_type) * (not hyper_share_type)),1) == len(List_Param_Encoders), \
                "number of Graph Encoder incorrect"
            
            List_Encoders = []
            for e in range(max((nb_levels * (not share_level)),1) * max((2 * (not share_type)),1)):
                List_Encoders.append(GraphEncoder(**List_Param_Encoders[int((e - ((e%2)*(hyper_share_type)) - (2 * ((e//2) * (hyper_share_level))))/(1+hyper_share_type))]))
            
            self.List_Encoders = nn.ModuleList(List_Encoders)
            self.share_level = share_level
            self.share_type = share_type
            self.hyper_share_level = hyper_share_level
            self.hyper_share_type = hyper_share_type
            self.nb_levels = nb_levels
            self.nano = nano
            self.use_shape_interface = use_shape_interface

            self.feature_fusion = CatFusion()
            
    def forward(self,nag):
        
        
        assert self.use_shape_interface, "the shape encoding is disabled"
        assert isinstance(nag,NAG), "can only compute shape encoding for a NAG with computed partition"
        
        for l in range(nag.num_levels-((not self.nano)*1)):
            
            data = nag[l+((not self.nano)*1)]
            
            assert hasattr(data,"shapes"), "missing shape information for shape encoding"
            assert hasattr(data,"interfaces"), "missing shape information for interface encoding"
            
            if self.share_level:
                self.forward_GraphEncoders(data,self.List_Encoders)
            else:
                self.forward_GraphEncoders(data,self.List_Encoders[l*((not self.share_type)+1):l*((not self.share_type)+1)+(not self.share_type)+1])

    def forward_GraphEncoders(self,data,Encoders):
        """
        run GraphEncoders for one level
        """
        data.x = self.feature_fusion(Encoders[0](data.shapes.pos,data.shapes.edge_index,data.shapes.batch_node),data.x)
        # duplique the result for both direction en self conections
        embeddings = Encoders[(not self.share_type)*1](data.interfaces.pos,data.interfaces.edge_index,data.interfaces.batch_node)
        data.edge_attr = self.feature_fusion(torch.cat((torch.cat((embeddings,embeddings),dim=0),torch.zeros([data.num_nodes,embeddings.shape[1]],device=embeddings.device)),dim=0),data.edge_attr)



