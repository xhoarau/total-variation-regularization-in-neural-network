from torch_geometric.nn.pool.consecutive import consecutive_cluster
from src.utils.sparse import indices_to_pointers
from src.utils.tensor import arange_interleave


__all__ = ['edge_key_equivalent','edge_index_to_uid', 'edge_wise_pixels']

def edge_key_equivalent(key):
    """ from an edge_key give the node equivalent 
    (to find the values in normalisation dict)
    """
    if key == 'e_size':
        return 'node_size'
    if key in ['e_log_size','e_perimeter','e_log_perimeter']:
        return key[2:]
    return key

def edge_index_to_uid(edge_index):
    """Compute consecutive unique identifiers for the edges. This may be
    needed for scatter operations.
    """
    assert edge_index.dim() == 2
    assert edge_index.shape[0] == 2
    source = edge_index[0]
    target = edge_index[1]
    edge_uid = source * (max(source.max(), target.max()) + 1) + target
    edge_uid = consecutive_cluster(edge_uid)[0]
    return edge_uid


def edge_wise_pixels(pixels, index, edge_index):
    """Given a graph of pixel segments, compute the concatenation of
    pixels belonging to either source or target segments for each edge
    of the segment graph. This operation arises when dealing with
    pairwise relationships between pixel segments.

    Warning: the output tensors might be memory-intensive

    :param pixels: (N, D) tensor
        Pixels
    :param index: (N) LongTensor
        Segment index, for each pixel
    :param edge_index: (2, E) LongTensor
        Edges of the segment graph
    """
    assert pixels.dim() == 2
    assert index.dim() == 1
    assert pixels.shape[0] == index.shape[0]
    assert edge_index.dim() == 2
    assert edge_index.shape[0] == 2
    assert edge_index.max() <= index.max()

    # We define the segments in the first row of edge_index as 'source'
    # segments, while the elements of the second row are 'target'
    # segments. The corresponding variables are prepended with 's_' and
    # 't_' for clarity
    s_idx = edge_index[0]
    t_idx = edge_index[1]

    # Compute consecutive unique identifiers for the edges
    uid = edge_index_to_uid(edge_index)

    # Compute the pointers and ordering to express the segments and the
    # pixels they hold in CSR format
    pointers, order = indices_to_pointers(index)

    # Compute the size of each segment
    segment_size = index.bincount()

    # Expand the edge variables to pixel-edge values. That is, the
    # concatenation of all the source -or target- pixels for each edge.
    # The corresponding variables are prepended with 'S_' and 'T_' for
    # clarity
    def expand(source=True):
        x_idx = s_idx if source else t_idx
        size = segment_size[x_idx]
        start = pointers[:-1][x_idx]
        X_pixels_idx = order[arange_interleave(size, start=start)]
        X_pixels = pixels[X_pixels_idx]
        X_uid = uid.repeat_interleave(size, dim=0)
        return X_pixels, X_pixels_idx, X_uid

    S_pixels, S_pixels_idx, S_uid = expand(source=True)
    T_pixels, T_pixels_idx, T_uid = expand(source=False)

    return (S_pixels, S_pixels_idx, S_uid), (T_pixels, T_pixels_idx, T_uid)
