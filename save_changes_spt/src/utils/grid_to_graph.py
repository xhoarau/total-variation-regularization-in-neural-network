import numpy as np

def grid_to_graph_unique_dist(dist,connect,grid):
    """Create the edges for one distance cf. grid_to_graph
    
    param:
        diss: int 
            distance between pixels : norm_inf(2^dist)
        connect: int
            density of edges
            if -1 : takes only the edges in the princial directions of the grid
            else : takes all egdes with norm inf = dist and 
                with relative coordinate containing powers of 2 between (2^dist/2^connect) and (2^dist)
        grid: numpy array (dimention de la grille)

    return
        edge: numpy array 
            set of pairs of pixel
        connectivity: numpy array 
            euclidean lenth of the edge
    """
    dist_val = np.power(2,dist) # value of the distance (norm inf)
    dim = len(grid) # number of dimention of the grid
    assert dist>=0,"dist is strictly positive"
    assert connect<=dist and connect>=-1,"connect is between -1 and dist"
    if connect == -1:
        ensemble = np.diag(np.full(dim,dist_val)) # relative coordinate in all pricipal direction
    else:
        list_element = np.arange(-dist_val,dist_val+0.1,dist_val/np.power(2,connect),dtype=int) # values of the relative coordinate
        ensemble = np.array(np.meshgrid(*[list_element for i in range(dim)])).reshape([dim,np.power(len(list_element),dim)]).transpose() # all relative coordinate
        ensemble = ensemble[ensemble[np.arange(ensemble.shape[0]),np.argmin(ensemble==0,axis=1)]>0] # keep only edges if the first non null valu is positive (non directional edges) 
    grid_co = np.array(np.meshgrid(*[np.arange(grid[i]) for i in range(dim)])).reshape([dim,grid.prod()]).transpose() # all coordinate in the grid
    end_co = np.repeat(grid_co,ensemble.shape[0],axis=0)+np.tile(ensemble,(grid.prod(),1)) # all edges ending
    edge = np.stack((np.repeat(grid_co,ensemble.shape[0],axis=0),end_co),axis=1) # all edges as pixel to pixel
    edge = edge[np.all(np.logical_and(edge<grid,edge>=0),axis=(1,2))] # remove edges that goes outside of the grid
    connectivity = np.linalg.norm(edge[:,0,:]-edge[:,1,:],2,axis=1) # euclidean lenth of the edges
    edge = np.sum(np.multiply(edge,np.flip(np.cumprod(np.insert(grid[1:],0,1)))),axis=2) # code edges in numero of pixels
    return edge,connectivity

def grid_to_graph(dist,connect,grid):
    """Create the edges linking pixels in a grid
    param:
        dist: numpy array 
            list of lenght of the edges
                the lenght is calculated as norm_if(2^dist) for the consruction
        connect: numpy array 
            if one value : maximum density
            if multiple value : density for each distance (len(dist) = len(connect))
        grid: numpy array
            dimention of the grid
        
    return
        edge: numpy array
            set of paires of pixels in the grid
        connectivities: numpy array
           euclidean lenth of the edges 
  """
    assert len(dist.shape) == 1,"dist is a 1D numpy array"
    assert len(connect.shape)==1 and (connect.shape[0]==1 or connect.shape[0]==dist.shape[0]),"connect is a 1D numpy array with same size as dist or of size 1"
    assert len(grid.shape) == 1,"grid is a 1D numpy array"
    connect = np.minimum(connect,dist)
    edges,connectivities = grid_to_graph_unique_dist(dist[0],connect[0],grid)
    for d in range(1,dist.shape[0]):
        n_edge,n_connectivitie = grid_to_graph_unique_dist(dist[d],connect[d],grid)
        edges = np.concatenate([edges,n_edge])
        connectivities = np.concatenate([connectivities,n_connectivitie])
    return edges.astype(np.int32).transpose(),connectivities
