import h5py
import torch
from time import time
from src.data.csr import CSRData, CSRBatch
from src.utils import has_duplicates, tensor_idx, \
    save_tensor, load_tensor
from torch_geometric.nn.pool.consecutive import consecutive_cluster


__all__ = ['Cluster', 'ClusterBatch']


class Cluster(CSRData):
    """Child class of CSRData to simplify some common operations
    dedicated to cluster-pixel indexing.
    """

    def __init__(self, pointers, pixels, dense=False, **kwargs):
        super().__init__(
            pointers, pixels, dense=dense, is_index_value=[True])

    @staticmethod
    def get_batch_type():
        """Required by CSRBatch.from_csr_list."""
        return ClusterBatch

    @property
    def pixels(self):
        return self.values[0]

    @pixels.setter
    def pixels(self, pixels):
        assert pixels.device == self.device, \
            f"Points is on {pixels.device} while self is on {self.device}"
        self.values[0] = pixels
        # if src.is_debug_enabled():
        #     self.debug()

    @property
    def num_clusters(self):
        return self.num_groups

    @property
    def num_pixels(self):
        return self.num_items

    def to_super_index(self):
        """Return a 1D tensor of indices converting the CSR-formatted
        clustering structure in 'self' into the 'super_index' format.
        """
        device = self.device
        out = torch.empty((self.num_items,), dtype=torch.long, device=device)
        cluster_idx = torch.arange(self.num_groups, device=device)
        out[self.pixels] = cluster_idx.repeat_interleave(self.size)
        return out

    def select(self, idx, update_sub=True):
        """Returns a new Cluster with updated clusters and pixels, which
        indexes `self` using entries in `idx`. Supports torch and numpy
        fancy indexing. `idx` must NOT contain duplicate entries, as
        this would cause ambiguities in super- and sub- indices.

        NB: if `self` belongs to a NAG, calling this function in
        isolation may break compatibility with pixel and cluster indices
        in the other hierarchy levels. If consistency matters, prefer
        using NAG indexing instead.

        :parameter
        idx: int or 1D torch.LongTensor or numpy.NDArray
            Cluster indices to select from 'self'. Must NOT contain
            duplicates
        update_sub: bool
            If True, the pixel (ie subpixel) indices will also be
            updated to maintain dense indices. The output will then
            contain '(idx_sub, sub_super)' which can help apply these
            changes to maintain consistency with lower hierarchy levels
            of a NAG.

        :returns cluster, (idx_sub, sub_super)
        clusters: Cluster
            indexed cluster
        idx_sub: torch.LongTensor
            to be used with 'Data.select()' on the sub-level
        sub_super: torch.LongTensor
            to replace 'Data.super_index' on the sub-level
        """
        # Normal CSRData indexing, creates a new object in memory
        cluster = self[idx]

        if not update_sub:
            return cluster, (None, None)

        # Convert subpixel indices, in case some subpixels have
        # disappeared. 'idx_sub' is intended to be used with
        # Data.select() on the level below
        new_cluster_pixels, perm = consecutive_cluster(cluster.pixels)
        idx_sub = cluster.pixels[perm]
        cluster.pixels = new_cluster_pixels

        # Selecting the subpixels with 'idx_sub' will not be
        # enough to maintain consistency with the current pixels. We
        # also need to update the sub-level's 'Data.super_index', which
        # can be computed from 'cluster'
        sub_super = cluster.to_super_index()

        return cluster, (idx_sub, sub_super)

    def debug(self):
        super().debug()
        assert not has_duplicates(self.pixels)

    def __repr__(self):
        info = [
            f"{key}={getattr(self, key)}"
            for key in ['num_clusters', 'num_pixels', 'device']]
        return f"{self.__class__.__name__}({', '.join(info)})"

    def save(self, f, fp_dtype=torch.float):
        """Save Cluster to HDF5 file.

        :param f: h5 file path of h5py.File or h5py.Group
        :param fp_dtype: torch dtype
            Data type to which floating point tensors will be cast
            before saving
        :return:
        """
        if not isinstance(f, (h5py.File, h5py.Group)):
            with h5py.File(f, 'w') as file:
                self.save(file, fp_dtype=fp_dtype)
            return

        save_tensor(self.pointers, f, 'pointers', fp_dtype=fp_dtype)
        save_tensor(self.pixels, f, 'pixels', fp_dtype=fp_dtype)

    @staticmethod
    def load(f, idx=None, update_sub=True, verbose=False): # some pretretement made before update, remove points #######################################################
        """Load Cluster from an HDF5 file. See `Cluster.save` for
        writing such file. Options allow reading only part of the
        pixels.

        This reproduces the behavior of Cluster.select but without
        reading the full pointer data from disk.

        :param f: h5 file path of h5py.File or h5py.Group
        :param idx: int, list, numpy.ndarray, torch.Tensor
            Used to select pixels when reading. Supports fancy indexing
        :param update_sub: bool
            If True, the pixel (ie subpixel) indices will also be
            updated to maintain dense indices. The output will then
            contain '(idx_sub, sub_super)' which can help apply these
            changes to maintain consistency with lower hierarchy levels
            of a NAG.
        :param verbose: bool
        :return: cluster, (idx_sub, sub_super)
        """
        if not isinstance(f, (h5py.File, h5py.Group)):
            with h5py.File(f, 'r') as file:
                out = Cluster.load(
                    file, idx=idx, update_sub=update_sub, verbose=verbose)
            return out

        KEYS = ['pointers', 'pixels']

        assert all(k in f.keys() for k in KEYS)

        start = time()
        idx = tensor_idx(idx)
        if verbose:
            print(f'Cluster.load tensor_idx         : {time() - start:0.5f}s')

        if idx is None or idx.shape[0] == 0:
            start = time()
            pointers = load_tensor(f['pointers'])
            pixels = load_tensor(f['pixels'])
            if verbose:
                print(f'Cluster.load read all           : {time() - start:0.5f}s')
            start = time()
            out = Cluster(pointers, pixels), (None, None)
            if verbose:
                print(f'Cluster.load init               : {time() - start:0.5f}s')
            return out

        # Read only pointers start and end indices based on idx
        start = time()
        ptr_start = load_tensor(f['pointers'], idx=idx)
        ptr_end = load_tensor(f['pointers'], idx=idx + 1)
        if verbose:
            print(f'Cluster.load read ptr       : {time() - start:0.5f}s')

        # Create the new pointers
        start = time()
        pointers = torch.cat([
            torch.zeros(1, dtype=ptr_start.dtype),
            torch.cumsum(ptr_end - ptr_start, 0)])
        if verbose:
            print(f'Cluster.load new pointers   : {time() - start:0.5f}s')

        # Create the indexing tensor to select and order values.
        # Simply, we could have used a list of slices, but we want to
        # avoid for loops and list concatenations to benefit from torch
        # capabilities.
        start = time()
        sizes = pointers[1:] - pointers[:-1]
        val_idx = torch.arange(pointers[-1])
        val_idx -= torch.arange(pointers[-1] + 1)[
            pointers[:-1]].repeat_interleave(sizes)
        val_idx += ptr_start.repeat_interleave(sizes)
        if verbose:
            print(f'Cluster.load val_idx        : {time() - start:0.5f}s')

        # Read the pixels, now we have computed the val_idx
        start = time()
        pixels = load_tensor(f['pixels'], idx=val_idx)
        if verbose:
            print(f'Cluster.load read pixels    : {time() - start:0.5f}s')

        # Build the Cluster object
        start = time()
        cluster = Cluster(pointers, pixels)
        if verbose:
            print(f'Cluster.load init           : {time() - start:0.5f}s')

        if not update_sub:
            return cluster, (None, None)

        # Convert subpixel indices, in case some subpixels have
        # disappeared. 'idx_sub' is intended to be used with
        # Data.select() on the level below
        start = time()
        new_cluster_pixels, perm = consecutive_cluster(cluster.pixels)
        idx_sub = cluster.pixels[perm]
        cluster.pixels = new_cluster_pixels
        if verbose:
            print(f'Cluster.load update_sub     : {time() - start:0.5f}s')

        # Selecting the subpixels with 'idx_sub' will not be
        # enough to maintain consistency with the current pixels. We
        # also need to update the sublevel's 'Data.super_index', which
        # can be computed from 'cluster'
        start = time()
        sub_super = cluster.to_super_index()
        if verbose:
            print(f'Cluster.load super_index    : {time() - start:0.5f}s')

        return cluster, (idx_sub, sub_super)


class ClusterBatch(Cluster, CSRBatch):
    """Wrapper for Cluster batching."""
    __csr_type__ = Cluster
