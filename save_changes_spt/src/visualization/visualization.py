import torch
import torch_geometric
import numpy as np
import os.path as osp
import plotly.graph_objects as go
from src.data import Data, NAG, Cluster
from src.utils import fast_randperm, to_trimmed, grid_to_graph
from torch_scatter import scatter_mean
from src.utils.color import *
import matplotlib.pyplot as plt
import networkx as nx

def figure_html(fig):
    # Save plotly figure to temp HTML
    fig.write_html(
        '/tmp/fig.html',
        config={'displayModeBar': False},
        include_plotlyjs='cdn',
        full_html=False)

    # Read the HTML
    with open("/tmp/fig.html", "r") as f:
        fig_html = f.read()

    # Center the figure div for cleaner display
    fig_html = fig_html.replace(
        'class="plotly-graph-div" style="',
        'class="plotly-graph-div" style="margin:0 auto;')

    return fig_html


def plt_figure(n_row, n_col, num = None, Final_version=False):
    """ parametrize the plt figure

    :param n_row: maximum number of subplot per column
    :param n_column: maximum number of subplot per row
    :param num: identifier of figure if reuse an existing figure 
        if None create a new figure, 
        if string also title of figure
    :Final_version: to set the precision on the figure
        to achieve lighter image uring experiment
    """
    # square of 5/5 inches for each subplot
    figsize = [5*n_col,5*n_row]

    if Final_version:
        dpi = 600
    else:
        dpi = 300

    fig = plt.figure(num = num, figsize = figsize, dpi = dpi, layout = 'constrained')

    return fig

def new_subplot(coord, fig, title = None, axis= False):
    """ create a new subplot in fig at the given position

    :param coord: position of the subplot
    :param fig: figure in witch create the sub plot
    :param title: optional title of the subplot
    :param axis: keep or remove the axis visible
    """
    
    n_col,n_row = fig.get_size_inches()/5
    n_col = int(n_col)
    n_row = int(n_row)

    assert coord <= [n_row,n_col], "coordinate outside of the figure"

    index = (coord[1]-1)*n_col + coord[0]

    ax = fig.add_subplot(n_row, n_col, index, aspect='equal')
    if not title is None :
        ax.set(title=title)
    if not axis:
        ax.axis('off')
    
    return ax

def image_SuperPixel_visualize(image,super_index):
    """ create a big image to visualise super pixel
    compute the mean rgb for each super pixel and drow blacl lines to seperate them

    :param image: the origin image (rgb [3,w,h] or gray [1,w,h])
    :param super_index: index of super pixel for each pixel
    """

    big_image_super_index = torch.nn.functional.interpolate(super_index.reshape(image.shape[-2:]).unsqueeze(0).unsqueeze(0).float(),scale_factor=5).squeeze().long()
    big_super_index = big_image_super_index.flatten()
    
    edges,connect = grid_to_graph(np.array([0]),np.array([0]),np.array(big_image_super_index.shape))
    
    index_bord = edges[0,big_super_index[edges[0]]!=big_super_index[edges[1]]]

    big_image = scatter_mean(image.flatten(-2,-1),super_index)[:,big_super_index]
    big_image[:,index_bord] = torch.tensor([0.,0.,0.],dtype=image.dtype,device=image.device).unsqueeze(1)
    big_image = big_image.reshape([*image.shape[:-2],*big_image_super_index.shape])

    return big_image

def label_SuperPixel_visualize(y,shape,super_index):
    """ create a big image to visualise class of superpixel
    compute the majority label in each super pixel and draw black lines to seperate them

    :param y: the low level label [num_nodes,num_classes]
    :param super_index: index of super pixel for each pixel
    """

    big_image_super_index = torch.nn.functional.interpolate(super_index.reshape(shape).unsqueeze(0).unsqueeze(0).float(),scale_factor=5).squeeze().long()
    big_super_index = big_image_super_index.flatten()
    
    edges,connect = grid_to_graph(np.array([0]),np.array([0]),np.array(big_image_super_index.shape))
    
    index_bord = edges[0,big_super_index[edges[0]]!=big_super_index[edges[1]]]

    big_label = y.argmax(dim=1)[big_super_index]
    big_label[index_bord] = 0
    big_label = big_label.reshape(big_image_super_index.shape)

    return big_label

def next_subplot_feature(num_row,num_col):
    """ compute the next subplot coordinate """

    if num_col >= 3:
        return (num_row+1, 1)
    return (num_row, num_col+1)

def visualize(input, keys = None, feature_keys = None, title=None, class_color = None, Final_version=False, fig=None): 
    """ create the figure visualising a DATA or a NAG
    
    :param input: Data or NAG object
    :param keys: data to show
        accept in : ["image","label","SegError","Pred"] or ["all"]
        "SegError" show the error made by the oracle (with the level 1 partition)
    :param feature_keys: handcrafted feature to show
        handfeatures are in Nag[0].$keys$ or Data.$keys$
    :param title:
    :param class_color: color of each class [nclass,3]
    :param Final_version:to set the precision on the figure (dot per inches, dpi)
        to achieve lighter image during experiment 
    """

    if title is None:
        title = "SPT Image"
    
    assert isinstance(input, (Data, NAG))
    
    # We work on copies of the input data, to allow modified in this
    # scope
    input = input.clone().detach().cpu()

    # If the input is a simple Data object, we convert it to a NAG
    input = NAG([input]) if isinstance(input, Data) else input

    # If the last level of the NAG has super_index, we manually
    # construct an additional Data level and append it to the NAG
    if input[input.num_levels - 1].is_sub:
        data_last = input[input.num_levels - 1]
        sub = Cluster(
            data_last.super_index, torch.arange(data_last.num_nodes),
            dense=True)
        pos = scatter_mean(data_last.pos, data_last.super_index, dim=0)
        input = NAG(input.to_list() + [Data(pos=pos, sub=sub)])
    
    n_row = input.num_levels
    
    # visualise pricipal data (rgb, labels, prediction)
    if not keys is None:
        if isinstance(keys, str):
            keys = [keys]

        if keys == ["all"]:
            n_col = 3
        else:
            n_col = len(keys)
            if "SegError" in keys and "Pred" in keys: # both on the same column
                n_col = n_col-1
        
        if fig is None:
            fig = plt_figure(n_row, n_col, num="Nag Visualisation", Final_version = Final_version)
        
        num_col = 1

        if keys == ["all"] or "image" in keys:
            assert hasattr(input[0],"image"), "no image in the lowest level"
            num_row = 1
            
            # create a subplot
            ax = new_subplot([num_col,num_row],fig,title="IMAGES")
            
            # add the image to the current subplot
            image = input[0].image
            image = (image-torch.min(image))
            if torch.max(image)-torch.min(image) != 0:
                image = image/(torch.max(image)-torch.min(image))
            if len(image.shape) == 2:
                image = image.unsqueeze(0)
            img = np.array(image.permute(1,2,0)) # to normalise data
            plt.imshow(img.astype(np.float32()),cmap="gray") # cmap ignored for rgb data
            
            num_row = num_row + 1
            
            for l in range(1,n_row):
                super_index = input.get_super_index(l)

                # create a subplot
                ax = new_subplot([num_col,num_row],fig,title="nb super pixels : "+str(torch.max(super_index).floor().item()+1))
                
                # add the image to the current subplot
                img = np.array(image_SuperPixel_visualize(image,super_index).permute(1,2,0))
                plt.imshow(img.astype(np.float32()),cmap="gray")
                
                num_row = num_row + 1
            num_col = num_col + 1

        if keys == ["all"] or "label" in keys:
            assert not class_color is None, "to visualise labels, class_color is needed"
            assert hasattr(input[0],"label"), "no label in the lowest level"
            
            num_row = 1
            
            # create a subplot
            ax = new_subplot([num_col,num_row],fig,title="Label")
            
            # add the label to the current subplot
            label = input[0].label

            label[label>=class_color.shape[0]] = class_color.shape[0]
            
            img = np.array(class_color[label])/255.
            plt.imshow(img.astype(np.float32()))
            
            num_row = num_row + 1
            
            for l in range(1,n_row):
                super_index = input.get_super_index(l)
                y = input[l].y
                
                # create a subplot
                ax = new_subplot([num_col,num_row],fig,title="nb super pixels : "+str(torch.max(super_index).floor().item()+1))
                
                # add the label to the current subplot
                img = np.array(class_color[label_SuperPixel_visualize(y,label.shape,super_index)])/255.
                plt.imshow(img.astype(np.float32()))
                
                num_row = num_row + 1
            num_col = num_col + 1
    
        num_row = 1 # SegError and Pred on the same column
        if keys == ["all"] or "SegError" in keys:
            assert not class_color is None, "to visualise labels, class_color is needed"
            assert n_row >= 2, "to visualise the Over Segmentation Error, at least  level are needed"
            assert hasattr(input[0],"label"), "no label in the lowest level"
            
            # create a subplot
            ax = new_subplot([num_col,num_row],fig,title="SegError")
            
            label = input[0].label
            y = input[1].y
            label[label>=class_color.shape[0]] = class_color.shape[0]
            super_index = input.get_super_index(1)
            super_label = y.argmax(dim=1)[super_index.reshape(label.shape)]

            error_color= np.array([[0.,1,0], # correct prediction
                                [1,0,0], # error
                                [1,1,1]]) # unlabeled

            error = (label != super_label)*1
            error[label==class_color.shape[0]] = 2

            img = np.array(error_color[error])
            plt.imshow(img.astype(np.float32()))
            num_row = num_row + 1

        if keys == ["all"] or "Pred" in keys:
            assert not class_color is None, "to visualise predictions, class_color is needed"
            assert hasattr(input[0],"pred"), "no pred in the lowest level"
            assert hasattr(input[0],"image"), "need image size to show prediction"
            
            # create a subplot
            ax = new_subplot([num_col,num_row],fig,title="Pred")

            pred = input[0].pred
            pred = pred.argmax(1).numpy() if pred.dim() == 2 else pred.numpy() # if pred is an histogram or raw prediction
            pred = pred.reshape(input[0].image.shape[1:])

            pred[pred>=class_color.shape[0]] = class_color.shape[0]
            
            img = np.array(class_color[pred])/255.
            plt.imshow(img.astype(np.float32()))

            num_row = num_row + 1

            if hasattr(input[0],"label"): # pred error only if annotation to compare to
                # create a subplot
                ax = new_subplot([num_col,num_row],fig,title="PredError")
                
                error_color= np.array([[0.,1,0], # correct prediction
                                    [1,0,0], # error
                                    [1,1,1]]) # unlabeled
                
                label = input[0].label.numpy()
                label[label>=class_color.shape[0]] = class_color.shape[0]
                error = (label!=pred)*1
                error[label==class_color.shape[0]] = 2
                img = np.array(error_color[error])
                plt.imshow(img.astype(np.float32()))
                num_row = num_row + 1
    else:
        fig = None
    
    # visualise other features
    if not feature_keys is None:

        if isinstance(feature_keys, str):
            feature_keys = [feature_keys]

        n_col = min(3,len(feature_keys))
        n_row = int(np.floor(len(feature_keys)/3))
        
        fig_feature = plt_figure(n_row, n_col, num="Features Visualisation", Final_version = Final_version)
        num_col = 1
        num_row = 1

        for key in feature_keys:
            if getattr(input[0], key, None) is not None:
                colors = feats_to_rgb(input[0][key], normalize=True).reshape(input[0].image.shape)
                ax = new_subplot([num_col,num_row],fig,title=key)
                img = np.array(colors)
                plt.imshow(img.astype(np.float32()))
                num_row,num_col = next_subplot_feature(num_row,num_col)
    else:
        fig_feature = None

    return fig,fig_feature

                

def show(input, path=None, file_name=None, **kwargs): ### TODO, adapt to 2D data ### 
    """Interactive data visualization.

    :param input: Data or NAG object
    :param path: path to save the visualization into a sharable HTML
    :param file_name: figure title # if not in path
    :param no_output: set to True if you want to return the 3D and 2D
      Plotly figure objects
    :param kwargs: parameter of the visualisation
    :return:
    """

    # Sanitize title and path
    if file_name is None:
        file_name = "SPT Image"
    if path is not None:
        if osp.isdir(path):
            path = osp.join(path, f"{file_name}.svg")
            path_feature = osp.join(path, f"{file_name}_feature.svg")
        else:
            path = osp.splitext(path)[0] + '.svg' # make sure the extention is correct
            path_feature = osp.splitext(path)[0] + '_feature.svg'

    # Draw a figure for 3D data visualization
    out_principal, out_feature = visualize(input, **kwargs)
    if path is None:
        plt.show()
    else:
        if not out_principal is None:
            fig = plt.figure(out_principal)
            plt.savefig(path)
            plt.close(fig)
        if not out_feature is None:
            fig = plt.figure(out_feature)
            plt.savefig(path_feature)
            plt.close(fig)

def graph_visualization(nag_path,path,class_color,Final_version=False):
    """ graph visualisation (only to put on presentations)

    :param nag_path: path to preprocessed nag
    :param path: path to save resulting image
    """

    nag = NAG.load(nag_path)

    n_row = nag.num_levels
    n_col = 3

    fig = plt_figure(n_row, n_col, num="Graph Visualisation", Final_version = Final_version)

    visualize(nag,keys=["image","label","SegError"],title="graph_visualisation",class_color=class_color,fig=fig)

    num_col = 3
    num_row=2
    for i in range(1,n_row):
        g = torch_geometric.utils.to_networkx(nag[i], to_undirected=True)
        pos = {a.item(): b.numpy() for a,b in zip(torch.arange(g.number_of_nodes()).unsqueeze(1),nag[i].pos*torch.tensor([1,-1]))}
        ax = new_subplot([num_col,num_row],fig)
        colors = scatter_mean(nag[0].image.flatten(-2).squeeze(),nag.get_super_index(i)).transpose(1,0)
        nx.draw(g,node_size=5,pos=pos,node_color=colors,ax=ax,margins=0.)
        num_row = num_row + 1
    plt.savefig(path)
    plt.close()
    
