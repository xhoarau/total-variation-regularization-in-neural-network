measure_time: true
device_id: 0
task_name: train
optimized_metric: val/miou
tags:
- dev
train: true
test: false
compile: false
ckpt_path: null
seed: null
float32_matmul_precision: highest
datamodule:
  partition_hf:
  - rgb
  pixel_hf: []
  segment_base_hf:
  - node_size
  segment_mean_hf:
  - rgb
  segment_std_hf: []
  edge_hf:
  - interface_length
  - e_size
  v_edge_hf: []
  segment_hf: ${eval:'${datamodule.segment_base_hf}+["mean_" + x for x in ${datamodule.segment_mean_hf}]+["std_"
    + x for x in ${datamodule.segment_std_hf}]'}
  pixel_hf_preprocess: ${eval:'list(set( ${datamodule.partition_hf} + ${datamodule.pixel_hf}
    + ${datamodule.segment_mean_hf} + ${datamodule.segment_std_hf} )) if ${datamodule.lite_preprocessing}
    else None'}
  segment_base_hf_preprocess: ${eval:'list(set( ${datamodule.segment_base_hf} )) if
    ${datamodule.lite_preprocessing} else None'}
  segment_mean_hf_preprocess: ${eval:'${datamodule.segment_mean_hf} if ${datamodule.lite_preprocessing}
    else None'}
  segment_std_hf_preprocess: ${eval:'${datamodule.segment_std_hf} if ${datamodule.lite_preprocessing}
    else None'}
  num_hf_pixel: ${eval:'sum([ ${datamodule.feat_size}[k] for k in ${datamodule.pixel_hf}
    ])'}
  num_hf_segment: ${eval:'sum([ ${datamodule.feat_size}[k] for k in ${datamodule.segment_hf}
    ])'}
  num_hf_edge: ${eval:'sum([ ${datamodule.feat_size}[k] for k in ${datamodule.edge_hf}
    ])'}
  num_hf_v_edge: ${eval:'sum([ ${datamodule.feat_size}[k] for k in ${datamodule.v_edge_hf}
    ])'}
  pixel_save_keys: null
  pixel_no_save_keys:
  - edge_attr
  - node_size
  - grid_size
  segment_save_keys: null
  pixel_basic_load_keys:
  - pos
  - 'y'
  - super_index
  - edge_index
  segment_basic_load_keys:
  - pos
  - 'y'
  - super_index
  - sub
  - edge_index
  - edge_attr
  shape_keys:
  - shapes
  - interfaces
  pixel_load_keys: ${eval:'list(set( ${datamodule.pixel_basic_load_keys} + ${datamodule.pixel_hf}
    ))'}
  segment_load_keys: ${eval:'list(set( ${datamodule.segment_basic_load_keys} + ${datamodule.segment_hf}
    + (${datamodule.shape_keys} if ${datamodule.process_shape_interface} else [])))'}
  feat_size:
    pos: 2
    rgb: 3
    hsv: 3
    lab: 3
    node_size: 1
    intensity: 1
    log_pos: 2
    log_rgb: 3
    log_hsv: 3
    log_lab: 3
    log_size: 1
    mean_pos: 2
    mean_rgb: 3
    mean_hsv: 3
    mean_lab: 3
    mean_node_size_0: 1
    mean_intensity: 1
    std_pos: 2
    std_rgb: 3
    std_hsv: 3
    std_lab: 3
    std_node_size_0: 1
    std_intensity: 1
    mean_off: 2
    std_off: 2
    mean_dist: 1
    centroid_dir: 2
    centroid_dist: 1
    shape_embedding: 10
    interface_embedding: 10
    interface_length: 1
    perimeter: 1
    log_perimeter: 1
    e_size: 2
    e_perimeter: 2
    e_log_size: 2
    e_log_perimeter: 2
  _target_: src.datamodules.pascalvoc.PascalVOCDataModule
  data_dir: ${paths.data_dir}
  num_classes: 21
  save_y_to_csr: true
  save_pos_dtype: float32
  save_fp_dtype: float16
  in_memory: false
  lite_preprocessing: true
  max_num_nodes: 50000
  max_num_edges: 1000000
  pre_transform:
  - transform: DataTo
    params:
      device: cpu
  - transform: PixelFeatures
    params:
      keys: ${datamodule.pixel_hf_preprocess}
  - transform: AdjacencyGraph
    params:
      dist: ${datamodule.pcp_dist}
      connectivity: ${datamodule.pcp_connectivity}
  - transform: AddKeysTo
    params:
      keys: ${datamodule.partition_hf}
      to: x
      delete_after: false
  - transform: CutPursuitPartition
    params:
      regularization: ${datamodule.pcp_regularization}
      spatial_weight: ${datamodule.pcp_spatial_weight}
      cutoff: ${datamodule.pcp_cutoff}
      iterations: ${datamodule.pcp_iterations}
      parallel: true
      verbose: false
  - transform: NAGRemoveKeys
    params:
      level: all
      keys: x
  - transform: ShapeInterfacePolygones
    params:
      straight_line_tol: 1.0
  - transform: NAGTo
    params:
      device: ${eval:'"cuda:" + str(${device_id})'}
  - transform: SegmentFeatures
    params:
      keys: ${datamodule.segment_base_hf_preprocess}
      mean_keys: ${datamodule.segment_mean_hf_preprocess}
      std_keys: ${datamodule.segment_std_hf_preprocess}
      edge_keys: ${datamodule.edge_hf}
      strict: false
      num_classes: ${datamodule.num_classes}
  - transform: NAGTo
    params:
      device: cpu
  train_transform: null
  val_transform: ${datamodule.train_transform}
  test_transform: ${datamodule.val_transform}
  on_device_train_transform:
  - transform: NAGCast
  - transform: OnTheFlyHorizontalEdgeFeatures
    params:
      keys: ${datamodule.edge_hf}
  - transform: OnTheFlyVerticalEdgeFeatures
    params:
      keys: ${datamodule.v_edge_hf}
  - transform: NAGAddKeysTo
    params:
      level: 0
      keys: ${eval:'ListConfig([k for k in ${datamodule.pixel_hf} if k != "rgb"])'}
      to: x
  - transform: NAGAddKeysTo
    params:
      level: 1+
      keys: ${eval:'ListConfig([k for k in ${datamodule.segment_hf} if k != "rgb"])'}
      to: x
  - transform: NAGColorAutoContrast
    params:
      p: ${datamodule.rgb_autocontrast}
  - transform: NAGColorDrop
    params:
      p: ${datamodule.rgb_drop}
  - transform: NAGAddKeysTo
    params:
      keys: rgb
      to: x
      strict: false
  - transform: NAGAddSelfLoops
  - transform: NodeSize
  on_device_val_transform:
  - transform: NAGCast
  - transform: OnTheFlyHorizontalEdgeFeatures
    params:
      keys: ${datamodule.edge_hf}
  - transform: OnTheFlyVerticalEdgeFeatures
    params:
      keys: ${datamodule.v_edge_hf}
  - transform: NAGAddKeysTo
    params:
      level: 0
      keys: ${eval:'ListConfig([k for k in ${datamodule.pixel_hf} if k != "rgb"])'}
      to: x
  - transform: NAGAddKeysTo
    params:
      level: 1+
      keys: ${eval:'ListConfig([k for k in ${datamodule.segment_hf} if k != "rgb"])'}
      to: x
  - transform: NAGAddKeysTo
    params:
      keys: rgb
      to: x
      strict: false
  - transform: NAGAddSelfLoops
  - transform: NodeSize
  on_device_test_transform: ${datamodule.on_device_val_transform}
  tta_runs: null
  tta_val: false
  submit: false
  test_subset: null
  shuffle: null
  measure_time: ${measure_time}
  dataloader:
    batch_size: 5
    num_workers: 2
    pin_memory: true
    persistent_workers: true
  custom_hash: 35d2ce50a1b8c531d85ca999364888de
  trainval: false
  pcp_regularization:
  - 5.0e-05
  - 0.0008
  pcp_spatial_weight:
  - 0.1
  - 0.01
  pcp_cutoff:
  - 10
  - 10
  pcp_iterations: 15
  pcp_dist:
  - 0
  pcp_connectivity:
  - -1
  rgb_jitter: 0
  rgb_autocontrast: 0
  rgb_drop: 0
  process_shape_interface: ${eval:'any([i["transform"] == "ShapeInterfacePolygones"
    for i in ${datamodule.pre_transform}])'}
model:
  scheduler:
    _target_: src.optim.CosineAnnealingLRWithWarmup
    _partial_: true
    T_max: ${eval:'${trainer.max_epochs} - ${model.scheduler.num_warmup}'}
    eta_min: 1.0e-06
    warmup_init_lr: 1.0e-06
    num_warmup: 20
    warmup_strategy: cos
  _target_: src.models.segmentation.PixelSegmentationModule
  num_classes: ${datamodule.num_classes}
  sampling_loss: false
  loss_type: ce_kl
  weighted_loss: true
  init_linear: null
  init_rpe: null
  multi_stage_loss_lambdas:
  - 1
  - 50
  transformer_lr_scale: 0.1
  gc_every_n_steps: 0
  optimizer:
    _target_: torch.optim.AdamW
    _partial_: true
    lr: 0.0005
    weight_decay: 0.0001
  criterion:
    _target_: torch.nn.CrossEntropyLoss
  _pixel_mlp:
  - 32
  - 64
  - 128
  _node_mlp_out: 32
  _h_edge_mlp_out: 32
  _v_edge_mlp_out: 32
  _pixel_hf_dim: ${eval:'${model.net.use_pos} * ${datamodule.feat_size.pos} + ${datamodule.num_hf_pixel}
    + ${model.net.use_diameter_parent}'}
  _node_hf_dim: ${eval:'${model.net.use_node_hf} * ${datamodule.num_hf_segment}'}
  _node_injection_dim: ${eval:'${model.net.use_pos} * ${datamodule.feat_size.pos}
    + ${model.net.use_diameter} + ${model.net.use_diameter_parent} + (${model._node_mlp_out}
    if ${model._node_mlp_out} and ${model.net.use_node_hf} and ${model._node_hf_dim}
    > 0 else ${model._node_hf_dim})'}
  _h_edge_hf_dim: ${datamodule.num_hf_edge}
  _h_edge_injection_dim: ${eval:'${model.net.use_pos} * ${datamodule.feat_size.pos}
    * 2 + (${model._h_edge_mlp_out} if ${model._h_edge_mlp_out} else ${model._h_edge_hf_dim})'}
  _v_edge_hf_dim: ${datamodule.num_hf_v_edge}
  _down_dim:
  - 64
  - 64
  _up_dim:
  - 64
  _mlp_depth: 2
  measure_time: ${measure_time}
  net:
    pixel_mlp: ${eval:'[${model._pixel_hf_dim}] + ${model._pixel_mlp}'}
    pixel_drop: null
    down_dim: ${eval:'${model._down_dim}[:2]'}
    down_pool_dim: ${eval:'[${model._pixel_mlp}[-1]] + ${model._down_dim}[:-1]'}
    down_in_mlp: ${eval:'[ [${model._node_injection_dim} + ${model._pixel_mlp}[-1]
      * (not ${model.net.nano}) + ${datamodule.num_hf_segment} * (${model.net.nano}
      and not ${model.net.use_node_hf})] + [${model._down_dim}[0]] * ${model._mlp_depth},
      [${model._node_injection_dim} + ${model._down_dim}[0]] + [${model._down_dim}[1]]
      * ${model._mlp_depth}]'}
    down_out_mlp: null
    down_mlp_drop: null
    down_num_heads: 16
    down_num_blocks: 3
    down_ffn_ratio: 1
    down_residual_drop: null
    down_attn_drop: null
    down_drop_path: null
    up_dim: ${eval:'${model._up_dim}[:1]'}
    up_in_mlp: ${eval:'[ [${model._node_injection_dim} + ${model._down_dim}[-1] +
      ${model._down_dim}[-2]] + [${model._up_dim}[0]] * ${model._mlp_depth}]'}
    up_out_mlp: ${model.net.down_out_mlp}
    up_mlp_drop: ${model.net.down_mlp_drop}
    up_num_heads: ${model.net.down_num_heads}
    up_num_blocks: 1
    up_ffn_ratio: ${model.net.down_ffn_ratio}
    up_residual_drop: ${model.net.down_residual_drop}
    up_attn_drop: ${model.net.down_attn_drop}
    up_drop_path: ${model.net.down_drop_path}
    activation:
      _target_: torch.nn.LeakyReLU
    norm:
      _target_: src.nn.GraphNorm
      _partial_: true
    pre_norm: true
    no_sa: false
    no_ffn: true
    qk_dim: 4
    qkv_bias: true
    qk_scale: null
    in_rpe_dim: ${model._h_edge_injection_dim}
    k_rpe: true
    q_rpe: true
    v_rpe: true
    k_delta_rpe: false
    q_delta_rpe: false
    qk_share_rpe: false
    q_on_minus_rpe: false
    stages_share_rpe: false
    blocks_share_rpe: false
    heads_share_rpe: false
    Local_GNN:
      _target_: src.nn.Local_GNN.Shape_Interface_Encoders
      List_Param_Encoders:
      - num_features: 2
        latent_dim: ${datamodule.feat_size.interface_embedding}
        hidden_dim: 64
        pool: att
        act_fn: leakyrelu
        linear_dropout: 0.1
        conv_layer_type: edgeconv
        num_linear_layers_mult: 2
        agg: mean
        num_res_blocks: 2
        residual_type: add
      share_level: false
      share_type: false
      hyper_share_level: true
      hyper_share_type: true
      nb_levels: ${eval:'len(${datamodule.pcp_regularization})'}
      nano: ${model.net.nano}
      use_shape_interface: ${model.net.use_shape_interface}
    _target_: src.models.components.spt.SPT
    nano: true
    node_mlp: ${eval:'[${model._node_hf_dim} + (${model.net.use_shape_interface}*${datamodule.feat_size.shape_embedding})]
      + [${model._node_mlp_out}] * ${model._mlp_depth} if ${model._node_mlp_out} and
      ${model.net.use_node_hf} and ${model._node_hf_dim} > 0 else None'}
    h_edge_mlp: ${eval:'[${model._h_edge_hf_dim} + (${model.net.use_shape_interface}*${datamodule.feat_size.interface_embedding})]
      + [${model._h_edge_mlp_out}] * ${model._mlp_depth} if ${model._h_edge_mlp_out}
      else None'}
    v_edge_mlp: ${eval:'[${model._v_edge_hf_dim}] + [${model._v_edge_mlp_out}] * ${model._mlp_depth}
      if ${model._v_edge_mlp_out} else None'}
    share_hf_mlps: false
    mlp_activation:
      _target_: torch.nn.LeakyReLU
    mlp_norm:
      _target_: src.nn.GraphNorm
      _partial_: true
    use_pos: true
    use_node_hf: true
    use_diameter: false
    use_diameter_parent: false
    use_shape_interface: ${datamodule.process_shape_interface}
    pool: max
    unpool: index
    fusion: cat
    norm_mode: graph
    measure_time: ${measure_time}
callbacks:
  model_checkpoint:
    _target_: pytorch_lightning.callbacks.ModelCheckpoint
    dirpath: ${paths.output_dir}/checkpoints
    filename: epoch_{epoch:03d}
    monitor: val/miou
    verbose: false
    save_last: true
    save_top_k: 1
    mode: max
    auto_insert_metric_name: false
    save_weights_only: false
    every_n_train_steps: null
    train_time_interval: null
    every_n_epochs: null
    save_on_train_epoch_end: null
  early_stopping:
    _target_: pytorch_lightning.callbacks.EarlyStopping
    monitor: val/miou
    min_delta: 0.0
    patience: 500
    verbose: false
    mode: max
    strict: true
    check_finite: true
    stopping_threshold: null
    divergence_threshold: null
    check_on_train_epoch_end: null
  model_summary:
    _target_: pytorch_lightning.callbacks.RichModelSummary
    max_depth: -1
  rich_progress_bar:
    _target_: pytorch_lightning.callbacks.RichProgressBar
  lr_monitor:
    _target_: pytorch_lightning.callbacks.LearningRateMonitor
    logging_interval: epoch
    log_momentum: true
  gradient_accumulator:
    _target_: pytorch_lightning.callbacks.GradientAccumulationScheduler
    scheduling:
      0: 1
logger:
  csv:
    _target_: pytorch_lightning.loggers.csv_logs.CSVLogger
    save_dir: ${paths.output_dir}
    name: csv/
    prefix: ''
trainer:
  _target_: pytorch_lightning.Trainer
  default_root_dir: ${paths.output_dir}
  min_epochs: 1
  max_epochs: 60
  accelerator: gpu
  devices: ${eval:'[${device_id}]'}
  check_val_every_n_epoch: 10
  deterministic: false
  precision: 32
paths:
  root_dir: ${oc.env:PROJECT_ROOT}
  data_dir: ${paths.root_dir}/data/
  log_dir: ${paths.root_dir}/logs/
  output_dir: ${hydra:runtime.output_dir}
  work_dir: ${hydra:runtime.cwd}
extras:
  ignore_warnings: false
  enforce_tags: true
  print_config: true
