import numpy as np
import torch
import torchnet as tnt
import torch.nn as nn
import torch.optim as optim
from tqdm import tqdm
import copy
import GPUtil


import modules.Confusion_Matrix as CM
import modules.network_definition as net_def

def get_repart():
  return net_def.get_repart()

def train_TV(model, optimizer, args, epoch, train_set):
  """train for one epoch"""
  model.train() #switch the model in training mode
  
  #the loader function will take care of the batching
  loader = torch.utils.data.DataLoader(train_set, \
         batch_size=args.batch_size, shuffle=True, drop_last=True)
  #tqdm will provide some nice progress bars
  loader = tqdm(loader)
    
  #will keep track of the loss
  loss_meter = tnt.meter.AverageValueMeter()
  loss_meter_reg = tnt.meter.AverageValueMeter()
  loss_meter_total = tnt.meter.AverageValueMeter()
  cm = CM.ConfusionMatrix(args)

  for index, (tiles, gt) in enumerate(loader):

    if model.is_cuda: #put the ground truth on the GPU
      gt = gt.cuda()

    optimizer.zero_grad() #put gradient to zero

    pred,reg_val = model(args, tiles, epoch) #compute the prediction
    
    if epoch < args.blocage_epoch+args.blocage_reg:
      loss_reg = torch.tensor(0.)
    else:
      loss_reg = reg_val * args.coef_reg
    
    loss = nn.functional.cross_entropy(pred,gt,weight=args.weight_class, ignore_index=99) #compute the loss
    loss_total = loss + loss_reg

    loss_total.backward() #compute gradients

    optimizer.step() #one SGD step

    loss_meter.add(loss.item())
    loss_meter_reg.add(loss_reg.item())
    loss_meter_total.add(loss_total.item())

    gt = gt.cpu() #back on the RAM

    labeled = np.where(gt.view(-1)!=99)[0] #select gt with a label, ie not 99
    #need to put the prediction back on the cpu and convert to numpy before feeding it to the CM
    cm.add_batch(gt.view(-1)[labeled], pred.argmax(1).view(-1)[labeled].cpu().detach().numpy())
    
  return cm, loss_meter.value()[0], loss_meter_reg.value()[0], loss_meter_total.value()[0]

def eval_TV(model, args, epoch, img_set):
  """eval on test/validation set"""

  model.eval() #switch in eval mode
  
  loader = torch.utils.data.DataLoader(img_set, batch_size=1, shuffle=False, drop_last=False) 
  
  loader = tqdm(loader)
  
  loss_meter = tnt.meter.AverageValueMeter()
  loss_meter_reg = tnt.meter.AverageValueMeter()
  loss_meter_total = tnt.meter.AverageValueMeter()
  cm = CM.ConfusionMatrix(args)

  with torch.no_grad(): #do not compute gradients (saves memory)
    for index, (tiles, gt) in enumerate(loader):
      
      if model.is_cuda: #put the ground truth on the GPU
        gt = gt.cuda()
      
      pred,reg_val = model(args, tiles, epoch) #compute the prediction

      if epoch < args.blocage_epoch+args.blocage_reg:
        loss_reg = torch.tensor(0.)
      else:
        loss_reg = reg_val * args.coef_reg
      
      loss = nn.functional.cross_entropy(pred,gt,weight=args.weight_class, ignore_index=99) #compute the loss
      loss_total = loss + loss_reg
      
      loss_meter.add(loss.item())
      loss_meter_reg.add(loss_reg.item())
      loss_meter_total.add(loss_total.item())

      gt = gt.cpu() #back on the RAM

      labeled = np.where(gt.view(-1)!=99)[0] #select gt with a label, ie not 99
      #need to put the prediction back on the cpu and convert to numpy before feeding it to the CM
      cm.add_batch(gt.view(-1)[labeled], pred.argmax(1).view(-1)[labeled].cpu().detach().numpy())
    
  return cm, loss_meter.value()[0], loss_meter_reg.value()[0], loss_meter_total.value()[0]

def train_full_TV(args, train_set, validation_set, test_set):
  """The full training loop"""
  #initialize the model
  net_def.reset_repart()

  model = net_def.SegNet(args) 

  #stock les valeurs pour les graph d'evolution
  evol_train=np.zeros((args.n_epoch,6))
  evol_valid=np.zeros((args.n_epoch,6))
  evol_test=np.zeros((args.n_epoch,6))
  evol_composante=np.zeros((args.n_epoch,4))
  compt_test=0

  print('Total number of parameters: {}'.format(sum([p.numel() for p in model.parameters()])))
  
  #define the optimizer
  #adam optimizer is always a good guess for classification
  optimizer = optim.Adam(model.parameters(), lr=args.lr)
  
  scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=args.scheduler_step, gamma=args.scheduler_gamma, last_epoch = args.scheduler_end)

  TESTCOLOR = '\033[104m'
  TRAINCOLOR = '\033[100m'
  VALIDCOLOR = '\033[101m'
  NORMALCOLOR = '\033[0m'

  i_epoch = 0
  compt_verif = 0
  max_IoU = 0
  min_comp = 65536
  save_model = copy.deepcopy(model)
  epoch_save = 0
  
  while i_epoch < args.n_epoch and compt_verif < args.n_epoch_validation:
    #train one epoch
    cm_train, loss_train, loss_reg_train, loss_total_train = train_TV(model, optimizer, args, i_epoch, train_set)

    evol_composante[i_epoch,0]=i_epoch
    evol_composante[i_epoch,1]=torch.mean(torch.tensor(args.compte_composante).float())
    args.compte_composante = []
    
    print(TRAINCOLOR)
    print('Epoch %3d -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f Loss reg: %1.10f Loss_total: %1.4f' % \
          (i_epoch, cm_train.overall_accuracy()*100, cm_train.class_IoU(), loss_train,  loss_reg_train, loss_total_train) + NORMALCOLOR)
    evol_train[i_epoch,0]=i_epoch
    evol_train[i_epoch,1]=cm_train.overall_accuracy()
    evol_train[i_epoch,2]=cm_train.class_IoU()
    evol_train[i_epoch,3]=loss_train
    evol_train[i_epoch,4]=loss_reg_train
    evol_train[i_epoch,5]=loss_total_train

    cm_valid, loss_valid, loss_reg_valid, loss_total_valid= eval_TV(model, args, i_epoch, validation_set)

    evol_composante[i_epoch,2]=torch.mean(torch.tensor(args.compte_composante).float())
    args.compte_composante = []

    print(VALIDCOLOR)
    print('Validation %3d -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f Loss reg: %1.10f Loss_total: %1.4f' % \
        (i_epoch, cm_valid.overall_accuracy()*100, cm_valid.class_IoU(), loss_valid, loss_reg_valid, loss_total_valid) + NORMALCOLOR)
    evol_valid[i_epoch,0]=i_epoch
    evol_valid[i_epoch,1]=cm_valid.overall_accuracy()
    evol_valid[i_epoch,2]=cm_valid.class_IoU()
    evol_valid[i_epoch,3]=loss_valid
    evol_valid[i_epoch,4]=loss_reg_valid
    evol_valid[i_epoch,5]=loss_total_valid

    new_model = False
    # sauvegarde dans le cas d'un nouveau model mieux que le precedants
    if not args.do_reg or i_epoch >= args.blocage_epoch + (max(args.blocage_reg, args.red_grad_lambda)*2):
      if max_IoU <= evol_valid[i_epoch,2]: # and evol_composante[i_epoch,2] <= min_comp:
        compt_verif = 0
        new_model = True
        max_IoU = evol_valid[i_epoch,2]
        min_comp = evol_composante[i_epoch,2]
        save_model = copy.deepcopy(model)
        epoch_save = i_epoch
      else:
        compt_verif = compt_verif +1
    
    scheduler.step()

    if (i_epoch == args.n_epoch - 1) or new_model or compt_verif >= args.n_epoch_validation:
      #periodic testing
      net_def.reset_repart()
      cm_test, loss_test, loss_reg_test, loss_total_test= eval_TV(save_model, args, epoch_save, test_set)

      evol_composante[i_epoch,3]=torch.mean(torch.tensor(args.compte_composante).float())
      args.compte_composante = []
      
      print(TESTCOLOR)
      print('Test %3d -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f Loss reg: %1.10f Loss_total: %1.4f' % \
          (compt_test, cm_test.overall_accuracy()*100, cm_test.class_IoU(), loss_test, loss_reg_test, loss_total_test) + NORMALCOLOR)
      evol_test[compt_test,0]=compt_test
      evol_test[compt_test,1]=cm_test.overall_accuracy()
      evol_test[compt_test,2]=cm_test.class_IoU()
      evol_test[compt_test,3]=loss_test
      evol_test[compt_test,4]=loss_reg_test
      evol_test[compt_test,5]=loss_total_test
      compt_test=compt_test+1

    i_epoch = i_epoch + 1 
    print("\n")
    
  return [save_model,evol_train,evol_test,evol_valid,evol_composante,epoch_save]